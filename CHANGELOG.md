# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.5.0] - 2022-03-17
### Added
- Ability to create new people when adding guests to an episode.
- The setting in image form, whether to use URL or file, is remembered.
- Ability to merge duplicate people.
- Ability to merge duplicate episodes of same show.
- Ability to add aliases for episodes.

### Fixed
- "Aired at" for episodes contained time even though the accuracy from external sources is usually only the date.
- The icon for shows in person list was incorrect.
- The icon for Wikipedia was incorrect.
- The label for YouTube when viewing an episode was incorrect.

## [1.4.0] - 2021-10-13
### Added
- Ability to add books a person has authored.
- Ability to earn achievements by creating and editing resources.
- Clicking on a topic shows a page with all resources for that topic.
- Episode description is shown when viewing a description.
- Episode lists contain information about the show and when the episode aired.
- Improved formatting of episode descriptions.
- Indicator showing if an episode will be merged when generating from an external source.
- Links to Wikipedia for people and shows.
- Redesign of all lists.
- The page for viewing a person has been redesigned and split into separate pages.
- When viewing a person for a specific show, if the person has appeared on the show, embedded media is shown for that episode.

### Fixed
- Incorrect badge in admin area for imageless resources.
- List of guests not yet on show contained previous guests.
- Nothing happened when trying to update a resource with an image, without changing the image.
- Only a maximum of ten episodes were listed when viewing a show.

## [1.3.0] - 2021-10-02
### Added
- Add ratings to shows, episodes and people.
- Any user can suggest edits to shows, episodes and people.
- Confirmation dialog when generating episodes manually from source.
- Double whitespace is removed from episode titles when generating from external source.
- Share buttons on some select pages, but without external tracking.

### Fixed
- Could not add existing topics to resource.
- Error when creating a person with pseudonyms.
- Error when creating a resource with topics.
- Error when deleting source from 'Edit show' page.
- Error when generating episodes with same title, but different casing.
- Redis Store gem was not compatible with cache versioning.
- Some lists contained duplicate entries.
- Unable to deploy due to missing Redis gem.
- Unable to deploy due to the slug being too large.

### Removed
- Turbolinks is no longer used to fetch pages.

## [1.2.0] - 2021-09-23
### Added
- The user is redirected back to the originating page after editing a resource, instead of to the resource.
- Users can turn off getting emails when they get a notification such as when a guest they follow appears on a show.
- When viewing a person you can now cast votes for that person to be on a given show.
- Some lists of episodes, poeple and shows are split into pages.
- Episodes, shows and people can be assigned topics.

### Fixed
- It was not possible to update a source's foreign ID.
- Error when trying to fetch episodes from a YouTube user (instead of a channel).
- New episodes created when fetching from YouTube were incorrectly given a Spotify ID as well.
- Trying to view a non-existing resource resulted in error 500 instead of 404.
- Fix error message when trying to sign in with Google account.
- Some text had an incorrect translation.
- It was not possible to update full name in user settings.
- Gravatar was fetched over HTTP instead of HTTPS, causing warnings in browser console.
- Error occurred when generating an episode with an existing slug.
- Minor performance improvements.

## [1.1.0] - 2021-09-14
### Added
- Change language on settings page. Currently only Swedish is (partly) available.
- Set full name on settings page.
- Images in search results makes it easier to spot what you are looking for.
- Button to follow/unfollow when viewing person.
- Episodes can now be fetched from YouTube.
- Embedded YouTube player for episodes with an external YouTube link.
- Titles are cleaned up when fetching from external source, removing prefix/suffix.
- Small badge on admin link in navbar, indicating whether or not there are any unconfirmed resources.
- Ability to remove notifications.
- People can be added to shows as hosts.

### Fixed
- Fix overflow of overly long lines in episodes, shows and people descriptions

## [1.0.0] - 2021-09-12
### Added
- Statistics box on the admin dashboard.
- List of recent people and episodes on admin dashboard.

### Fixed
- The list of recent shows on the admin dashboard was in the wrong order.
- Error when deleting person with appearance.

## [0.11.0] - 2021-09-12
### Added
- Slugs are automatically generated when creating new resources, no more need to manually create them.
- It is possible to manually set when an episode aired.
- Admins can list resources without an image.
- Descriptions of people.

### Changed
- When fetching episode guests will no longer be generated if the episode already exists.
- Links to external sources open in new tabs instead of the existing tab.

### Fixed
- Pseudonyms were not considered when generating guests from external source.

## [0.10.2] - 2021-09-12
### Fixed
- Sitemap was missing from deployment.
- A debug string was shown when listing guests on show.

## [0.10.1] - 2021-09-11
### Fixed
- An error occured when trying to vote for non-existing person.
- The website didn't have a favicon.
- List of shows and guests were overflowing on smaller screens.

## [0.10.0] - 2021-09-11
### Added
- Images are clickable, not just the title / name.
- Links to GitLab are added to the page, letting users know how to report issues and feature requests.
- List of guests and shows on user profile are extended with more info and images.
- User profile shows list of guests the user has marked as favorite.
- A list of all the guests the user has voted on are shown when viewing a show.
- An autocomplete list when typing in a name to vote for.

### Fixed
- On smaller screen the images on the front page where too big and created a horizontal scrollbar.
- Improved title when viewing person as a guest on a show.

## [0.9.2] - 2021-09-10
### Fixed
- Error in list of guests not yet appeared on show.
- Long text in search menu would overflow.
- It was not possible to vote for existing person via text box.
- No feedback was shown when voting on a person via text box.
- An error was shown when trying to view a person for a show.

## [0.9.1] - 2021-09-10
### Fixed
- Error pages 404 and 500 use the layout and style of the rest of the website.
- Error fetching guests for show.
- Default images were not shown for people without an image.

## [0.9.0] - 2021-09-09
### Added
- Page with privacy policy.
- Page with terms of service.
- Welcome page explaining the website.
- Footer with links to pages and copyright notice.
- Sign in and sign up pages are redesigned with Google logo for OAuth based authentication.
- Show sources can be toggled to automatically fetch episodes on a daily schedule.

### Fixed
- Users could not see resources they themselves created until it was confirmed by an admin.
- When users would click on a search result, the back button would take them to an empty search and not the previous
  query.

## [0.8.1] - 2021-09-08
### Fixed
- Episodes could not be fetched from Spotify due to invalid show ID being used.
- Episodes on shows were shown in an incorrect order.
- When viewing an episode, an incorrect time was shown under "Aired at".

## [0.8.0] - 2021-09-07
### Added
- Emails are sent out when a user receives a new notification of a guest appearance.
- Major redesign of the site to make it look more modern.
- All pages use proper page titles, making your browser history easier to read.
- Breadcrumbs are added to the top on some pages, making it easier to navigate around.
- When viewing a person lists are displayed with the shows and episodes where that person has appeared.
- Page for voting on a person to appear on a show. Great for sharing!
- Guests are automatically parsed when fetching episodes from an external source.
- It is possible to vote when signed out, once.

## [0.7.0] - 2021-09-04
### Added
- Normal users are allowed to create episodes on shows.
- Shows and episodes created by normal users need to be confirmed by an admin.
- Shows and episodes can be blocked by an admin and hidden from lists.
- Authentication is no longer required when viewing guests, shows and episodes.

### Fixed
- Guests which are not confirmed or blocked are no longer shown in lists.
- On small screens the scrollbar was placed next to the navbar, making the navbar move when moving from a page with
  a scrollbar to a page without one.

## [0.6.0] - 2021-09-03
### Added
- Episodes can be fetched from an external service such as Spotify.
- Embedded Spotify media is shown when an episode has a Spotify ID.
- Users can report shows, episodes and guests.

### Fixed
- The page would zoom in when focusing on input fields on iPhone.

## [0.5.0] - 2021-08-31
### Added
- Icons in the navbar to make it easier to quickly identify navigation buttons.
- Application settings inside the administrative area, allowing admins to adjust settings.
- Votes are limited per show, currently set to 5 but this limit is adjustable by admins.

### Fixed
- Lots of fixes when the site is viewed on smaller screens.
- Clicking thumbs up a second time will now remove the previous vote.

## [0.4.0] - 2021-08-28
### Added
- Notifications whenever a guest whom you've voted for appears on the show.
- Search function for quickly finding episodes, shows and guests.
- Ability to configure country and time zone.
- You can now sign in with your Google account.
- Guests on shows are sorted by votes.

## [0.3.0] - 2021-08-26
### Added
- Ability to edit images for people and episodes.

## [0.2.0] - 2021-08-25
### Added
- Improved styling of the web site.
- Guests can be added to an episode.
- Ability to sign in using both email and username.
- Link shows, guests and episodes to external sources such as YouTube and Spotify.

### Fixed
- Failed captcha verification resulted in server error instead of just an alert message.

## [0.1.3] - 2021-08-21
### Fixed
- Fixed issue with sending confirmation emails.
- Fixed issue with excessive error messages.

## [0.1.2] - 2021-08-21
### Fixed
- Fixed issue with sending emails due to an unconfigured hostname preventing URL generation inside mail templates.

## [0.1.1] - 2021-08-20
### Fixed
- Typo in ApplicationMailer.
- Install Heroku prior to deployment to fix database migration.

## [0.1.0] - 2021-08-20
### Added
- Shows.
- Guests.
- Episodes.

[Unreleased]: https://gitlab.com/ephracis/shocadb/compare/1.5.0...HEAD
[1.5.0]: https://gitlab.com/ephracis/shocadb/compare/1.4.0...1.5.0
[1.4.0]: https://gitlab.com/ephracis/shocadb/compare/1.3.0...1.4.0
[1.3.0]: https://gitlab.com/ephracis/shocadb/compare/1.2.0...1.3.0
[1.2.0]: https://gitlab.com/ephracis/shocadb/compare/1.1.0...1.2.0
[1.1.0]: https://gitlab.com/ephracis/shocadb/compare/1.0.0...1.1.0
[1.0.0]: https://gitlab.com/ephracis/shocadb/compare/0.11.0...1.0.0
[0.11.0]: https://gitlab.com/ephracis/shocadb/compare/0.10.2...0.11.0
[0.10.2]: https://gitlab.com/ephracis/shocadb/compare/0.10.1...0.10.2
[0.10.1]: https://gitlab.com/ephracis/shocadb/compare/0.10.0...0.10.1
[0.10.0]: https://gitlab.com/ephracis/shocadb/compare/0.9.2...0.10.0
[0.9.2]: https://gitlab.com/ephracis/shocadb/compare/0.9.1...0.9.2
[0.9.1]: https://gitlab.com/ephracis/shocadb/compare/0.9.0...0.9.1
[0.9.0]: https://gitlab.com/ephracis/shocadb/compare/0.8.1...0.9.0
[0.8.1]: https://gitlab.com/ephracis/shocadb/compare/0.8.0...0.8.1
[0.8.0]: https://gitlab.com/ephracis/shocadb/compare/0.7.0...0.8.0
[0.7.0]: https://gitlab.com/ephracis/shocadb/compare/0.6.0...0.7.0
[0.6.0]: https://gitlab.com/ephracis/shocadb/compare/0.5.0...0.6.0
[0.5.0]: https://gitlab.com/ephracis/shocadb/compare/0.4.0...0.5.0
[0.4.0]: https://gitlab.com/ephracis/shocadb/compare/0.3.0...0.4.0
[0.3.0]: https://gitlab.com/ephracis/shocadb/compare/0.2.0...0.3.0
[0.2.0]: https://gitlab.com/ephracis/shocadb/compare/0.1.3...0.2.0
[0.1.3]: https://gitlab.com/ephracis/shocadb/compare/0.1.2...0.1.3
[0.1.2]: https://gitlab.com/ephracis/shocadb/compare/0.1.1...0.1.2
[0.1.1]: https://gitlab.com/ephracis/shocadb/compare/0.1...0.1.1
[0.1.0]: https://gitlab.com/ephracis/shocadb/compare/f72e38444bf30aa941a2da88770cc93b6e8e551e...0.1
