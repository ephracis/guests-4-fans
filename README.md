# ShocaDB

[ShocaDB](https://shocadb.com) is a web app where users can vote for their favorite guests to appear on
their favorite shows.

This is an open project where anyone can contribute by registering shows and
possible guests, hunting for bugs and reporting issues, or create Merge
Requests with code suggestions.

## Development

Before you can start developing you need to install Ruby, Python and NodeJS.

Then install all dependencies:

```shell
gem install bundler
npm install yarn
bundle install
yarn install
```

Read more about:
- [Testing](docs/Testing.md)
- [Managing dependencies](docs/Dependencies.md)
- [Releasing](docs/Release.md)
- [Deployment](docs/Deployment.md)
