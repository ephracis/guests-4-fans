# frozen_string_literal: true

require 'application_system_test_case'

class PeopleTest < ApplicationSystemTestCase
  setup do
    @person = create(:person)
  end

  test 'visiting the index signed out' do
    visit people_url
    assert_selector '.card-header-lg', text: 'POPULAR GUESTS'
    assert_selector 'a', text: 'ADD PERSON', count: 0
    assert_selector '.dropdown a[type="button"]', count: 0
    assert_selector 'a', text: 'Edit', count: 0
    assert_selector 'a', text: 'Remove', count: 0
  end

  test 'visiting the index as user' do
    login_as create(:user), scope: :user
    visit people_url
    assert_selector '.card-header-lg', text: 'POPULAR GUESTS'
    assert_selector 'a', text: 'ADD PERSON'
    assert_selector '.dropdown a[type="button"]', count: 1
    find('.dropdown a[type="button"]', match: :first).click
    assert_selector 'a', text: 'Edit', count: 1
    assert_selector 'a', text: 'Remove', count: 0
  end

  test 'visiting the index as admin' do
    login_as create(:admin_user), scope: :user
    visit people_url
    assert_selector '.card-header-lg', text: 'POPULAR GUESTS'
    assert_selector 'a', text: 'ADD PERSON'
    assert_selector '.dropdown a[type="button"]', count: 1
    find('.dropdown a[type="button"]', match: :first).click
    assert_selector 'a', text: 'Edit'
    assert_selector 'a', text: 'Remove'
  end

  test 'creating a person' do
    login_as create(:user), scope: :user

    visit people_url
    click_on 'Add person'

    fill_in 'Description', with: 'This is my test person.'
    fill_in 'Name', with: 'Test Person'
    click_on 'Save'

    assert_text 'The person has been created'
  end

  test 'creating a person as admin' do
    login_as create(:admin_user), scope: :user

    visit people_url
    click_on 'Add person'

    fill_in 'Description', with: 'This is my test person.'
    fill_in 'Name', with: 'Test Person'
    fill_in 'Aliases', with: 'A Test Person,'
    fill_in 'Topics', with: 'Test Topic,'
    click_on 'Save'

    assert_text 'The person has been created'
    assert_equal 'Test Person', Person.last.name
    assert_equal Person.last, Person.find_by_alias('A Test Person')
    assert_equal 'Test Topic', Person.last.topics.first.name
  end

  test 'updating a person from index' do
    login_as create(:admin_user), scope: :user

    visit people_url
    find('.dropdown a[type="button"]', match: :first).click
    click_on 'Edit', match: :first

    fill_in 'Slug', with: 'new-slug'
    fill_in 'Description', with: 'A new description.'
    click_on 'Save'

    assert_text 'The person has been updated'
  end

  test 'updating a person from show' do
    login_as create(:admin_user), scope: :user

    visit people_url
    click_on @person.name
    click_on 'Edit', match: :first

    fill_in 'Slug', with: 'new-slug'
    fill_in 'Description', with: 'A new description.'
    click_on 'Save'

    assert_text 'The person has been updated'
  end

  test 'destroying a person' do
    login_as create(:admin_user), scope: :user

    visit people_url
    find('.dropdown a[type="button"]', match: :first).click
    page.accept_confirm do
      click_on 'Remove', match: :first
    end

    assert_text 'The person has been removed'
  end

  test 'visiting person' do
    person = create(:person, name: 'Test Person')
    visit person_url(person)
    assert_selector 'h2', text: 'Test Person'
    assert_selector '.profile-header a', text: 'Shows'
    assert_selector '.profile-header a', text: 'Episodes'
    assert_selector '.profile-header a', text: 'Books'
    assert_selector '.profile-header a', text: 'About'

    find('.profile-header a', text: 'Episodes').click
    assert_selector '.card-header', text: /Episodes with Test Person/i

    find('.profile-header a', text: 'Books').click
    assert_selector '.card-header', text: /Books by Test Person/i

    find('.profile-header a', text: 'About').click
    assert_selector 'dt', text: /Name/i
    assert_selector 'dd', text: 'Test Person'

    find('.profile-header a', text: 'Shows').click
    assert_selector '.card-header', text: /My votes/i
  end

  test 'visiting person for show without appearance' do
    person = create(:person, name: 'Test Person')
    show = create(:show, title: 'Test Show')

    visit show_person_url(show, person)
    assert_selector 'h2', text: 'Test Person'
    assert_selector 'h2', text: 'Test Show'
    assert_selector '.profile-duo-header', text: /Bring Test Person to Test Show/
  end

  test 'visiting person for show with appearance' do
    person = create(:person, name: 'Test Person')
    show = create(:show, title: 'Test Show')
    episode = create(:episode, show: show, spotify: nil, youtube: nil)
    create(:appearance, person: person, episode: episode)

    visit show_person_url(show, person)
    assert_selector 'h2', text: 'Test Person'
    assert_selector 'h2', text: 'Test Show'
    assert_selector '.profile-duo-header', text: /Vote for Test Person to return to Test Show/

    episode.update(spotify: '5EaeXVzcvxR20AYqkI1XA6')
    visit show_person_url(show, person)
    assert_selector '.embed-spotify'

    episode.update(youtube: 'jtl4VPgKKHc')
    visit show_person_url(show, person)
    assert_selector '.embed-youtube'
  end

  test 'marking person as duplicate' do
    login_as create(:admin_user), scope: :user

    original = create(:person, name: 'Mr Original', description: nil)
    create(:person, name: 'Mr Duplicate', description: 'A nice description.')

    visit people_url
    find('.dropdown a[type="button"]', match: :first).click
    click_on 'Merge', match: :first

    assert_selector 'h1', text: 'Merge the person'
    assert_selector 'h5', text: 'Mr Duplicate'
    assert_selector 'h2', text: 'with the person'

    fill_in 'Original person', with: 'original'
    find('#original input').click
    find('#original .dropdown-menu li', match: :first).click

    assert_difference 'Person.count', -1 do
      click_on 'Merge'
      assert_text 'Mr Duplicate has been merged with Mr Original'
    end

    original.reload
    assert_equal 'A nice description.', original.description
  end
end
