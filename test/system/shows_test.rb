# frozen_string_literal: true

require 'application_system_test_case'

class ShowsTest < ApplicationSystemTestCase
  setup do
    @show = create(:show)
  end

  test 'visiting the index signed out' do
    visit shows_url
    assert_selector '.card-header-lg', text: 'SHOWS'
    assert_selector 'a', text: 'ADD SHOW', count: 0
    assert_selector '.dropdown a[type="button"]', count: 0
    assert_selector 'a', text: 'Edit', count: 0
    assert_selector 'a', text: 'Remove', count: 0
  end

  test 'visiting the index as user' do
    login_as create(:user), scope: :user
    visit shows_url
    assert_selector '.card-header-lg', text: 'SHOWS'
    assert_selector 'a', text: 'ADD SHOW'
    assert_selector '.dropdown a[type="button"]', count: 1
    find('.dropdown a[type="button"]', match: :first).click
    assert_selector 'a', text: 'Edit', count: 1
    assert_selector 'a', text: 'Remove', count: 0
  end

  test 'visiting the index as admin' do
    login_as create(:admin_user), scope: :user
    visit shows_url
    assert_selector '.card-header-lg', text: 'SHOWS'
    assert_selector 'a', text: 'ADD SHOW'
    assert_selector '.dropdown a[type="button"]', count: 1
    find('.dropdown a[type="button"]', match: :first).click
    assert_selector 'a', text: 'Edit'
    assert_selector 'a', text: 'Remove'
  end

  test 'creating a show' do
    login_as create(:user), scope: :user

    visit shows_url
    click_on 'Add show'

    fill_in 'Description', with: 'This is my test show.'
    fill_in 'Title', with: 'Test Show'
    click_on 'Save'

    assert_text 'Show was successfully created'
  end

  test 'updating a show' do
    login_as create(:admin_user), scope: :user

    visit shows_url
    find('.dropdown a[type="button"]', match: :first).click
    click_on 'Edit', match: :first

    fill_in 'Name', with: 'a-new-name'
    fill_in 'Description', with: 'A new description.'
    click_on 'Save'

    assert_text 'The show has been updated'
  end

  test 'destroying a show' do
    login_as create(:admin_user), scope: :user

    visit shows_url
    find('.dropdown a[type="button"]', match: :first).click
    page.accept_confirm do
      click_on 'Remove', match: :first
    end

    assert_text 'Show was successfully deleted'
  end

  test 'add show source' do
    show = create(:show)
    create(:episode, show: show, title: 'episode 1')
    login_as create(:admin_user), scope: :user

    visit show_url(show)
    assert_selector '#sidebar a', text: 'Edit', count: 1
    find('#sidebar a', text: 'Edit').click

    click_on 'Add source'
    assert_selector 'a.btn', text: 'Fetch episodes', count: 0

    fill_in 'Foreign ID', with: 'my-spotify-id'
    find('#sidebar a', text: 'Save').click
    assert_selector 'a.btn', text: 'Fetch episodes', count: 1

    source = Source.last
    episodes = {
      'source' => JSON.parse(source.to_json),
      'episodes' => [
        {
          'description' => 'some description',
          'source_id' => 'episode-spotify-id',
          'image' => 'https://via.placeholder.com/150',
          'title' => 'Episode 1',
          'aired_at' => DateTime.now,
          'guests' => ['Guest 1', 'Guest 2']
        },
        {
          'description' => 'some description',
          'source_id' => 'episode-spotify-id',
          'image' => 'https://via.placeholder.com/150',
          'title' => 'Episode 2',
          'aired_at' => DateTime.now,
          'guests' => ['Guest 3']
        }
      ]
    }
    Source.any_instance.expects(:fetch_from_spotify).returns(episodes)

    assert_equal 1, Source.count

    find('a.btn', text: 'Fetch episodes').click

    has_field? '.list-group-item input', with: 'Episode 1'
    assert_text 'Guest 1'
    assert_text 'Guest 2'
    has_field? '.list-group-item input', with: 'Episode 2'
    assert_text 'Guest 3'
    assert_selector "[title='Episode exists and will merge.']", count: 1
  end
end
