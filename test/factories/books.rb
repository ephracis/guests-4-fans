# frozen_string_literal: true

FactoryBot.define do
  factory :book do
    sequence(:slug) { |n| "test-book-#{n}" }
    sequence(:name) { |n| "Test Book #{n}" }
    created_by { create(:user) }
    confirmed_at { nil }
    confirmed_by { nil }
    published_at { '2021-10-04 12:26:32' }
    amazon { 'MyString' }
    authors { [create(:person)] }
  end
end
