# frozen_string_literal: true

FactoryBot.define do
  factory :role do
    sequence(:name) { |n| "my_role_#{n}" }
    sequence(:title) { |n| "My Role #{n}" }

    factory :root_role do
      name { 'root' }
      title { 'Root' }
    end

    factory :admin_role do
      name { 'admin' }
      title { 'Administrator' }
    end

    factory :limited_role do
      name { 'limited' }
      title { 'Limited' }
    end

    factory :blocked_role do
      name { 'blocked' }
      title { 'Blocked' }
    end
  end
end
