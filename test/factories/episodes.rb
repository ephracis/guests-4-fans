# frozen_string_literal: true

FactoryBot.define do
  factory :episode do
    show { create(:show) }
    sequence(:slug) { |n| "test-episode-#{n}" }
    sequence(:title) { |n| "Test Episode #{n}" }
    created_by { create(:user) }
    description { 'MyString' }
    confirmed_at { DateTime.now }
    confirmed_by { create(:user) }
    length { 1 }
    aired_at { '2021-07-29 11:18:22' }
    youtube { 'my_episode' }
    apple_podcast { 'my_episode' }
    spotify { 'my_episode' }
    website { 'http://my_episode.com' }
  end
end
