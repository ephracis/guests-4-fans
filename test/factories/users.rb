# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    sequence(:username) { |n| "john#{n}" }
    fullname { 'John Doe' }
    email { "#{username}@mail.com" }
    time_zone { 'Stockholm' }
    password { 'changeme' }
    confirmed_at { DateTime.now }

    factory :root_user do
      roles { create_list(:root_role, 1) }
    end

    factory :admin_user do
      roles { create_list(:admin_role, 1) }
    end

    factory :limited_user do
      roles { create_list(:limited_role, 1) }
    end

    factory :blocked_user do
      roles { create_list(:blocked_role, 1) }
    end
  end
end
