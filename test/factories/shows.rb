# frozen_string_literal: true

FactoryBot.define do
  factory :show do
    sequence(:slug) { |n| "test-show-#{n}" }
    sequence(:title) { |n| "Test Show #{n}" }
    created_by { create(:user) }
    confirmed_at { DateTime.now }
    confirmed_by { create(:user) }
    youtube { 'my_show' }
    twitter { 'my_show' }
    facebook { 'my_show' }
    apple_podcast { 'my_show' }
    spotify { 'my_show' }
    website { 'http://my_show.com' }
    rumble { 'my_show' }
    parler { 'my_show' }
    description { 'My cool show' }
  end
end
