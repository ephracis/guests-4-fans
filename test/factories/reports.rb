# frozen_string_literal: true

FactoryBot.define do
  factory :report do
    comment { 'MyText' }
    user { create(:user) }
    category { 'MyString' }
    reportable { create(:show) }
    read { false }
  end
end
