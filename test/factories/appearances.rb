# frozen_string_literal: true

FactoryBot.define do
  factory :appearance do
    person { create(:person) }
    episode { create(:episode) }
  end
end
