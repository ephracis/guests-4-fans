# frozen_string_literal: true

FactoryBot.define do
  factory :person do
    sequence(:slug) { |n| "test-person-#{n}" }
    sequence(:name) { |n| "Test Person #{n}" }
    twitter { 'my_show' }
    facebook { 'my_show' }
    wikipedia { 'test_person' }
    website { 'http://my_show.com' }
    parler { 'my_show' }
    confirmed_at { DateTime.now }
    confirmed_by { create(:user) }

    created_by { create(:user) }

    factory :confirmed_person do
      confirmed_at { DateTime.now }
      confirmed_by { create(:user) }
    end

    factory :unconfirmed_person do
      confirmed_at { nil }
      confirmed_by { nil }
    end

    factory :blocked_person do
      blocked_at { DateTime.now }
      blocked_by { create(:user) }
    end
  end
end
