# frozen_string_literal: true

FactoryBot.define do
  factory :application_settings do
    max_votes_per_show { 5 }
  end
end
