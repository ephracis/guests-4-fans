# frozen_string_literal: true

FactoryBot.define do
  factory :edit do
    name { 'name' }
    value { 'myname' }
    created_by { create(:user) }
    editable { create(:show) }
    discarded { false }
    discarded_at { nil }
    discarded_by { nil }
    applied { false }
    applied_at { nil }
    applied_by { nil }
    previous { nil }
  end
end
