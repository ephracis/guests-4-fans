# frozen_string_literal: true

FactoryBot.define do
  factory :favorite do
    user { create(:user) }
    favoritable { create(:person) }
  end
end
