# frozen_string_literal: true

FactoryBot.define do
  factory :vote do
    value { 1 }
    user { create(:user) }
    show { create(:show) }
    person { create(:person) }

    factory :anonymous_vote do
      user { nil }
      ip_address { '127.0.0.1' }
    end
  end
end
