# frozen_string_literal: true

FactoryBot.define do
  factory :rating do
    user { create(:user) }
    rateable { create(:show) }
    value { 2 }
  end
end
