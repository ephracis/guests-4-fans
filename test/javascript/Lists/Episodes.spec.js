import { shallowMount } from '@vue/test-utils'
import ListsEpisodes from 'Lists/Episodes.vue'

function mountComponent(response, options) {
  return shallowMount(ListsEpisodes, options)
}

describe('ListsEpisodes', () => {

  it('sets the correct default data', () => {
    expect(typeof ListsEpisodes.data).toBe('function')
    const defaultData = ListsEpisodes.data()
    expect(defaultData.episodes).toEqual([])
  })

  it('mounts properly', () => {
    const wrapper = mountComponent()
    expect(wrapper.vm.$data.episodes.length).toEqual(0)
  })

  it('generates links to guests', () => {
    const wrapper = mountComponent()
    let people = [
      { name: 'Person1', view_url: '/person1' },
      { name: 'Person2', view_url: '/person2' },
    ]
    let links = "<a href='/person1'>Person1</a>, <a href='/person2'>Person2</a>"
    expect(wrapper.vm.guestLinks(people)).toEqual(links)
  })
})
