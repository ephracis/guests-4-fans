import { shallowMount } from '@vue/test-utils'
import ListsShows from 'Lists/Shows.vue'

import axios from 'axios'
jest.mock('axios')

import EventBus from 'eventBus'
jest.mock('eventBus')

global.gtag = jest.fn()

function mountComponent (collection, options) {
  // mock csrf token
  document.getElementsByName = jest.fn(() => {
    return [
      { getAttribute: function () { return 'my-secret-token' } }
    ]
  })

  const defaultCollection = [
    { name: 'Show 1', id: 1, voted: false },
    { name: 'Show 2', id: 2, voted: false }
  ]

  if (!options) {
    options = {
      propsData: {
        collection: collection || defaultCollection
      },
      stubs: ['font-awesome-icon']
    }
  }
  return shallowMount(ListsShows, options)
}

describe('ListsShows', () => {
  it('sets the correct default data', () => {
    expect(typeof ListsShows.data).toBe('function')
    const defaultData = ListsShows.data()
    expect(defaultData.shows).toEqual([])
  })

  it('mounts properly', () => {
    const wrapper = mountComponent()
    expect(wrapper.vm.$data.shows.length).toEqual(2)
  })

  it('returns vote label', () => {
    const wrapper = mountComponent()
    expect(wrapper.vm.voteLabel({ title: 'My Show' })).toEqual('Vote for My Show')
  })

  it('returns vote for new label', () => {
    const wrapper = mountComponent()
    expect(wrapper.vm.voteNewLabel()).toEqual('Vote for new show')
  })

  describe('.search', () => {
    it('searches for shows', async () => {
      const wrapper = mountComponent()
      axios.get.mockClear()
      axios.get.mockResolvedValue({ data: [{ id: 420, title: 'Result 1' }] })
      return wrapper.vm.search('test-query').then(results => {
        expect(results[0].id).toEqual(420)
        expect(results[0].title).toEqual('Result 1')
      })
    })

    it('handles search errors', async () => {
      const wrapper = mountComponent()
      axios.get.mockClear()
      axios.get.mockRejectedValue('my error')
      return wrapper.vm.search('test-query').catch(error => {
        expect(error).toEqual('my error')
      })
    })
  })

  describe('.toggleVote', () => {
    it('casts a positive vote', async () => {
      const wrapper = mountComponent()
      const resp = {
        data: {
          vote: {
            show: {
              title: 'Test Show',
              id: 420,
              voted: true
            }
          },
          limit: {
            count: 1,
            max: 5,
            message: 'test message'
          }
        }
      }
      axios.post.mockClear()
      axios.post.mockResolvedValue(resp)

      return wrapper.vm.toggleVote({ id: 420, voted: false }).then(function () {
        expect(axios.post.mock.calls.length).toBe(1)
        expect(axios.post.mock.calls[0][0]).toBe('/votes.json')
        expect(axios.post.mock.calls[0][1]).toEqual({ vote: { show_id: 420, person_id: '0', value: 1 } })
        expect(wrapper.vm.$data.voting).toEqual(false)
      })
    })

    it('casts a negative vote', async () => {
      const wrapper = mountComponent()
      const resp = {
        data: {
          vote: {
            show: {
              title: 'Test Show',
              id: 420,
              voted: false
            }
          },
          limit: {
            count: 1,
            max: 5,
            message: 'test message'
          }
        }
      }
      axios.post.mockClear()
      axios.post.mockResolvedValue(resp)

      return wrapper.vm.toggleVote({ id: 420, voted: true }).then(function () {
        expect(axios.post.mock.calls.length).toBe(1)
        expect(axios.post.mock.calls[0][0]).toBe('/votes.json')
        expect(axios.post.mock.calls[0][1]).toEqual({ vote: { show_id: 420, person_id: '0', value: -1 } })
        expect(wrapper.vm.$data.voting).toEqual(false)
      })
    })
  })

  describe('.voteForNew', () => {
    it('casts a vote', async () => {
      const wrapper = mountComponent()
      await wrapper.vm.$nextTick()

      const resp = {
        data: {
          vote: {
            show: {
              title: 'Test Show',
              id: 420,
              voted: false
            }
          }
        }
      }
      axios.post.mockClear()
      axios.post.mockResolvedValue(resp)

      expect(wrapper.vm.$data.shows.length).toEqual(2)
      return wrapper.vm.voteForNew(1).then(function() {
        expect(wrapper.vm.$data.shows.length).toEqual(3)
        expect(wrapper.vm.$data.shows[2]['title']).toEqual('Test Show')
        expect(wrapper.vm.$data.shows[2]['id']).toEqual(420)
      })
    })

    it('emit success message', async () => {
      const wrapper = mountComponent(null, { propsData: { url: 'shows.json', personId: '42' }, stubs: ['font-awesome-icon'] })
      await wrapper.vm.$nextTick()

      const resp = {
        data: {
          vote: {
            show: {
              name: 'Test Show',
              id: 420,
              voted: false
            }
          },
          limit: {
            count: 1,
            max: 5,
            message: 'test message'
          }
        }
      }
      axios.post.mockClear()
      axios.post.mockResolvedValue(resp)
      EventBus.$emit = jest.fn()

      return wrapper.vm.voteForNew(1).then(function() {
        expect(EventBus.$emit.mock.calls[0][0]).toEqual('alert')
        expect(EventBus.$emit.mock.calls[0][1]).toEqual({ message: 'test message', type: 'success' })
      })
    })

    it('handles errors', async () => {
      const wrapper = mountComponent()
      await wrapper.vm.$nextTick()

      axios.post.mockRejectedValue('my error')
      EventBus.$emit = jest.fn()

      expect(wrapper.vm.$data.shows.length).toEqual(2)
      return wrapper.vm.voteForNew(1).then(function() {
        expect(wrapper.vm.$data.shows.length).toEqual(2)
        expect(EventBus.$emit.mock.calls[0][0]).toEqual('alert')
        expect(EventBus.$emit.mock.calls[0][1]).toEqual({ message: 'my error', type: 'danger' })
      })
    })

    it('handles error list', async () => {
      const wrapper = mountComponent()
      await wrapper.vm.$nextTick()

      axios.post.mockClear()
      axios.post.mockRejectedValue({ response: { data: ['error1', 'error2'] } })

      EventBus.$emit = jest.fn()
      return wrapper.vm.voteForNew(1).then(function() {
        expect(EventBus.$emit.mock.calls.length).toBe(2)
        expect(EventBus.$emit.mock.calls[0][0]).toEqual('alert')
        expect(EventBus.$emit.mock.calls[0][1]).toEqual({ message: 'error1', type: 'danger' })
        expect(EventBus.$emit.mock.calls[1][0]).toEqual('alert')
        expect(EventBus.$emit.mock.calls[1][1]).toEqual({ message: 'error2', type: 'danger' })
      })
    })

    it('handles limit reached', async () => {
      const wrapper = mountComponent()
      await wrapper.vm.$nextTick()

      axios.post.mockClear()
      axios.post.mockRejectedValue({
        response: {
          data: {
            error: 'limit_reached',
            message: 'error message'
          }
        }
      })

      EventBus.$emit = jest.fn()
      return wrapper.vm.voteForNew(1).then(function() {
        expect(EventBus.$emit.mock.calls.length).toBe(1)
        expect(EventBus.$emit.mock.calls[0][0]).toEqual('alert')
        expect(EventBus.$emit.mock.calls[0][1]).toEqual({ message: 'error message', type: 'danger' })
      })
    })
  })

  describe('.vote', () => {
    it('casts a vote', async () => {
      const wrapper = mountComponent(null, { propsData: { url: 'shows.json', personId: '42' }, stubs: ['font-awesome-icon'] })
      await wrapper.vm.$nextTick()

      const resp = {
        data: {
          vote: {
            show: {
              name: 'Test Show',
              id: 420,
              voted: false
            }
          }
        }
      }
      axios.post.mockClear()
      axios.post.mockResolvedValue(resp)

      return wrapper.vm.vote({ id: 420 }, 1).then(function () {
        expect(axios.post.mock.calls.length).toBe(1)
        expect(axios.post.mock.calls[0][0]).toBe('/votes.json')
        expect(axios.post.mock.calls[0][1]).toEqual({ vote: { show_id: 420, person_id: '42', value: 1 } })
        expect(wrapper.vm.$data.voting).toEqual(false)
      })
    })

    it('emit success message', async () => {
      const wrapper = mountComponent(null, { propsData: { url: 'shows.json', personId: '42' }, stubs: ['font-awesome-icon'] })
      await wrapper.vm.$nextTick()

      const resp = {
        data: {
          vote: {
            show: {
              name: 'Test Show',
              id: 420,
              voted: false
            }
          },
          limit: {
            count: 1,
            max: 5,
            message: 'test message'
          }
        }
      }
      axios.post.mockClear()
      axios.post.mockResolvedValue(resp)
      EventBus.$emit = jest.fn()

      return wrapper.vm.vote({ id: 420 }, 1).then(function () {
        expect(EventBus.$emit.mock.calls.length).toBe(1)
        expect(EventBus.$emit.mock.calls[0][0]).toEqual('alert')
        expect(EventBus.$emit.mock.calls[0][1]).toEqual({ message: 'test message', type: 'success' })
      })
    })

    it('handles errors', async () => {
      const wrapper = mountComponent()
      await wrapper.vm.$nextTick()

      axios.post.mockClear()
      axios.post.mockRejectedValue('my error')
      EventBus.$emit = jest.fn()

      expect(wrapper.vm.$data.shows.length).toEqual(2)
      return wrapper.vm.vote(420, 1).then(function () {
        expect(wrapper.vm.$data.shows.length).toEqual(2)
        expect(EventBus.$emit.mock.calls[0][0]).toEqual('alert')
        expect(EventBus.$emit.mock.calls[0][1]).toEqual({ message: 'my error', type: 'danger' })
      })
    })

    it('handles limit reached', async () => {
      const wrapper = mountComponent()
      await wrapper.vm.$nextTick()

      axios.post.mockClear()
      axios.post.mockRejectedValue({
        response: {
          data: {
            error: 'limit_reached',
            message: 'error message'
          }
        }
      })

      EventBus.$emit = jest.fn()

      expect(wrapper.vm.$data.shows.length).toEqual(2)
      return wrapper.vm.vote(420, 1).then(function () {
        expect(wrapper.vm.$data.shows.length).toEqual(2)
        expect(EventBus.$emit.mock.calls.length).toBe(1)
        expect(EventBus.$emit.mock.calls[0][0]).toEqual('alert')
        expect(EventBus.$emit.mock.calls[0][1]).toEqual({ message: 'error message', type: 'danger' })
      })
    })
  })
})
