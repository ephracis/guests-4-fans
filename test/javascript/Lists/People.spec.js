import { shallowMount } from '@vue/test-utils'
import ListsPeople from 'Lists/People.vue'

import axios from 'axios'
jest.mock('axios')

import EventBus from 'eventBus'
jest.mock('eventBus')

global.gtag = jest.fn()

function mountComponent (collection, options) {
  // mock csrf token
  document.getElementsByName = jest.fn(() => {
    return [
      { getAttribute: function () { return 'my-secret-token' } }
    ]
  })

  const defaultCollection = [
    { name: 'Person 1', id: 1, voted: false },
    { name: 'Person 2', id: 2, voted: false }
  ]

  if (!options) {
    options = {
      propsData: {
        collection: collection || defaultCollection
      },
      stubs: ['font-awesome-icon']
    }
  }
  return shallowMount(ListsPeople, options)
}

describe('ListsPeople', () => {
  it('sets the correct default data', () => {
    expect(typeof ListsPeople.data).toBe('function')
    const defaultData = ListsPeople.data()
    expect(defaultData.people).toEqual([])
  })

  it('mounts properly', () => {
    const wrapper = mountComponent()
    expect(wrapper.vm.$data.people.length).toEqual(2)
  })

  it('returns favorite label', () => {
    const wrapper = mountComponent()
    expect(wrapper.vm.favoriteLabel({ name: 'My Person' })).toEqual('Mark My Person as favorite')
  })

  it('returns vote label', () => {
    const wrapper = mountComponent()
    expect(wrapper.vm.voteLabel({ name: 'My Person' })).toEqual('Vote for My Person')
  })

  it('returns vote for new label', () => {
    const wrapper = mountComponent()
    expect(wrapper.vm.voteNewLabel()).toEqual('Vote for new person')
  })

  describe('.search', () => {
    it('searches for people', async () => {
      const wrapper = mountComponent()
      axios.get.mockClear()
      axios.get.mockResolvedValue({ data: [{ id: 420, name: 'Result 1' }] })
      return wrapper.vm.search('test-query').then(results => {
        expect(results[0].id).toEqual(420)
        expect(results[0].name).toEqual('Result 1')
      })
    })

    it('handles search errors', async () => {
      const wrapper = mountComponent()
      axios.get.mockClear()
      axios.get.mockRejectedValue('my error')
      return wrapper.vm.search('test-query').catch(error => {
        expect(error).toEqual('my error')
      })
    })
  })

  describe('.toggleVote', () => {
    it('casts a positive vote', async () => {
      const wrapper = mountComponent()
      const resp = {
        data: {
          vote: {
            person: {
              name: 'Test Person',
              id: 420,
              voted: true
            }
          },
          limit: {
            count: 1,
            max: 5,
            message: 'test message'
          }
        }
      }
      axios.post.mockClear()
      axios.post.mockResolvedValue(resp)

      return wrapper.vm.toggleVote({ id: 420, voted: false }).then(function() {
        expect(axios.post.mock.calls.length).toBe(1)
        expect(axios.post.mock.calls[0][0]).toBe('/votes.json')
        expect(axios.post.mock.calls[0][1]).toEqual({ vote: { person_id: 420, show_id: '0', value: 1 } })
        expect(wrapper.vm.$data.voting).toEqual(false)
      })
    })

    it('casts a negative vote', async () => {
      const wrapper = mountComponent()
      const resp = {
        data: {
          vote: {
            person: {
              name: 'Test Person',
              id: 420,
              voted: false
            }
          },
          limit: {
            count: 1,
            max: 5,
            message: 'test message'
          }
        }
      }
      axios.post.mockClear()
      axios.post.mockResolvedValue(resp)

      return wrapper.vm.toggleVote({ id: 420, voted: true }).then(function() {
        expect(axios.post.mock.calls.length).toBe(1)
        expect(axios.post.mock.calls[0][0]).toBe('/votes.json')
        expect(axios.post.mock.calls[0][1]).toEqual({ vote: { person_id: 420, show_id: '0', value: -1 } })
        expect(wrapper.vm.$data.voting).toEqual(false)
      })
    })
  })

  describe('.voteForNew', () => {
    it('casts a vote', async () => {
      const wrapper = mountComponent()
      await wrapper.vm.$nextTick()

      const resp = {
        data: {
          vote: {
            person: {
              name: 'Test Person',
              id: 420,
              voted: false
            }
          }
        }
      }
      axios.post.mockClear()
      axios.post.mockResolvedValue(resp)

      expect(wrapper.vm.$data.people.length).toEqual(2)
      return wrapper.vm.voteForNew(1).then(function() {
        expect(wrapper.vm.$data.people.length).toEqual(3)
        expect(wrapper.vm.$data.people[2]['name']).toEqual('Test Person')
        expect(wrapper.vm.$data.people[2]['id']).toEqual(420)
      })
    })

    it('emit success message', async () => {
      const wrapper = mountComponent(null, { propsData: { url: 'people.json', showId: '42' }, stubs: ['font-awesome-icon'] })
      await wrapper.vm.$nextTick()

      const resp = {
        data: {
          vote: {
            person: {
              name: 'Test Person',
              id: 420,
              voted: false
            }
          },
          limit: {
            count: 1,
            max: 5,
            message: 'test message'
          }
        }
      }
      axios.post.mockClear()
      axios.post.mockResolvedValue(resp)
      EventBus.$emit = jest.fn()

      return wrapper.vm.voteForNew(1).then(function() {
        expect(EventBus.$emit.mock.calls[0][0]).toEqual('alert')
        expect(EventBus.$emit.mock.calls[0][1]).toEqual({ message: 'test message', type: 'success' })
      })
    })

    it('handles errors', async () => {
      const wrapper = mountComponent()
      await wrapper.vm.$nextTick()

      axios.post.mockRejectedValue('my error')
      EventBus.$emit = jest.fn()

      expect(wrapper.vm.$data.people.length).toEqual(2)
      return wrapper.vm.voteForNew(1).then(function() {
        expect(wrapper.vm.$data.people.length).toEqual(2)
        expect(EventBus.$emit.mock.calls[0][0]).toEqual('alert')
        expect(EventBus.$emit.mock.calls[0][1]).toEqual({ message: 'my error', type: 'danger' })
      })
    })

    it('handles error list', async () => {
      const wrapper = mountComponent()
      await wrapper.vm.$nextTick()

      axios.post.mockClear()
      axios.post.mockRejectedValue({ response: { data: ['error1', 'error2'] } })

      EventBus.$emit = jest.fn()
      return wrapper.vm.voteForNew(1).then(function() {
        expect(EventBus.$emit.mock.calls.length).toBe(2)
        expect(EventBus.$emit.mock.calls[0][0]).toEqual('alert')
        expect(EventBus.$emit.mock.calls[0][1]).toEqual({ message: 'error1', type: 'danger' })
        expect(EventBus.$emit.mock.calls[1][0]).toEqual('alert')
        expect(EventBus.$emit.mock.calls[1][1]).toEqual({ message: 'error2', type: 'danger' })
      })
    })

    it('handles limit reached', async () => {
      const wrapper = mountComponent()
      await wrapper.vm.$nextTick()

      axios.post.mockClear()
      axios.post.mockRejectedValue({
        response: {
          data: {
            error: 'limit_reached',
            message: 'error message'
          }
        }
      })

      EventBus.$emit = jest.fn()
      return wrapper.vm.voteForNew(1).then(function() {
        expect(EventBus.$emit.mock.calls.length).toBe(1)
        expect(EventBus.$emit.mock.calls[0][0]).toEqual('alert')
        expect(EventBus.$emit.mock.calls[0][1]).toEqual({ message: 'error message', type: 'danger' })
      })
    })
  })

  describe('.vote', () => {
    it('casts a vote', async () => {
      const wrapper = mountComponent(null, { propsData: { url: 'people.json', showId: '42' }, stubs: ['font-awesome-icon'] })
      await wrapper.vm.$nextTick()

      const resp = {
        data: {
          vote: {
            person: {
              name: 'Test Person',
              id: 420,
              voted: false
            }
          }
        }
      }
      axios.post.mockClear()
      axios.post.mockResolvedValue(resp)

      return wrapper.vm.vote({ id: 420 }, 1).then(function () {
        expect(axios.post.mock.calls.length).toBe(1)
        expect(axios.post.mock.calls[0][0]).toBe('/votes.json')
        expect(axios.post.mock.calls[0][1]).toEqual({ vote: { person_id: 420, show_id: '42', value: 1 } })
        expect(wrapper.vm.$data.voting).toEqual(false)
      })
    })

    it('emit success message', async () => {
      const wrapper = mountComponent(null, { propsData: { url: 'people.json', showId: '42' }, stubs: ['font-awesome-icon'] })
      await wrapper.vm.$nextTick()

      const resp = {
        data: {
          vote: {
            person: {
              name: 'Test Person',
              id: 420,
              voted: false
            }
          },
          limit: {
            count: 1,
            max: 5,
            message: 'test message'
          }
        }
      }
      axios.post.mockClear()
      axios.post.mockResolvedValue(resp)
      EventBus.$emit = jest.fn()

      return wrapper.vm.vote({ id: 420 }, 1).then(function() {
        expect(EventBus.$emit.mock.calls.length).toBe(1)
        expect(EventBus.$emit.mock.calls[0][0]).toEqual('alert')
        expect(EventBus.$emit.mock.calls[0][1]).toEqual({ message: 'test message', type: 'success' })
      })
    })

    it('handles errors', async () => {
      const wrapper = mountComponent()
      await wrapper.vm.$nextTick()

      axios.post.mockClear()
      axios.post.mockRejectedValue('my error')
      EventBus.$emit = jest.fn()

      expect(wrapper.vm.$data.people.length).toEqual(2)
      return wrapper.vm.vote(420, 1).then(function() {
        expect(wrapper.vm.$data.people.length).toEqual(2)
        expect(EventBus.$emit.mock.calls[0][0]).toEqual('alert')
        expect(EventBus.$emit.mock.calls[0][1]).toEqual({ message: 'my error', type: 'danger' })
      })
    })

    it('handles limit reached', async () => {
      const wrapper = mountComponent()
      await wrapper.vm.$nextTick()

      axios.post.mockClear()
      axios.post.mockRejectedValue({
        response: {
          data: {
            error: 'limit_reached',
            message: 'error message'
          }
        }
      })

      EventBus.$emit = jest.fn()

      expect(wrapper.vm.$data.people.length).toEqual(2)
      return wrapper.vm.vote(420, 1).then(function() {
        expect(wrapper.vm.$data.people.length).toEqual(2)
        expect(EventBus.$emit.mock.calls.length).toBe(1)
        expect(EventBus.$emit.mock.calls[0][0]).toEqual('alert')
        expect(EventBus.$emit.mock.calls[0][1]).toEqual({ message: 'error message', type: 'danger' })
      })
    })
  })

  describe('.favorite', () => {
    it('mark as favorite', async () => {
      const wrapper = mountComponent(null, { propsData: { url: 'people.json', show_id: 42 }, stubs: ['font-awesome-icon'] })
      await wrapper.vm.$nextTick()

      const resp = {
        data: {
          person: {
            name: 'Test Person',
            id: 420,
            favorited: false,
            voted: false
          }
        }
      }
      axios.post.mockClear()
      axios.post.mockResolvedValue(resp)

      return wrapper.vm.favorite({ slug: 'mr-test' }, true).then(function () {
        expect(axios.post.mock.calls.length).toBe(1)
        expect(axios.post.mock.calls[0][0]).toBe('/people/mr-test/favorite.json')
        expect(axios.post.mock.calls[0][1]).toEqual({ value: true })
        expect(wrapper.vm.$data.favoriting).toEqual(false)
      })
    })

    it('handles errors', async () => {
      const wrapper = mountComponent()
      await wrapper.vm.$nextTick()

      axios.post.mockClear()
      axios.post.mockRejectedValue('my error')
      EventBus.$emit = jest.fn()

      expect(wrapper.vm.$data.people.length).toEqual(2)
      return wrapper.vm.favorite(420, true).then(function () {
        expect(wrapper.vm.$data.people.length).toEqual(2)
        expect(EventBus.$emit.mock.calls[0][0]).toEqual('alert')
        expect(EventBus.$emit.mock.calls[0][1]).toEqual({ message: 'my error', type: 'danger' })
      })
    })
  })
})
