import { shallowMount } from '@vue/test-utils'
import ShowList from 'ShowList.vue'

import axios from 'axios'
jest.mock('axios')

import EventBus from 'eventBus'
jest.mock('eventBus')

global.gtag = jest.fn()

function mountComponent(response, options) {

  // mock axios request
  const defaultResponse = [
    { name: 'Show 1', id: 1, voted: false },
    { name: 'Show 2', id: 2, voted: false }
  ]
  const resp = { data: response || defaultResponse }
  axios.get.mockResolvedValue(resp)

  // mock csrf token
  document.getElementsByName = jest.fn(() => {
    return [
      { getAttribute: function() { return 'my-secret-token'} }
    ]
  })

  if (!options) {
    options = { propsData: { url: 'http://example.com' }, stubs: ['font-awesome-icon', 'lists-shows'] }
  }

  return shallowMount(ShowList, options)
}

describe('ShowList', () => {
  it('has a created hook', () => {
    expect(typeof ShowList.created).toBe('function')
  })

  it('sets the correct default data', () => {
    expect(typeof ShowList.data).toBe('function')
    const defaultData = ShowList.data()
    expect(defaultData.loading).toEqual(true)
    expect(defaultData.shows).toEqual([])
    expect(defaultData.error).toEqual('')
  })

  it('mounts properly', () => {
    const wrapper = mountComponent()
    expect(wrapper.vm.$data.shows.length).toEqual(0)
  })

  it('displays error message', async () => {
    const wrapper = mountComponent(null, { propsData: { url: '' }, stubs: ['font-awesome-icon', 'lists-shows'] })
    await wrapper.vm.$nextTick()
    wrapper.vm.$data.error = 'test error'
    await wrapper.vm.$nextTick()
    expect(wrapper.text()).toContain('test error')
  })

  describe('getting show list', () => {

    it('sets shows from URL', async () => {
      const shows = [ { id: 1, name: 'Test Show', votes: { total: { up: 0, down: 0 }, mine: { up: 0, down: 0 } } } ]
      const url = 'https://example.com/shows.json'
      const wrapper = mountComponent(shows, { propsData: { url: url }, stubs: ['font-awesome-icon', 'lists-shows'] })
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.$data.shows.length).toEqual(1)
      expect(wrapper.vm.$data.shows[0]['id']).toEqual(1)
      expect(wrapper.vm.$data.shows[0]['name']).toEqual('Test Show')
    })

    it('handles API errors', async () => {
      axios.get.mockRejectedValue('my error')
      console.error = jest.fn()

      const url = 'https://example.com/shows.json'
      const wrapper = shallowMount(ShowList, { propsData: { url: url }, stubs: ['font-awesome-icon', 'lists-shows'] })
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.$data.shows.length).toEqual(0)
      expect(wrapper.vm.$data.error).toEqual('Error fetching shows: my error')
    })
  })
})
