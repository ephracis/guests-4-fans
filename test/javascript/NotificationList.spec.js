import { shallowMount } from '@vue/test-utils'
import NotificationList from 'NotificationList.vue'

function mountComponent(props, options) {

  const defaultProps = {
    notifications: [
      { title: 'Notification 1', id: 1, content: 'Some content', image: 'img.jpg', read: false, created_ago: '1 second' },
      { title: 'Notification 2', id: 2, content: 'Some content', image: 'img.jpg', read: true, created_ago: '1 second' }
    ]
  }

  if (!options) {
    options = { propsData: props || defaultProps, stubs: ['font-awesome-icon'] }
  }

  return shallowMount(NotificationList, options)
}

describe('NotificationList', () => {

  it('mounts properly', () => {
    const wrapper = mountComponent()
    expect(wrapper.find('.list-group').exists()).toBe(true)
    expect(wrapper.find('.list-group .list-group-item').text()).toContain('Notification 1')
  })

  it('mounts without properties', () => {
    const wrapper = mountComponent({})
    expect(wrapper.find('.list-group').exists()).toBe(true)
    expect(wrapper.find('.list-group .list-group-item').text()).toContain('You have no notifications')
  })

  describe('.removeNotification', () => {
    it('emits event removeNotification', async () => {
      const wrapper = mountComponent()
      wrapper.vm.removeNotification('test')
      await wrapper.vm.$nextTick()
      expect(wrapper.emitted().removeNotification).toBeTruthy()
      expect(wrapper.emitted().removeNotification.length).toBe(1)
      expect(wrapper.emitted().removeNotification[0]).toEqual(['test'])
    })
  })
})
