import { shallowMount } from '@vue/test-utils'
import Merge from 'Merge.vue'
import Axios from 'axios'
import EventBus from 'eventBus'

jest.mock('axios')

function mountComponent (response, options) {
  // mock axios request
  const defaultResponse = { name: 'Person 1', id: 1 }
  const resp = { data: response || defaultResponse }
  Axios.get.mockResolvedValue(resp)

  // mock csrf token
  document.getElementsByName = jest.fn(() => {
    return [
      { getAttribute: function () { return 'my-secret-token'} }
    ]
  })

  if (!options) {
    options = { propsData: { url: 'http://example.com' }, stubs: ['autocomplete'] }
  }

  return shallowMount(Merge, options)
}

describe('Merge', () => {
  it('has a mounted hook', () => {
    expect(typeof Merge.mounted).toBe('function')
  })

  it('sets the correct default data', () => {
    expect(typeof Merge.data).toBe('function')
    const defaultData = Merge.data()
    expect(defaultData.resource).toEqual(null)
  })

  it('mounts properly', () => {
    const wrapper = mountComponent()
    expect(wrapper.vm.$data.resource).toEqual(null)
  })

  describe('.mounted', () => {
    it('fetches person data', async () => {
      const wrapper = mountComponent()
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.$data.resource).toEqual({ id: 1, name: 'Person 1' })
    })

    it('handles API errors', async () => {
      Axios.get.mockRejectedValue('person error')
      EventBus.$emit = jest.fn()

      const props = { url: '/people/1.json' }
      const wrapper = shallowMount(Merge, { propsData: props, stubs: ['autocomplete'] })
      await wrapper.vm.$nextTick()

      expect(wrapper.vm.$data.resource).toEqual(null)
      expect(wrapper.vm.$data.error).toEqual('Error fetching resource: person error')
    })
  })

  describe('.search', () => {
    it('searches for people', async () => {
      const wrapper = mountComponent()
      await wrapper.vm.$nextTick()
      wrapper.vm.search('query')
    })
    it('handles API errors', async () => {
      const wrapper = mountComponent()
      await wrapper.vm.$nextTick()
      Axios.get.mockRejectedValue('search error')
      expect(wrapper.vm.search('query')).rejects.toEqual('search error')
    })
  })

  describe('.select', () => {
    it('selects person as original', async () => {
      const wrapper = mountComponent()
      await wrapper.vm.$nextTick()
      const person = { name: 'Person X', id: 1 }
      wrapper.vm.select({ item: person })
      expect(wrapper.vm.$data.original.name).toEqual('Person X')
    })
  })

  describe('.clear', () => {
    it('removes person as original', async () => {
      const wrapper = mountComponent()
      await wrapper.vm.$nextTick()
      const person = { name: 'Person X', id: 42 }
      wrapper.vm.$data.original = person
      wrapper.vm.clear()
      expect(wrapper.vm.$data.original).toEqual(null)
    })
  })
})
