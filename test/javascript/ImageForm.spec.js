global.window = window
global.$ = function () {
  return { tab () {} }
}

jest.useFakeTimers('legacy')

import { shallowMount } from '@vue/test-utils'
import ImageForm from 'ImageForm.vue'

describe('ImageForm', () => {
  it('has a mounted hook', () => {
    expect(typeof ImageForm.mounted).toBe('function')
  })

  it('mounts properly', () => {
    const wrapper = shallowMount(ImageForm)
    expect(wrapper.find('input.custom-file-input').exists()).toBe(true)
  })

  it('mounts with image', () => {
    const wrapper = shallowMount(ImageForm, { propsData: { image: 'mypic.jpg' } })
    expect(wrapper.find('input.custom-file-input').exists()).toBe(true)
    expect(wrapper.vm.$data.imageData).toBe('mypic.jpg')
  })

  describe('.mode', () => {
    it('switches to url', async () => {
      const wrapper = shallowMount(ImageForm)
      expect(wrapper.find('input#show_image_file').exists()).toBe(true)
      expect(wrapper.find('input#show_image_url').exists()).toBe(false)
      wrapper.vm.$data.mode = 'url'
      await wrapper.vm.$nextTick()
      expect(wrapper.find('input#show_image_file').exists()).toBe(false)
      expect(wrapper.find('input#show_image_url').exists()).toBe(true)
    })
  })

  describe('.fileChanged', () => {
    it('loads image', () => {
      const wrapper = shallowMount(ImageForm)
      const event = { target: { files: ['mypic.jpg'], value: 'C:\\fakepath\\mypic.jpg' } }

      FileReader.prototype.readAsDataURL = jest.fn()
      FileReader.prototype.addEventListener = jest.fn()

      wrapper.vm.fileChanged(event)

      expect(FileReader.prototype.readAsDataURL.mock.calls.length).toBe(1)
      expect(FileReader.prototype.readAsDataURL.mock.calls[0][0]).toBe('mypic.jpg')

      expect(FileReader.prototype.addEventListener.mock.calls.length).toBe(1)
      expect(FileReader.prototype.addEventListener.mock.calls[0][0]).toBe('load')

      // do this to mock `this.result` inside anon func
      let mockObject = {
        func: FileReader.prototype.addEventListener.mock.calls[0][1],
        result: 'mypic.jpg'
      }
      expect(wrapper.vm.$data.imageData).toBe('')
      mockObject.func()
      expect(wrapper.vm.$data.imageData).toBe('mypic.jpg')
    })

    it('falls back to null', () => {
      const wrapper = shallowMount(ImageForm)
      const event = { target: { files: [], value: 'C:\\fakepath\\mypic.jpg' } }
      wrapper.vm.$data.imageData = 'mypic.jpg'

      FileReader.prototype.readAsDataURL = jest.fn()
      FileReader.prototype.addEventListener = jest.fn()

      expect(wrapper.vm.$data.imageData).toBe('mypic.jpg')
      wrapper.vm.fileChanged(event)
      expect(wrapper.vm.$data.imageData).toBeNull()
    })
  })

  describe('.imageURL', () => {

    it('skips update if typing', () => {
      const wrapper = shallowMount(ImageForm)
      wrapper.vm.loadingImageURL = true
      wrapper.vm.imageURL = 'mypic.jpg'
      expect(wrapper.vm.$data.imageData).toBeNull()
      expect(setTimeout).toHaveBeenCalledTimes(0)
    })
    
    it('sets imageData after 1s', async () => {
      const wrapper = shallowMount(ImageForm)

      wrapper.vm.loadingImageURL = false
      wrapper.vm.imageURL = 'mypic.jpg'
      await wrapper.vm.$nextTick()

      expect(wrapper.vm.$data.imageData).toBeNull()
      expect(setTimeout).toHaveBeenCalledTimes(1)
      expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 1000)

      jest.runAllTimers()
      
      expect(wrapper.vm.$data.imageData).toBe('mypic.jpg')
    })
  })
})
