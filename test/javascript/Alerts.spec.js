global.window = window
global.$ = function () {
  return { tab () {} }
}

jest.useFakeTimers('legacy')

import { shallowMount } from '@vue/test-utils'
import Alerts from 'Alerts.vue'

describe('Alerts', () => {
  it ('has a created hook', () => {
    expect(typeof Alerts.created).toBe('function')
  })

  it('sets the correct default data', () => {
    expect(typeof Alerts.data).toBe('function')
    const defaultData = Alerts.data()
    expect(defaultData.alerts).toEqual([])
  })

  it('mounts properly', () => {
    const wrapper = shallowMount(Alerts)
    expect(wrapper.vm.$data.alerts).toEqual([])
  })

  describe('.alertClass', () => {
    it('returns a computed class', () => {
      const wrapper = shallowMount(Alerts)
      expect(wrapper.vm.alertClass({ type: 'mytype'})).toEqual('alert-mytype')
    })
  })

  describe('.receiveAlert', () => {
    it('pushes alert and removes after delay', () => {
      const wrapper = shallowMount(Alerts)
      expect(wrapper.vm.$data.alerts.length).toEqual(0)
      wrapper.vm.receiveAlert({ message: 'my alert', type: 'danger' })
      expect(wrapper.vm.$data.alerts.length).toEqual(1)
      expect(wrapper.vm.$data.alerts[0].message).toEqual('my alert')
      expect(wrapper.vm.$data.alerts[0].type).toEqual('danger')
      expect(setTimeout).toHaveBeenCalledTimes(1)
      expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 3000)
      jest.runAllTimers()
      expect(wrapper.vm.$data.alerts.length).toEqual(0)
    })
  })
})
