import { shallowMount } from '@vue/test-utils'
import SourceList from 'SourceList.vue'

function mountComponent(props = {}, options = null) {

  if (!options) {
    options = { propsData: props, stubs: ['font-awesome-icon'] }
  }

  return shallowMount(SourceList, options)
}

describe('SourceList', () => {

  it('mounts properly', () => {
    const wrapper = mountComponent()
    expect(typeof wrapper.vm.sourceIcon).toBe('function')
  })

  describe('.sourceIcon', () => {
    it('gets icon for Spotify', async () => {
      const wrapper = mountComponent()
      expect(wrapper.vm.sourceIcon({ kind: 'spotify' })).toEqual(['fab', 'spotify'])
    })

    it('gets icon for YouTube', async () => {
      const wrapper = mountComponent()
      expect(wrapper.vm.sourceIcon({ kind: 'youtube' })).toEqual(['fab', 'youtube'])
    })

    it('gets unknown icon', async () => {
      const wrapper = mountComponent()
      expect(wrapper.vm.sourceIcon({ kind: 'invalid' })).toEqual(['far', 'question-circle'])
    })
  })
})
