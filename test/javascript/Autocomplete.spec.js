import { shallowMount } from '@vue/test-utils'
import Autocomplete from 'Autocomplete.vue'

const wrapper = shallowMount(Autocomplete)

function getMountedComponent(Component, propsData) {
  return shallowMount(Component, {
    propsData
  })
}

describe('Autocomplete', () => {
  it('has a mounted hook', () => {
    expect(typeof Autocomplete.mounted).toBe('function')
  })

  it('has a destroyed hook', () => {
    expect(typeof Autocomplete.destroyed).toBe('function')
  })

  it('sets the correct default data', () => {
    expect(typeof Autocomplete.data).toBe('function')
    const defaultData = Autocomplete.data()
    expect(defaultData.isOpen).toBe(false)
    expect(defaultData.value).toEqual({})
    expect(defaultData.arrowCounter).toBe(0)
  })

  it('correctly sets the addEventListener when mounted', () => {
    document.addEventListener = jest.fn()
    const autocomplete = getMountedComponent(Autocomplete)
    expect(document.addEventListener).toHaveBeenCalledTimes(1)
  })

  it('correctly removes the addEventListener when destroyed', () => {
    document.removeEventListener = jest.fn()
    const autocomplete = getMountedComponent(Autocomplete)
    autocomplete.destroy()
    expect(document.removeEventListener).toHaveBeenCalledTimes(1)
  })

  it('renders the correct message', () => {
    expect(wrapper.text()).toContain('No matches found.')
  })

  it('can be mounted with initial query', async () => {
    function getResults(query) {
      return new Promise((resolve, reject) => {
        expect(autocomplete.vm.$data.isLoading).toBe(true)
        resolve([{ name: query }])
      })
    }
    const autocomplete = getMountedComponent(Autocomplete, { query: 'foo', getResults: getResults })
    await autocomplete.vm.$nextTick()
    expect.assertions(4)
    return autocomplete.vm.onChange().then(data => {
      expect(autocomplete.vm.$data.displayValue).toEqual('foo')
      expect(autocomplete.vm.$data.results[0]).toEqual({ name: 'foo' })
      expect(autocomplete.vm.$data.isLoading).toBe(false)
    })
  })

  describe('.onChange', () => {
    it('skips for small queries', () => {
      const autocomplete = getMountedComponent(Autocomplete)
      let ret = autocomplete.vm.onChange()
      expect(ret).toBeUndefined()
    })
    it('handles when result is array', () => {
      function getResults() {
        return new Promise((resolve, reject) => {
          expect(autocomplete.vm.$data.isLoading).toBe(true)
          resolve([{ name: 'one' }, { name: 'two' }])
        })
      }
      const autocomplete = getMountedComponent(Autocomplete, { getResults: getResults })
      autocomplete.vm.$data.displayValue = 'two'
      expect.assertions(4)
      return autocomplete.vm.onChange().then(data => {
        expect(autocomplete.vm.$data.results[0]).toEqual({ name: 'one' })
        expect(autocomplete.vm.$data.results[1]).toEqual({ name: 'two' })
        expect(autocomplete.vm.$data.isLoading).toBe(false)
      })
    })
    it('handles when result is object', () => {
      function getResults() {
        return new Promise((resolve, reject) => {
          expect(autocomplete.vm.$data.isLoading).toBe(true)
          resolve({ numbers: [{ name: 'one' }, { name: 'two' }], animals: [{ name: 'cat' }] })
        })
      }
      const autocomplete = getMountedComponent(Autocomplete, { getResults: getResults })
      autocomplete.vm.$data.displayValue = 'two'
      expect.assertions(5)
      return autocomplete.vm.onChange().then(data => {
        expect(autocomplete.vm.$data.results.numbers[0]).toEqual({ name: 'one' })
        expect(autocomplete.vm.$data.results.numbers[1]).toEqual({ name: 'two' })
        expect(autocomplete.vm.$data.results.animals[0]).toEqual({ name: 'cat' })
        expect(autocomplete.vm.$data.isLoading).toBe(false)
      });
    })
    it('resets value when not matching displayValue', () => {
      function getResults() {
        return new Promise((resolve, reject) => {
          expect(autocomplete.vm.$data.isLoading).toBe(true)
          resolve([{ name: 'one' }, { name: 'two' }])
        })
      }
      const autocomplete = getMountedComponent(Autocomplete, { getResults: getResults })
      autocomplete.vm.$data.value = { name: 'test' }
      autocomplete.vm.$data.displayValue = 'test2'
      expect.assertions(2)
      return autocomplete.vm.onChange().then(data => {
        expect(autocomplete.vm.$data.value).toEqual({ name: 'test2' })
      });
    })
    it('keeps value when matching displayValue', () => {
      function getResults() {
        return new Promise((resolve, reject) => {
          expect(autocomplete.vm.$data.isLoading).toBe(true)
          resolve([{ name: 'one' }, { name: 'two' }])
        })
      }
      const autocomplete = getMountedComponent(Autocomplete, { getResults: getResults })
      autocomplete.vm.$data.value = { name: 'test' }
      autocomplete.vm.$data.displayValue = 'test'
      expect.assertions(2)
      return autocomplete.vm.onChange().then(data => {
        expect(autocomplete.vm.$data.value).toEqual({ name: 'test' })
      });
    })
    it('handlers an error', () => {
      function getResults() {
        return new Promise((resolve, reject) => {
          expect(autocomplete.vm.$data.isLoading).toBe(true)
          reject('my error')
        })
      }
      const autocomplete = getMountedComponent(Autocomplete, { getResults: getResults })

      expect.assertions(4)
      autocomplete.vm.$data.displayValue = 'foo'
      return autocomplete.vm.onChange().then(data => {
        expect(autocomplete.vm.$data.results).toEqual([])
        expect(autocomplete.vm.$data.isLoading).toBe(false)
        expect(autocomplete.vm.$data.error).toBe('my error')
      });
    })
  })

  describe('.setResult', () => {
    it('sets a string', () => {
      const result = 'test'
      wrapper.vm.setResult(result)
      expect(wrapper.vm.$data.value).toBe('test')
      expect(wrapper.vm.displayValue).toBe('test')
      expect(wrapper.vm.isOpen).toBe(false)
    })
    it('sets an object', () => {
      const result = {
        'foo': 'fooValue',
        'display_name': 'nameValue'
      }
      wrapper.vm.setResult(result)
      expect(wrapper.vm.$data.value).toBe(result)
      expect(wrapper.vm.displayValue).toBe('nameValue')
      expect(wrapper.vm.isOpen).toBe(false)
    })
  })

  describe('.emptyResults', () => {
    it('returns true when empty object', () => {
      wrapper.vm.$data.results = { foo: [], bar: [] }
      expect(wrapper.vm.emptyResults()).toBe(true)
    })
    it('returns false when non-empty object', () => {
      wrapper.vm.$data.results = { foo: [], bar: [{ name: 'one' }] }
      expect(wrapper.vm.emptyResults()).toBe(false)
    })
    it('returns true when empty array', () => {
      wrapper.vm.$data.results = []
      expect(wrapper.vm.emptyResults()).toBe(true)
    })
    it('returns false when non-empty array', () => {
      wrapper.vm.$data.results = [ { name: 'one' }]
      expect(wrapper.vm.emptyResults()).toBe(false)
    })
    it('returns false when no results', () => {
      wrapper.vm.$data.results = undefined
      expect(wrapper.vm.emptyResults()).toBe(false)
    })
  })

  describe('.openResult', () => {
    it('opens when result has URL', async () => {
      wrapper.vm.openResult({ url: '/test' })
    })
  })

  describe('.renderResult', () => {
    it('renders a string', () => {
      expect(wrapper.vm.renderResult('test')).toBe('test')
    })
    it('renders an object', () => {
      const result = {
        'foo': 'fooValue',
        'display_name': 'nameValue'
      }
      expect(wrapper.vm.renderResult(result)).toBe('nameValue')
    })
  })

  describe('.onArrowDown', () => {
    it('steps up counter', () => {
      wrapper.vm.$data.results = ['one', 'two']
      wrapper.vm.$data.arrowCounter = 0
      wrapper.vm.onArrowDown(null)
      expect(wrapper.vm.$data.arrowCounter).toBe(1)
    })
    it('stops at end', () => {
      wrapper.vm.$data.results = ['one', 'two']
      wrapper.vm.$data.arrowCounter = 1
      wrapper.vm.onArrowDown(null)
      expect(wrapper.vm.$data.arrowCounter).toBe(1)
    })
  })

  describe('.onArrowUp', () => {
    it('steps down counter', () => {
      wrapper.vm.$data.results = ['one', 'two']
      wrapper.vm.$data.arrowCounter = 1
      wrapper.vm.onArrowUp(null)
      expect(wrapper.vm.$data.arrowCounter).toBe(0)
    })
    it('stops at start', () => {
      wrapper.vm.$data.results = ['one', 'two']
      wrapper.vm.$data.arrowCounter = 0
      wrapper.vm.onArrowUp(null)
      expect(wrapper.vm.$data.arrowCounter).toBe(0)
    })
  })

  describe('.onEnter', () => {
    it('sets value from results', () => {
      wrapper.vm.$data.displayValue = 'myDisplayValue'
      wrapper.vm.$data.results = ['myResult']
      wrapper.vm.$data.arrowCounter = 0
      wrapper.vm.$data.isOpen = true
      wrapper.vm.onEnter(null)
      expect(wrapper.vm.$data.value).toBe('myResult')
    })

    it('skips when result is object', () => {
      wrapper.vm.$data.displayValue = 'myDisplayValue'
      wrapper.vm.$data.results = { name: 'myResult' }
      wrapper.vm.$data.arrowCounter = 0
      wrapper.vm.$data.isOpen = true
      wrapper.vm.onEnter(null)
      expect(wrapper.vm.$data.value).toEqual({ name: 'myDisplayValue' })
    })

    it('skips when menu is closed', () => {
      wrapper.vm.$data.displayValue = 'myDisplayValue'
      wrapper.vm.$data.results = ['myResult']
      wrapper.vm.$data.arrowCounter = 0
      wrapper.vm.$data.isOpen = false
      wrapper.vm.onEnter(null)
      expect(wrapper.vm.$data.value).toEqual({ name: 'myDisplayValue' })
    })

    it('skips when arrow counter is out of bounds', () => {
      wrapper.vm.$data.displayValue = 'myDisplayValue'
      wrapper.vm.$data.results = ['myResult']
      wrapper.vm.$data.arrowCounter = -1
      wrapper.vm.$data.isOpen = false
      wrapper.vm.onEnter(null)
      expect(wrapper.vm.$data.value).toEqual({ name: 'myDisplayValue' })
    })
  })

  describe('.handleClickOutside', () => {
    it('sets value from results', () => {
      wrapper.vm.$data.isOpen = true
      wrapper.vm.$data.arrowCounter = 0
      wrapper.vm.$el.contains = jest.fn(() => { return false })
      wrapper.vm.handleClickOutside({ target: 'foobar' })
      expect(wrapper.vm.$data.isOpen).toBe(false)
      expect(wrapper.vm.$data.arrowCounter).toBe(-1)
    })
    it('skips when click is not outside', () => {
      wrapper.vm.$data.isOpen = true
      wrapper.vm.$data.arrowCounter = 1
      wrapper.vm.$el.contains = jest.fn(() => { return true })
      wrapper.vm.handleClickOutside({ target: 'foobar' })
      expect(wrapper.vm.$data.isOpen).toBe(true)
      expect(wrapper.vm.$data.arrowCounter).toBe(1)
    })
  })

  describe('.clear', () => {
    it('sets value to an empty string', () => {
      const autocomplete = getMountedComponent(Autocomplete)
      autocomplete.vm.$data.displayValue = 'test'
      autocomplete.vm.$data.value = { name: 'test' }
      autocomplete.vm.clear()
      expect(autocomplete.vm.$data.displayValue).toBe('')
      expect(autocomplete.vm.$data.value).toEqual({})
    })
  })
})
