# frozen_string_literal: true

require 'test_helper'

class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
  include Warden::Test::Helpers

  Capybara.server = :puma, { Silent: true }

  if ENV['SELENIUM_URL'].present?

    driver_options = {
      desired_capabilities: {
        chromeOptions: {
          args: %w[headless disable-gpu disable-dev-shm-usage] # preserve memory & cpu consumption
        }
      },
      url: ENV['SELENIUM_URL']
    }
    driven_by :selenium, using: :headless_chrome, screen_size: [1400, 1400], options: driver_options

    # Use the IP instead of localhost so Capybara knows where to direct Selenium
    ip = Socket.ip_address_list.detect(&:ipv4_private?).ip_address
    Capybara.server_port = 3000
    Capybara.server_host = '0.0.0.0'
    Capybara.app_host = "http://#{ip}:#{Capybara.server_port}"
  else
    # Otherwise, use the local machine's chromedriver
    driven_by :selenium, using: :headless_chrome, screen_size: [1400, 1400]
  end

  def prepare_options
    driver_options
  end
end
