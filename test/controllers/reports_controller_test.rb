# frozen_string_literal: true

require 'test_helper'

class ReportsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @report = create(:report)
    @user = @report.user
    sign_in @user
  end

  test 'should get index' do
    sign_in create(:root_user)
    get admin_reports_url
    assert_response :success
  end

  test 'should get new' do
    show = create(:show)
    get new_report_url(params: { report: { reportable_id: show, reportable_type: show.class } })
    assert_response :success
  end

  test 'should create report' do
    assert_difference('Report.count') do
      post reports_url,
           params: { report: { category: @report.category, comment: @report.comment, read: @report.read,
                               reportable_id: @report.reportable_id, reportable_type: @report.reportable_type } }
    end

    assert_redirected_to url_for(@report.reportable)
    assert_equal @user, Report.last.user
  end

  test 'should not create invalid report' do
    assert_no_difference('Report.count') do
      post reports_url, params: { report: { reportable_id: 0 } }
    end
  end

  test 'should get edit' do
    sign_in create(:root_user)
    get edit_admin_report_url(@report)
    assert_response :success
  end

  test 'should update report' do
    sign_in create(:root_user)
    patch admin_report_url(@report), params: { report: { read: true } }
    assert_redirected_to admin_reports_url
    assert_equal true, Report.find(@report.id).read
  end

  test 'should not update with invalid name' do
    sign_in create(:root_user)
    patch admin_report_url(@report), params: { report: { reportable_id: 0 } }
    assert_not_equal 0, Report.find(@report.id).reportable_id
  end

  test 'should destroy report' do
    sign_in create(:root_user)
    assert_difference('Report.count', -1) do
      delete admin_report_url(@report)
    end

    assert_redirected_to admin_reports_url
  end
end
