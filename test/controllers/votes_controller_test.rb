# frozen_string_literal: true

require 'test_helper'

class VotesControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  test 'should get votes for show' do
    vote = create(:vote)
    get show_votes_url(vote.show, format: :json)
    assert_response :success
    json = JSON.parse @response.body
    assert_equal vote.person.id, json[0]['id']
    assert_equal vote.person.name, json[0]['name']
  end

  test 'should get votes for person' do
    vote = create(:vote)
    get person_votes_url(vote.person, format: :json)
    assert_response :success
    json = JSON.parse @response.body
    assert_equal vote.show.id, json[0]['id']
    assert_equal vote.show.slug, json[0]['slug']
  end

  test 'should get user votes for show' do
    vote = create(:vote)
    sign_in vote.user
    get my_show_votes_url(vote.show, format: :json)
    assert_response :success
    json = JSON.parse @response.body
    assert_equal vote.person.id, json[0]['id']
    assert_equal vote.person.name, json[0]['name']
  end

  test 'should get user votes for person' do
    vote = create(:vote)
    sign_in vote.user
    get my_person_votes_url(vote.person, format: :json)
    assert_response :success
    json = JSON.parse @response.body
    assert_equal vote.show.id, json[0]['id']
    assert_equal vote.show.slug, json[0]['slug']
  end

  test 'should get ip votes for show' do
    vote = create(:anonymous_vote)
    get my_show_votes_url(vote.show, format: :json)
    assert_response :success
    json = JSON.parse @response.body
    assert_equal vote.person.id, json[0]['id']
    assert_equal vote.person.name, json[0]['name']
  end

  test 'should get ip votes for person' do
    vote = create(:anonymous_vote)
    get my_person_votes_url(vote.person, format: :json)
    assert_response :success
    json = JSON.parse @response.body
    assert_equal vote.show.id, json[0]['id']
    assert_equal vote.show.slug, json[0]['slug']
  end

  test 'should create vote when signed in' do
    show = create(:show)
    person = create(:person)
    sign_in create(:user)
    assert_difference('person.votes.count') do
      post votes_url(format: :json), params: { vote: { value: 1, person_id: person.id, show_id: show.id } }
    end

    assert_response :success
    json = JSON.parse @response.body
    assert_equal Vote.last.id, json['vote']['id']
    assert_equal show.id, json['vote']['show']['id']
    assert_equal person.id, json['vote']['person']['id']
  end

  test 'should create vote when signed out' do
    show = create(:show)
    person = create(:person)
    assert_difference('person.votes.count') do
      post votes_url(format: :json), params: { vote: { value: 1, person_id: person.id, show_id: show.id } }
    end

    assert_response :success
    json = JSON.parse @response.body
    assert_equal Vote.last.id, json['vote']['id']
    assert_equal '127.0.0.1', Vote.last.ip_address
    assert_equal show.id, json['vote']['show']['id']
    assert_equal person.id, json['vote']['person']['id']
  end

  test 'should count down when creating vote' do
    create(:application_settings, max_votes_per_show: 2)
    show = create(:show)
    people = create_list(:person, 2)
    user = create(:user)
    sign_in user

    post votes_url(format: :json), params: { vote: { value: 1, person_id: people[0].id, show_id: show.id } }
    assert_response :success
    json = JSON.parse @response.body
    assert_equal 1, json['limit']['count']
    assert_equal 2, json['limit']['max']

    post votes_url(format: :json), params: { vote: { value: 1, person_id: people[1].id, show_id: show.id } }
    assert_response :success
    json = JSON.parse @response.body
    assert_equal 0, json['limit']['count']
    assert_equal 2, json['limit']['max']
  end

  test 'should remove positive vote when casting negative vote' do
    show = create(:show)
    person = create(:person)
    user = create(:user)
    sign_in user
    create(:vote, person: person, user: user, show: show, value: 1)
    assert_difference('person.votes.count', -1) do
      post votes_url(format: :json), params: { vote: { value: -1, person_id: person.id, show_id: show.id } }
    end
  end

  test 'should remove negative vote when casting positive vote' do
    show = create(:show)
    person = create(:person)
    user = create(:user)
    sign_in user
    create(:vote, person: person, user: user, show: show, value: -1)
    assert_difference('person.votes.count', -1) do
      post votes_url(format: :json), params: { vote: { value: 1, person_id: person.id, show_id: show.id } }
    end
  end

  test 'should not create vote without value' do
    show = create(:show)
    person = create(:person)
    sign_in create(:user)
    assert_no_difference('Vote.count') do
      post votes_url(format: :json), params: { vote: { person_id: person.id, show_id: show.id } }
    end
    json = JSON.parse @response.body
    assert_equal 'Value is not a number', json[0]
  end

  test 'should not create vote without person' do
    show = create(:show)
    sign_in create(:user)
    assert_no_difference('Vote.count') do
      post votes_url(format: :json), params: { vote: { value: 1, show_id: show.id } }
    end
    json = JSON.parse @response.body
    assert_equal 'Person must exist', json[0]
  end

  test 'should not create vote without show' do
    person = create(:person)
    sign_in create(:user)
    assert_no_difference('Vote.count') do
      post votes_url(format: :json), params: { vote: { value: 1, person_id: person.id } }
    end
    json = JSON.parse @response.body
    assert_equal 'Show must exist', json[0]
  end

  test 'should create vote for new person' do
    show = create(:show)
    sign_in create(:user)
    assert_difference('show.votes.count') do
      post votes_url(format: :json), params: { vote: { value: 1, show_id: show.id }, person_name: 'New Person' }
    end
    json = JSON.parse @response.body
    assert_equal Vote.last.id, json['vote']['id']
    assert_equal show.id, json['vote']['show']['id']
    assert_equal Person.last.id, json['vote']['person']['id']
    assert_equal 'New Person', Person.last.name
  end

  test 'should not create vote for new person when limited user' do
    show = create(:show)
    sign_in create(:user, roles: [create(:limited_role)])
    assert_no_difference('Vote.count') do
      post votes_url(format: :json), params: { vote: { value: 1, show_id: show.id }, person_name: 'New Person' }
      assert_response :forbidden
    end
  end

  test 'should create vote for new show' do
    person = create(:person)
    sign_in create(:user)
    assert_difference('person.votes.count') do
      post votes_url(format: :json), params: { vote: { value: 1, person_id: person.id }, show_name: 'New Show' }
    end
    json = JSON.parse @response.body
    assert_equal Vote.last.id, json['vote']['id']
    assert_equal person.id, json['vote']['person']['id']
    assert_equal Show.last.id, json['vote']['show']['id']
    assert_equal 'New Show', Show.last.title
  end

  test 'should not create vote for new show when limited user' do
    person = create(:person)
    sign_in create(:user, roles: [create(:limited_role)])
    assert_no_difference('Vote.count') do
      post votes_url(format: :json), params: { vote: { value: 1, person_id: person.id }, show_name: 'New Show' }
      assert_response :forbidden
    end
  end

  test 'should not create vote when reaching limit' do
    create(:application_settings, max_votes_per_show: 2)
    show = create(:show)
    people = create_list(:person, 3)
    user = create(:user)
    create(:vote, user: user, show: show, value: 1, person: people[0])
    create(:vote, user: user, show: show, value: 1, person: people[1])

    sign_in user
    assert_no_difference('Vote.count') do
      post votes_url(format: :json), params: { vote: { person_id: people[2].id, value: 1, show_id: show.id } }
    end
    json = JSON.parse @response.body
    assert_equal 'limit_reached', json['error']
  end

  test 'should allow opposite vote when reaching limit' do
    create(:application_settings, max_votes_per_show: 2)
    show = create(:show)
    people = create_list(:person, 3)
    user = create(:user)
    create(:vote, user: user, show: show, value: 1, person: people[0])
    create(:vote, user: user, show: show, value: 1, person: people[1])

    sign_in user
    assert_difference('user.votes.count', -1) do
      post votes_url(format: :json), params: { vote: { person_id: people[1].id, value: -1, show_id: show.id } }
    end
  end
end
