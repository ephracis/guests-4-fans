# frozen_string_literal: true

require 'test_helper'

class EpisodesControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  include ConfirmableControllerTests
  include BlockableControllerTests
  include TopicableControllerTests
  include RateableControllerTests
  include EditableControllerTests
  include ImageableControllerTests

  setup do
    @factory_name = :episode
    @editable_params = %i[title description]
    @show = create(:show)
  end

  def episode_url(episode, opts = {})
    show_episode_url(episode.show, episode, opts)
  end

  def edits_episode_url(episode, opts = {})
    edits_show_episode_url(episode.show, episode, opts)
  end

  def rate_episode_url(episode, opts = {})
    rate_show_episode_url(episode.show, episode, opts)
  end

  def rating_episode_url(episode, opts = {})
    rating_show_episode_url(episode.show, episode, opts)
  end

  # This patch is for topicable which expects a RESOURCES_url for creating the resource.
  def episodes_url(opts = {})
    show_episodes_url(@show, opts)
  end

  test 'should get index' do
    get show_episodes_url(@show, format: :json)
    assert_response :success
  end

  test 'should get new' do
    get new_show_episode_url(@show)
    assert_redirected_to new_user_session_url

    sign_in create(:user)
    get new_show_episode_url(@show)
    assert_response :success
  end

  test 'should create episode' do
    params = { episode: { title: 'Test Episode' } }

    post show_episodes_url(@show), params: params
    assert_redirected_to new_user_session_url

    sign_in create(:user)
    assert_difference('@show.episodes.count') do
      post show_episodes_url(@show), params: params
      assert_redirected_to show_episode_url(@show, @show.episodes.last)
      assert_equal 'test-episode', @show.episodes.last.slug
    end
  end

  test 'should not create invalid episode' do
    root_user = create(:user, roles: [create(:root_role)])
    sign_in root_user
    assert_no_difference('@show.episodes.count') do
      post show_episodes_url(@show), params: { episode: { slug: 'wrong slug' } }
    end
  end

  test 'should show episode' do
    episode = create(:episode)
    get show_episode_url(episode.show, episode)
    assert_response :success
  end

  test 'should get edit' do
    episode = create(:episode)
    get edit_show_episode_url(episode.show, episode)
    assert_redirected_to new_user_session_url

    sign_in episode.created_by
    get edit_show_episode_url(episode.show, episode)
    assert_response :success
  end

  test 'should update episode' do
    params = { episode: { slug: 'new_slug' } }
    episode = create(:episode)
    patch show_episode_url(episode.show, episode), params: params
    assert_redirected_to new_user_session_url

    root_user = create(:user, roles: [create(:root_role)])
    sign_in root_user
    patch show_episode_url(episode.show, episode), params: params
    episode = Episode.find(episode.id)
    assert_redirected_to show_episode_url(episode.show, episode)
    assert_equal 'new_slug', episode.slug
  end

  test 'should not update with invalid slug' do
    root_user = create(:user, roles: [create(:root_role)])
    sign_in root_user
    episode = create(:episode)
    patch show_episode_url(episode.show, episode), params: { episode: { slug: 'wrong slug' } }
    episode = Episode.find(episode.id)
    assert_not_equal 'wrong slug', episode.slug
  end

  test 'should set aliases' do
    episode = create(:episode)
    episode.aliases.create(name: 'Alias 1')
    episode.aliases.create(name: 'Alias 2')
    params = { episode: { aliases: ['Alias 2', 'Alias 3', 'Alias 4'] } }
    root_user = create(:user, roles: [create(:root_role)])
    sign_in root_user

    patch show_episode_url(episode.show, episode), params: params
    episode.reload

    assert_redirected_to show_episode_url(episode.show, episode)
    assert_equal 3, episode.aliases.count
    assert_equal 3, Alias.count
    assert_equal 'Alias 2', episode.aliases[0].name
    assert_equal 'Alias 3', episode.aliases[1].name
    assert_equal 'Alias 4', episode.aliases[2].name
  end

  test 'should keep aliases when parameter is missing' do
    episode = create(:episode)
    episode.aliases.create(name: 'Alias 1')
    episode.aliases.create(name: 'Alias 2')
    params = { episode: { description: 'my new description' } }
    root_user = create(:user, roles: [create(:root_role)])
    sign_in root_user

    patch show_episode_url(episode.show, episode), params: params
    episode.reload

    assert_redirected_to show_episode_url(episode.show, episode)
    assert_equal 2, episode.aliases.count
    assert_equal 'Alias 1', episode.aliases[0].name
    assert_equal 'Alias 2', episode.aliases[1].name
  end

  test 'should destroy episode' do
    episode = create(:episode)
    delete show_episode_url(episode.show, episode)
    assert_redirected_to new_user_session_url

    root_user = create(:user, roles: [create(:root_role)])
    sign_in root_user
    assert_difference('Episode.count', -1) do
      delete show_episode_url(episode.show, episode)
      assert_redirected_to show_url(episode.show)
    end
  end

  test 'should add appearance of existing person' do
    episode = create(:episode)
    person = create(:person)
    users = create_list(:user, 2)
    create(:vote, person: person, show: episode.show, user: users[0], fulfilled: true)
    vote = create(:vote, person: person, show: episode.show, user: users[1], fulfilled: false)

    sign_in episode.created_by
    assert_emails 1 do
      assert_difference('episode.appearances.count') do
        assert_difference('Notification.count', 1) do
          post show_episode_appearances_url(episode.show, episode), params: { person: { name: person.name } }
        end
      end
    end

    assert_includes episode.people, person
    assert_includes person.shows, episode.show
    assert_equal "#{person.name} appeared on a show", Notification.last.title
    assert_equal users[1], Notification.last.user
    assert Vote.find(vote.id).fulfilled
  end

  test 'should add appearance of new person' do
    episode = create(:episode)

    sign_in episode.created_by
    assert_difference('episode.appearances.count') do
      assert_difference('Person.count') do
        post show_episode_appearances_url(episode.show, episode), params: { person: { name: 'Ms Test' } }
      end
    end

    person = Person.last

    assert_includes episode.people, person
    assert_includes person.shows, episode.show
    assert_equal 'Ms Test', person.name
  end

  test 'should not add duplicate appearance of person' do
    episode = create(:episode)
    person = create(:person)

    create(:appearance, episode: episode, person: person)

    sign_in episode.created_by
    assert_no_difference('episode.appearances.count') do
      post show_episode_appearances_url(episode.show, episode), params: { person: { name: person.name } }
    end
    assert_equal 1, episode.appearances.count
  end

  test 'should remove appearance of person' do
    root_user = create(:user, roles: [create(:root_role)])
    episode = create(:episode)
    person = create(:person)
    create(:appearance, episode: episode, person: person)

    sign_in root_user
    assert_difference('episode.appearances.count', -1) do
      delete show_episode_appearances_url(episode.show, episode), params: { person: { name: person.name } }
    end
    assert_not_includes episode.people, person
    assert_not_includes person.shows, episode.show
  end

  test 'should not get episode merge form when signed out' do
    episode = create(:episode)
    get merge_show_episode_url(episode.show, episode)
    assert_redirected_to new_user_session_url
  end

  test 'should not get episode merge form when normal user' do
    episode = create(:episode)
    sign_in create(:user)
    get merge_show_episode_url(episode.show, episode)
    assert_redirected_to root_url
  end

  test 'should get episode merge form when admin' do
    episode = create(:episode)
    sign_in create(:admin_user)
    get merge_show_episode_url(episode.show, episode)
    assert_response :success
  end

  test 'should not mark episode as duplicate when signed out' do
    episode1 = create(:episode, title: 'Episode One', description: nil)
    episode2 = create(:episode, title: 'Episode Two', description: 'A nice description.')
    post merge_show_episode_url(episode2.show, episode2, original_id: episode1.id, format: :json)
    assert_response :unauthorized
  end

  test 'should not mark person as duplicate when normal user' do
    episode1 = create(:episode, title: 'Episode One', description: nil)
    episode2 = create(:episode, title: 'Episode Two', description: 'A nice description.')
    sign_in create(:user)
    post merge_show_episode_url(episode2.show, episode2, original_id: episode1.id, format: :json)
    assert_response :forbidden
  end

  test 'should mark person as duplicate' do
    episode1 = create(:episode, show: @show, title: 'Episode One', description: nil)
    episode2 = create(:episode, show: @show, title: 'Episode Two', description: 'A nice description.')

    sign_in create(:admin_user)

    assert_difference 'Episode.count', -1 do
      post merge_show_episode_url(episode2.show, episode2, original_id: episode1.slug)
    end
    assert_redirected_to episode1
    episode1.reload

    assert_equal 'Episode One', episode1.title
    assert_equal 'A nice description.', episode1.description
    assert_equal episode1, Slug.find_by(name: episode2.slug, slugable_type: 'Episode').slugable
    assert_equal episode1, Episode.find_by_alias(episode2.title)

    get show_episode_url(episode2.show, episode2)
    assert_redirected_to episode1
  end
end
