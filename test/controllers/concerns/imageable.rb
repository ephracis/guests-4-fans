# frozen_string_literal: true

module ImageableControllerTests
  extend ActiveSupport::Testing::Declarative

  test 'can upload image url' do
    resource = create(@factory_name)
    user = create(:admin_user)

    params = {}
    params[resource.class.to_s.underscore] = { image: 'http://example.com/image.png' }

    test_image = File.open('test/fixtures/files/images/timcast.png')
    URI::HTTP.any_instance.expects(:open).returns(test_image)
    sign_in user
    patch url_for(resource), params: params
    resource.reload
    assert_response :redirect
    assert resource.image.attached?
  end

  test 'can upload image data' do
    resource = create(@factory_name)
    user = create(:admin_user)

    test_image = File.read('test/fixtures/files/images/timcast.png')
    image_data = "data:image/png;base64,#{Base64.encode64(test_image)}"
    params = {}
    params[resource.class.to_s.underscore] = { image: image_data }

    sign_in user
    patch url_for(resource), params: params
    resource.reload
    assert_response :redirect
    assert resource.image.attached?
  end

  test 'can keep image' do
    resource = create(@factory_name)
    user = create(:admin_user)

    params = {}
    params[resource.class.to_s.underscore] = { image: '/rails/active_storage/blobs/redirect/xxx--xxx/image.jpg' }

    sign_in user
    patch url_for(resource), params: params
    assert_response :redirect
  end

  test 'should purge image' do
    resource = create(@factory_name)
    user = create(:admin_user)

    params = {}
    params[resource.class.to_s.underscore] = { image: '' }

    sign_in user
    patch url_for(resource), params: params
    resource.reload
    assert_response :redirect
    assert_not resource.image.attached?
  end
end
