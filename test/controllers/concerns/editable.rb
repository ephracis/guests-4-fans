# frozen_string_literal: true

module EditableControllerTests
  extend ActiveSupport::Testing::Declarative

  test 'should create edit when normal user' do
    resource = create(@factory_name)
    user = create(:user)

    resource.write_attribute(@editable_params[0], 'oldvalue')
    resource.write_attribute(@editable_params[1], 'oldvalue')
    resource.save!

    resource_param = resource.class.to_s.underscore
    params = {}
    params[resource_param] = {}
    # keep one value, no edit should be created for this
    params[resource_param][@editable_params[0]] = 'oldvalue'
    # change this value
    params[resource_param][@editable_params[1]] = 'newvalue'

    sign_in user
    assert_difference 'Edit.count' do
      patch url_for(resource), params: params
      resource.reload
    end

    # assert no actual changes
    assert_equal 'oldvalue', resource.read_attribute(@editable_params[0])
    assert_equal 'oldvalue', resource.read_attribute(@editable_params[1])

    # assert new pending edit
    assert_equal 1, resource.pending_edits.count
    edit = resource.pending_edits.first
    assert_equal 'newvalue', edit.value
    assert_equal @editable_params[1].to_s, edit.name.to_s
    assert_equal user, edit.created_by
  end

  test 'should not get list of edits signed out' do
    resource = create(@factory_name)
    create(:edit, editable: resource, name: @editable_params[0])
    sign_in create(:user)
    get send("edits_#{@factory_name}_url", resource)
    assert_response :success
  end

  test 'should not get list of edits as normal user' do
    resource = create(@factory_name)
    create(:edit, editable: resource, name: @editable_params[0])
    sign_in create(:user)
    get send("edits_#{@factory_name}_url", resource)
    assert_response :success
  end

  test 'should get list of edits as admin user' do
    resource = create(@factory_name)
    create(:edit, editable: resource, name: @editable_params[0])
    user = create(:admin_user)

    sign_in user
    get send("edits_#{@factory_name}_url", resource)
    assert_response :success
  end
end
