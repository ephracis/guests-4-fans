# frozen_string_literal: true

module RateableControllerTests
  extend ActiveSupport::Testing::Declarative

  test 'should create rating' do
    resource = create(@factory_name)
    user = create(:user)

    params = { rating: { value: 2 } }
    sign_in user

    assert_difference 'Rating.count' do
      post send("rate_#{@factory_name}_url", resource, format: :json), params: params
    end

    assert_response :success
    assert_equal 1, resource.ratings.count

    rating = resource.ratings.first
    assert_equal 2, rating.value
    assert_equal user, rating.user
  end

  test 'should update rating' do
    resource = create(@factory_name)
    user = create(:user)
    rating = create(:rating, user: user, rateable: resource, value: 2)

    params = { rating: { value: 5 } }
    sign_in user

    assert_no_difference 'Rating.count' do
      post send("rate_#{@factory_name}_url", resource, format: :json), params: params
    end

    assert_response :success
    rating.reload
    assert_equal 1, resource.ratings.count
    assert_equal 5, rating.value
    assert_equal user, rating.user
  end

  test 'should get rating signed out' do
    resource = create(@factory_name)
    create(:rating, rateable: resource, value: 1)
    create(:rating, rateable: resource, value: 1)
    create(:rating, rateable: resource, value: 4)

    get send("rating_#{@factory_name}_url", resource, format: :json)
    assert_response :success

    json = JSON.parse @response.body
    assert_equal 2.0, json['rating']
    assert_equal(-1, json['my_rating'])
  end

  test 'should get rating signed in without rating' do
    resource = create(@factory_name)
    user = create(:user)
    create(:rating, rateable: resource, value: 1)
    create(:rating, rateable: resource, value: 1)
    create(:rating, rateable: resource, value: 4)

    sign_in user
    get send("rating_#{@factory_name}_url", resource, format: :json)
    assert_response :success

    json = JSON.parse @response.body
    assert_equal 2.0, json['rating']
    assert_equal(-1, json['my_rating'])
  end

  test 'should get rating signed in with rating' do
    resource = create(@factory_name)
    user = create(:user)
    create(:rating, rateable: resource, value: 1)
    create(:rating, rateable: resource, value: 1)
    create(:rating, rateable: resource, value: 4, user: user)

    sign_in user
    get send("rating_#{@factory_name}_url", resource, format: :json)
    assert_response :success

    json = JSON.parse @response.body
    assert_equal 2.0, json['rating']
    assert_equal 4, json['my_rating']
  end
end
