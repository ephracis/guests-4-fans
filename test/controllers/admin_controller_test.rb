# frozen_string_literal: true

require 'test_helper'

class AdminControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @user = create(:user)
    sign_in @user
  end

  test 'should not get dashboard when not root' do
    create(:root_user)
    get admin_dashboard_url
    assert_redirected_to root_url
    get admin_dashboard_url(format: :json)
    json = JSON.parse @response.body
    assert_equal 'Insufficient privileges', json['error']
  end

  test 'should get dashboard when root' do
    create(:user)
    @user.roles << create(:root_role)
    get admin_dashboard_url
    assert_response :success
  end

  test 'should not get unconfirmed when not root' do
    create(:root_user)
    get admin_unconfirmed_url
    assert_redirected_to root_url
    get admin_unconfirmed_url(format: :json)
    json = JSON.parse @response.body
    assert_equal 'Insufficient privileges', json['error']
  end

  test 'should get unconfirmed when root' do
    create(:user)
    @user.roles << create(:root_role)
    get admin_unconfirmed_url
    assert_response :success
  end

  test 'should not get imageless when not root' do
    create(:root_user)
    get admin_imageless_url
    assert_redirected_to root_url
    get admin_imageless_url(format: :json)
    json = JSON.parse @response.body
    assert_equal 'Insufficient privileges', json['error']
  end

  test 'should get imageless when root' do
    create(:user)
    @user.roles << create(:root_role)
    get admin_imageless_url
    assert_response :success
  end
end
