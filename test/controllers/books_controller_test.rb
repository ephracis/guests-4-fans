# frozen_string_literal: true

require 'test_helper'

class BooksControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  include ConfirmableControllerTests
  include BlockableControllerTests
  include TopicableControllerTests
  include RateableControllerTests
  include ImageableControllerTests

  setup do
    @factory_name = :book
    @editable_params = %i[name description]
    @person = create(:person)
  end

  def book_url(book, opts = {})
    person_book_url(book.authors.first, book, opts)
  end

  def edits_book_url(book, opts = {})
    edits_person_episode_url(book.authors.first, book, opts)
  end

  def rate_book_url(book, opts = {})
    rate_person_book_url(book.authors.first, book, opts)
  end

  def rating_book_url(book, opts = {})
    rating_person_book_url(book.authors.first, book, opts)
  end

  # This patch is for topicable which expects a RESOURCES_url for creating the resource.
  def books_url(opts = {})
    person_books_url(@person, opts)
  end

  test 'should get index' do
    get person_books_url(@person)
    assert_response :success
  end

  test 'should get new' do
    get new_person_book_url(@person)
    assert_redirected_to new_user_session_url

    sign_in create(:user)
    get new_person_book_url(@person)
    assert_response :success
  end

  test 'should create book' do
    params = { book: { name: 'Test Book' } }

    post person_books_url(@person), params: params
    assert_redirected_to new_user_session_url

    sign_in create(:user)
    assert_difference('@person.books.count') do
      post person_books_url(@person), params: params
      assert_redirected_to edit_person_book_url(@person, @person.books.last)
      assert_equal 'test-book', @person.books.last.slug
    end
  end

  test 'should not create duplicate book' do
    book = create(:book, name: "You're CORRECT")
    params = { book: { name: 'you’re   correct ' } }

    post person_books_url(@person), params: params
    assert_redirected_to new_user_session_url

    sign_in create(:user)
    assert_no_difference('Book.count') do
      assert_difference('@person.books.count') do
        post person_books_url(@person), params: params
        assert_redirected_to edit_person_book_url(@person, @person.books.last)
        assert_equal book, @person.books.last
      end
    end
  end

  test 'should not create invalid episode' do
    root_user = create(:user, roles: [create(:root_role)])
    sign_in root_user
    assert_no_difference('@person.books.count') do
      post person_books_url(@person), params: { book: { slug: 'wrong name' } }
    end
  end

  test 'should show book' do
    book = create(:book, authors: [@person], amazon: nil)
    get person_book_url(@person, book)
    assert_redirected_to person_books_url(@person)

    book.update(amazon: 'http://example.com/book1')
    get person_book_url(@person, book)
    assert_redirected_to 'http://example.com/book1'
  end

  test 'should get edit' do
    book = create(:book, authors: [@person])
    get edit_person_book_url(@person, book)
    assert_redirected_to new_user_session_url

    sign_in book.created_by
    get edit_person_book_url(@person, book)
    assert_response :success
  end

  test 'should update book' do
    params = { book: { name: 'New Name' } }
    book = create(:book, authors: [@person])
    patch person_book_url(@person, book), params: params
    assert_redirected_to new_user_session_url

    root_user = create(:user, roles: [create(:root_role)])
    sign_in root_user
    patch person_book_url(@person, book), params: params
    book.reload
    assert_redirected_to person_books_url(@person)
    assert_equal 'New Name', book.name
  end

  test 'should not update with invalid slug' do
    root_user = create(:user, roles: [create(:root_role)])
    sign_in root_user
    book = create(:book, authors: [@person])
    patch person_book_url(@person, book), params: { book: { slug: 'wrong name' } }
    book.reload
    assert_not_equal 'wrong name', book.slug
  end

  test 'should destroy book' do
    book = create(:book, authors: [@person])
    delete person_book_url(@person, book)
    assert_redirected_to new_user_session_url

    root_user = create(:user, roles: [create(:root_role)])
    sign_in root_user
    assert_difference('Book.count', -1) do
      delete person_book_url(@person, book)
      assert_redirected_to person_books_url(@person)
    end
  end
end
