# frozen_string_literal: true

require 'test_helper'

class PeopleControllerIndexTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  test 'should get index' do
    get people_url
    assert_response :success
  end

  test 'should get index with query' do
    get people_url, params: { q: 'test' }
    assert_response :success
  end

  test 'should get people without appearance on show' do
    show = create(:show)
    episode = create(:episode, show: show)

    # create 4 people
    people = create_list(:confirmed_person, 4)

    # make 2 people appear on show, 2 without appearance
    create(:appearance, episode: episode, person: people[0])
    create(:appearance, episode: episode, person: people[1])

    # create votes for one person with appearance, and one without
    create(:vote, person: people[1], show: show)
    create(:vote, person: people[2], show: show)

    get people_url(format: :json), params: { show_id: show.slug, mode: :new }
    assert_response :success
    json = JSON.parse(@response.body)
    assert_equal 2, json.length
    assert_equal people[2].id, json[0]['id']
    assert_equal people[3].id, json[1]['id']
  end

  test 'should get people with appearance on show' do
    show = create(:show)
    episode = create(:episode, show: show)

    # create 4 people
    people = create_list(:confirmed_person, 4)

    # make 2 people appear on show, 2 without appearance
    create(:appearance, episode: episode, person: people[0])
    create(:appearance, episode: episode, person: people[1])

    # create votes for one person with appearance, and one without
    create(:vote, person: people[1], show: show)
    create(:vote, person: people[2], show: show)

    get people_url(format: :json), params: { show_id: show.slug, mode: :old }
    assert_response :success
    json = JSON.parse(@response.body)
    assert_equal 2, json.length
    assert_equal people[1].id, json[0]['id']
    assert_equal people[0].id, json[1]['id']
  end

  test 'should fill votes' do
    user = create(:user)
    show = create(:show)
    person1 = create(:person)
    person2 = create(:person)

    sign_in user
    create(:vote, show: show, person: person1, user: user, value: 1)

    get people_url(show_id: show.slug, format: :json)
    assert_response :success
    json = JSON.parse @response.body

    assert_equal 2, json.length
    assert_equal person1.name, json[0]['name']
    assert json[0]['voted']
    assert_equal person2.name, json[1]['name']
    assert_not json[1]['voted']
  end

  test 'should get previously on show' do
    show = create(:show)
    episode1 = create(:episode, show: show, aired_at: 5.days.ago)
    episode2 = create(:episode, show: show, aired_at: 4.days.ago)
    episode3 = create(:episode, show: show, aired_at: 3.days.ago)
    episode4 = create(:episode, show: show, aired_at: 2.days.ago)

    # create people
    people = create_list(:confirmed_person, 4)

    # make some people appear on show
    create(:appearance, episode: episode1, person: people[1])
    create(:appearance, episode: episode2, person: people[0])
    create(:appearance, episode: episode3, person: people[2])
    create(:appearance, episode: episode4, person: people[2])

    get previous_show_people_url(show, format: :json)
    assert_response :success
    json = JSON.parse(@response.body)
    expected_order = [people[2].id, people[0].id, people[1].id]
    actual_order = json['data'].map { |p| p['id'] }
    assert_equal 3, json['data'].length
    assert_equal expected_order, actual_order
  end

  test 'should get not yet on show' do
    show = create(:show)
    episode = create(:episode, show: show)

    # create 4 people
    people = create_list(:confirmed_person, 4)

    # make 2 people appear on show, 2 without appearance
    create(:appearance, episode: episode, person: people[0])
    create(:appearance, episode: episode, person: people[1])

    # create votes for one person with appearance, and one without
    create(:vote, person: people[1], show: show)
    create(:vote, person: people[2], show: show)

    get other_show_people_url(show, format: :json)
    assert_response :success
    json = JSON.parse(@response.body)
    assert_equal 2, json['data'].length
    assert_equal people[2].id, json['data'][0]['id']
    assert_equal people[3].id, json['data'][1]['id']
  end
end
