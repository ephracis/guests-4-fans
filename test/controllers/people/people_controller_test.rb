# frozen_string_literal: true

require 'test_helper'

class PeopleControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  include ConfirmableControllerTests
  include BlockableControllerTests
  include TopicableControllerTests
  include RateableControllerTests
  include EditableControllerTests
  include ImageableControllerTests

  setup do
    @factory_name = :person
    @editable_params = %i[name description]
  end

  test 'should show person' do
    person = create(:person)
    get person_url(person)
    assert_response :success
  end

  test 'should show new person for show' do
    person = create(:person)
    show = create(:show)
    get show_person_url(show, person)
    assert_response :success
  end

  test 'should show previous person for show' do
    person = create(:person)
    show = create(:show)
    episode = create(:episode, show: show)
    create(:appearance, person: person, episode: episode)
    get show_person_url(show, person)
    assert_response :success
  end

  test 'should destroy person' do
    person = create(:person)
    delete show_url(person)
    assert_redirected_to new_user_session_url

    root_user = create(:user, roles: [create(:root_role)])
    sign_in root_user
    assert_difference('Person.count', -1) do
      delete person_url(person)
      assert_redirected_to people_url
    end
  end

  test 'should not destroy person when normal user' do
    person = create(:person)
    sign_in create(:user)
    assert_no_difference('Person.count') do
      delete person_url(person)
      assert_redirected_to root_url
    end
  end

  test 'should mark person as favorite' do
    user = create(:user)
    person = create(:confirmed_person)
    sign_in user
    assert_difference('Favorite.count') do
      post favorite_person_url(person)
    end

    assert_equal user, Favorite.last.user
    assert_equal person, Favorite.last.favoritable
  end

  test 'should unmark person as favorite' do
    person = create(:confirmed_person)
    user = create(:user)
    create(:favorite, user: user, favoritable: person)
    sign_in user
    assert_difference('Favorite.count', -1) do
      post favorite_person_url(person)
    end

    assert_equal 0, person.favorites.where(user: user).count
  end

  test 'should list shows for person without appearances' do
    person = create(:person)
    get shows_person_url(person, format: :json)
    assert_response :success
  end

  test 'should list shows for person with appearances' do
    person = create(:person)
    shows = create_list(:show, 3)
    create(:appearance, person: person, episode: create(:episode, show: shows[0], aired_at: 8.days.ago))
    create(:appearance, person: person, episode: create(:episode, show: shows[1], aired_at: 5.days.ago))
    create(:appearance, person: person, episode: create(:episode, show: shows[2], aired_at: 4.days.ago))
    create(:appearance, person: person, episode: create(:episode, show: shows[0], aired_at: 3.days.ago))
    get shows_person_url(person, format: :json)
    assert_response :success
    json = JSON.parse(@response.body)
    expected_order = [shows[0].id, shows[2].id, shows[1].id]
    actual_order = json['data'].map { |p| p['id'] }
    assert_equal 3, json['data'].length
    assert_equal expected_order, actual_order
  end

  test 'should not get person merge form when signed out' do
    person = create(:person)
    get merge_person_url(person)
    assert_redirected_to new_user_session_url
  end

  test 'should not get person merge form when normal user' do
    person = create(:person)
    sign_in create(:user)
    get merge_person_url(person)
    assert_redirected_to root_url
  end

  test 'should get person merge form when admin' do
    person = create(:person)
    sign_in create(:admin_user)
    get merge_person_url(person)
    assert_response :success
  end

  test 'should not mark person as duplicate when signed out' do
    person1 = create(:person, name: 'Person Testson', description: nil)
    person2 = create(:person, name: 'P. Testson', description: 'A nice description.')
    post merge_person_url(person2, original_id: person1.id, format: :json)
    assert_response :unauthorized
  end

  test 'should not mark person as duplicate when normal user' do
    person1 = create(:person, name: 'Person Testson', description: nil)
    person2 = create(:person, name: 'P. Testson', description: 'A nice description.')
    sign_in create(:user)
    post merge_person_url(person2, original_id: person1.id, format: :json)
    assert_response :forbidden
  end

  test 'should mark person as duplicate' do
    person1 = create(:person, name: 'Person Testson', description: nil)
    person2 = create(:person, name: 'P. Testson', description: 'A nice description.')

    create(:vote, person: person1)
    create(:vote, person: person2)
    create(:vote, person: person2)

    sign_in create(:admin_user)

    assert_difference 'Person.count', -1 do
      assert_difference 'person1.votes.count', 2 do
        post merge_person_url(person2, original_id: person1.slug)
      end
    end
    assert_redirected_to person1
    person1.reload

    assert_equal 'Person Testson', person1.name
    assert_equal 'A nice description.', person1.description
    assert_equal person1, Slug.find_by(name: person2.slug, slugable_type: 'Person').slugable
    assert_equal person1, Person.find_by_alias(person2.name)

    get person_url(person2)
    assert_redirected_to person1
  end
end
