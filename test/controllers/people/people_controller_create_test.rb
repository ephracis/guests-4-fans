# frozen_string_literal: true

require 'test_helper'

class PeopleControllerCreateTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  test 'should get new' do
    get new_person_url
    assert_redirected_to new_user_session_url

    sign_in create(:user)
    get new_person_url
    assert_response :success
  end

  test 'should create person' do
    params = { person: { name: 'New Person' } }

    post people_url, params: params
    assert_redirected_to new_user_session_url

    user = create(:user)
    sign_in user
    assert_difference('Person.count') do
      post people_url, params: params
      assert_redirected_to person_url(Person.last)
    end
    assert_equal user, Person.last.created_by
    assert_equal 'new-person', Person.last.slug
  end

  test 'should not create person with invalid name' do
    root_user = create(:user, roles: [create(:root_role)])
    sign_in root_user
    assert_no_difference('Person.count') do
      post people_url, params: { person: { name: 'x' } }
    end
  end

  test 'should create person as root' do
    root_user = create(:user, roles: [create(:root_role)])
    sign_in root_user

    assert_difference('Person.count') do
      post people_url, params: { person: { name: 'New Person', slug: 'new-person' } }
      assert_redirected_to person_url(Person.last)
    end
  end

  test 'should not create person when limited user' do
    limited_user = create(:user, roles: [create(:limited_role)])
    sign_in limited_user
    assert_no_difference('Person.count') do
      post people_url, params: { person: { name: 'New Person', slug: 'new-person' } }
      assert_redirected_to root_url
    end
  end

  test 'should create person with aliases' do
    params = { person: { name: 'New Person', aliases: ['Alias'] } }

    post people_url, params: params
    assert_redirected_to new_user_session_url

    user = create(:user)
    sign_in user
    assert_difference('Person.count') do
      assert_difference('Alias.count') do
        post people_url, params: params
      end
    end
    person = Person.last
    assert_redirected_to person_url(person)
    assert_equal user, person.created_by
    assert_equal 'new-person', person.slug
    assert_equal 1, person.aliases.count
    assert_equal person, Person.find_by_alias('Alias')
  end
end
