# frozen_string_literal: true

require 'test_helper'

class SourcesControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @user = create(:user, roles: [create(:root_role)])
    sign_in @user
    @show = create(:show)
    URI::HTTP.any_instance.stubs(:open).returns(File.open('test/fixtures/files/images/timcast.png'))
  end

  test 'should get index' do
    get show_sources_url(@show, format: :json)
    assert_response :success
  end

  test 'should get new' do
    get new_show_source_url(@show)
    assert_response :success
  end

  test 'should create source' do
    assert_difference('@show.sources.count') do
      post show_sources_url(@show), params: { source: { kind: 'spotify', foreign_id: 'my-show-id' } }
    end

    assert_redirected_to edit_show_source_url(@show, Source.last)
  end

  test 'should not create invalid source' do
    assert_no_difference('Source.count') do
      post show_sources_url(@show), params: { source: { kind: 'invalid' } }
    end
  end

  test 'should get edit' do
    @source = create(:source, show: @show)
    get edit_show_source_url(@show, @source)
    assert_response :success
  end

  test 'should update source' do
    @source = create(:source, show: @show)
    patch show_source_url(@show, @source), params: { source: { foreign_id: 'new-id' } }
    @source = Source.find(@source.id)
    assert_redirected_to edit_show_source_url(@show, @source)
    assert_equal 'new-id', @source.foreign_id
  end

  test 'should not update with invalid kind' do
    @source = create(:source, show: @show)
    patch show_source_url(@show, @source), params: { source: { kind: 'invalid' } }
    assert_not_equal 'invalid', Source.find(@source.id).kind
  end

  test 'should destroy source' do
    @source = create(:source, show: @show)
    assert_difference('@show.sources.count', -1) do
      delete show_source_url(@show, @source)
    end

    assert_redirected_to edit_show_url(@show)
  end

  test 'should fetch episodes from source' do
    create(:episode, title: 'Australia Cyber Spying Bill Allows Device Takeover w/Sydney Watson And Elijah Schaffer')
    @source = create(:source, title_pattern: 'Timcast IRL #\\d+ - (?<title>.*)')

    episode_json = File.read('test/fixtures/files/sources/spotify.json')
    Source.expects(:spotify_access_token).returns('my-token')
    Source.expects(:http_get).returns(JSON.parse(episode_json))

    get fetch_show_source_url(@source.show, @source, format: :json)

    json = JSON.parse @response.body
    assert_equal 'spotify', json['source']['kind']
    assert_equal 20, json['episodes'].length
    assert_equal '2YJTzYmsFigu9JRCfhIX3f', json['episodes'][0]['source_id']
    assert_match(/^Australia Cyber Spying/, json['episodes'][0]['title'])
    assert_equal 2, json['episodes'][0]['guests'].length
    assert json['episodes'][0]['exists']
    assert_not json['episodes'][1]['exists']
  end

  test 'should create episodes from source' do
    create(:episode, show: @show, title: 'Episode 1')
    @source = create(:source, show: @show)

    episodes = {
      'source' => { 'kind' => 'spotify' },
      'episodes' => [
        {
          'description' => 'some description',
          'source_id' => 'spotify-id-1',
          'image' => 'http://example.com/spotify-image.jpg',
          'title' => 'Episode 1',
          'aired_at' => '2021-09-01'
        },
        {
          'description' => 'some description',
          'source_id' => 'spotify-id-2',
          'image' => 'http://example.com/spotify-image.jpg',
          'title' => 'Episode 2',
          'aired_at' => '2021-09-02'
        }
      ]
    }

    assert_difference '@show.episodes.count', 1 do
      post generate_show_source_url(@source.show, @source), params: episodes
    end

    assert_equal 'Episode 2', @show.episodes.last.title
  end

  test 'should fail when trying to generate from invalid json' do
    @source = create(:source, show: @show)
    assert_no_difference 'Episode.count' do
      post generate_show_source_url(@source.show, @source), params: { episodes: 'invalid' }
    end
  end
end
