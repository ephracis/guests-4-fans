# frozen_string_literal: true

require 'test_helper'

class ApplicationControllerTest < ActionDispatch::IntegrationTest
  test 'should get search' do
    person = create(:person, name: 'Query Person')
    show = create(:show, title: 'Query Show')
    episode = create(:episode, title: 'Query Episode')

    get search_url(q: 'Query', format: :json)

    assert_response :success
    json = JSON.parse @response.body
    assert_equal 1, json['results']['people'].length
    assert_equal 1, json['results']['shows'].length
    assert_equal 1, json['results']['episodes'].length
    assert_equal person.id, json['results']['people'][0]['id']
    assert_equal show.id, json['results']['shows'][0]['id']
    assert_equal episode.id, json['results']['episodes'][0]['id']
  end

  # test 'should get episode url' do
  #   episode = create(:episode, title: 'my-episode', show: create(:show, name: 'my-show'))
  #   assert_equal 'foo', ApplicationController.episode_url(episode)
  # end

  # test 'should get episode path' do
  #   episode = create(:episode, title: 'my-episode', show: create(:show, name: 'my-show'))
  #   assert_equal 'foo', ApplicationController.episode_path(episode)
  # end
end
