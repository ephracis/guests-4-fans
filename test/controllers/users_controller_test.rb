# frozen_string_literal: true

require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  test 'should show user profile' do
    user = create(:user)
    sign_in user
    get user_url(user)
    assert_response :success
  end

  test 'should show password change' do
    user = create(:user)
    sign_in user
    get edit_password_users_url
    assert_response :success
  end

  test 'should update fullname' do
    user = create(:user, no_password: true)
    sign_in user
    patch user_registration_url, params: { user: { fullname: 'New Name' } }
    assert_redirected_to edit_user_registration_url
    assert_equal 'New Name', User.find(user.id).fullname
  end

  test 'should show update password without current' do
    user = create(:user, no_password: true)
    sign_in user
    patch update_password_users_url, params: { user: { password: 'changeme', password_confirmation: 'changeme' } }
    assert_redirected_to root_url
    assert_not User.find(user.id).no_password
  end

  test 'should show update password with current' do
    user = create(:user, no_password: false, password: 's3cr3t')
    current_encrypted = user.encrypted_password.dup
    sign_in user
    patch update_password_users_url, params: { user: {
      current_password: 's3cr3t', password: 'changeme', password_confirmation: 'changeme'
    } }
    assert_redirected_to root_url
    assert_not_equal current_encrypted, User.find(user.id).encrypted_password
  end

  test 'should show fail update password with incorrect confirmation' do
    user = create(:user, no_password: true)
    sign_in user
    patch update_password_users_url, params: { user: { password: 'changeme', password_confirmation: 'changeme1' } }
    assert_response :success
    assert_select '.alert', /Password confirmation doesn't match Password/
  end
end
