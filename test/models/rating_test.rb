# frozen_string_literal: true

require 'test_helper'

class RatingTest < ActiveSupport::TestCase
  test 'should save rating' do
    user = create(:user)
    show = create(:show)
    rating = build(:rating, user: user, rateable: show, value: 3)
    assert rating.valid?
    assert rating.save

    person = create(:person)
    rating = build(:rating, user: user, rateable: person, value: 3)
    assert rating.valid?
    assert rating.save
  end

  test 'should not create two ratings for same resource' do
    user = create(:user)
    show = create(:show)
    create(:rating, user: user, rateable: show, value: 3)
    rating = build(:rating, user: user, rateable: show, value: 3)
    assert_not rating.valid?
    assert_not rating.save
  end
end
