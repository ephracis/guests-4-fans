# frozen_string_literal: true

require 'test_helper'

class FavoriteTest < ActiveSupport::TestCase
  test 'should favorite guest' do
    user = create(:user)
    guest = create(:person)
    favorite = build(:favorite, user: user, favoritable: guest)

    assert favorite.valid?, 'Favorite relation is not valid'
    assert favorite.save, 'Could not save favorite relation'
  end

  test 'should not create duplicate favorite' do
    user = create(:user)
    guest = create(:person)
    create(:favorite, user: user, favoritable: guest)
    favorite = build(:favorite, user: user, favoritable: guest)

    assert_not favorite.valid?, 'Duplicate favorite relation is valid'
    assert_not favorite.save, 'Saved duplicate favorite relation'
  end
end
