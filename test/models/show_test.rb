# frozen_string_literal: true

require 'test_helper'

class ShowTest < ActiveSupport::TestCase
  include ConfirmableTests
  include BlockableTests
  include TopicableTests
  include ImageableTests
  include RateableTests

  setup do
    @factory_name = :show
  end

  test 'should save show' do
    show = build(:show)
    assert show.valid?
    assert show.save
  end

  test 'should cast to string' do
    show = create(:show, title: 'My Show')
    assert_equal 'My Show', show.to_s
  end

  test 'should get new guests' do
    shows = create_list(:show, 2)
    people = create_list(:person, 4)

    # people[0]: no apperances

    # people[1]: appeared only on show[0]
    create(:appearance, person: people[1], episode: create(:episode, show: shows[0]))

    # people[2]: appeared only on show[1]
    create(:appearance, person: people[2], episode: create(:episode, show: shows[1]))

    # people[3]: appeared on both shows
    create(:appearance, person: people[3], episode: create(:episode, show: shows[0]))
    create(:appearance, person: people[3], episode: create(:episode, show: shows[1]))

    assert_equal 2, shows[0].new_people.load.size, 'Only two people have not been on show'
    assert_includes shows[0].new_people, people[0], 'Person 0 should be new guest'
    assert_includes shows[0].new_people, people[2], 'Person 2 should be new guest'
  end

  test 'should get shows ordered by votes' do
    shows = create_list(:show, 3)
    create(:vote, show: shows[2])
    create(:vote, show: shows[1])
    create(:vote, show: shows[1])

    assert_equal shows[1], Show.top[0]
    assert_equal shows[2], Show.top[1]
    assert_equal shows[0], Show.top[2]
  end

  test 'should generate slug' do
    create(:show, title: 'Test Show', slug: 'test-show')
    create(:show, title: 'Test Show 1', slug: 'test-show1')

    show = build(:show, title: 'Test-Show', slug: nil)
    assert_not show.valid?
    show.generate_slug
    assert show.valid?
    assert_equal 'test-show2', show.slug
  end

  test 'should get any votes' do
    show = create(:show)
    create_list(:vote, 3)

    assert_not show.any_votes_for?
    create(:vote, show: show)
    show.reload
    assert show.any_votes_for?
  end

  test 'should get votes for person' do
    show = create(:show)
    person = create(:person)
    create_list(:vote, 3)

    assert_not show.any_votes_for?(person: person)
    create(:vote, person: person, show: show)
    show.reload
    assert show.any_votes_for?(person: person)
  end

  test 'should get votes for ip address' do
    show = create(:show)
    create_list(:vote, 3, ip_address: '1.2.3.4')

    assert_not show.any_votes_for?(user: '1.1.1.1')
    create(:vote, show: show, ip_address: '1.1.1.1')
    show.reload
    assert show.any_votes_for?(user: '1.1.1.1')
  end

  test 'should get votes for user' do
    user = create(:user)
    show = create(:show)
    create_list(:vote, 3)

    assert_not show.any_votes_for?(user: user)
    create(:vote, show: show, user: user)
    show.reload
    assert show.any_votes_for?(user: user)
  end
end
