# frozen_string_literal: true

require 'test_helper'

class ApplicationSettingsTest < ActiveSupport::TestCase
  test 'should create setting' do
    settings = build(:application_settings)
    assert settings.valid?
    assert settings.save
  end
end
