# frozen_string_literal: true

module ImageableTests
  extend ActiveSupport::Testing::Declarative

  test 'should get image type' do
    resource = create(@factory_name)

    assert_equal 'image.jpg', resource.image_type_to_filename('image/jpeg')
    assert_equal 'image.png', resource.image_type_to_filename('image/png')
    assert_equal 'image.webp', resource.image_type_to_filename('image/webp')
    assert_equal 'image', resource.image_type_to_filename('other')
  end
end
