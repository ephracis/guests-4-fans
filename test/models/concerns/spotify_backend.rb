# frozen_string_literal: true

module SpotifyBackendTests
  extend ActiveSupport::Testing::Declarative

  test 'should fetch episodes from spotify' do
    # mock call for getting access token
    mock_post_json = '{"access_token": "my-access-token"}'
    mock_post_http = mock
    mock_post_http.expects('use_ssl=').with(true)
    mock_post_http.expects(:request).returns mock(body: mock_post_json)
    Net::HTTP.expects(:new).with('accounts.spotify.com', 443).returns(mock_post_http)

    # mock call for getting show episodes
    mock_get_json = File.read('test/fixtures/files/sources/spotify.json')
    mock_get_http = mock
    mock_get_http.expects('use_ssl=').with(true)
    mock_get_http.expects(:request).returns mock(body: mock_get_json)
    Net::HTTP.expects(:new).with('api.spotify.com', 443).returns(mock_get_http)

    # call fetch
    source = create(
      :spotify_source,
      filter_pattern: '^Timcast IRL #\d+ - ',
      title_pattern: 'Timcast IRL #\d+ - (?<title>.*)',
      guests_pattern: '(w|ft)/(?<names>.*)'
    )
    response = source.fetch

    # assert response
    assert_equal 'spotify', response['source']['kind']
    assert_equal 20, response['episodes'].length
    assert_equal '2YJTzYmsFigu9JRCfhIX3f', response['episodes'][0]['source_id']

    assert_match(/^Australia/, response['episodes'][0]['title'])
    assert_equal 2, response['episodes'][0]['guests'].length
    assert_equal 'Sydney Watson', response['episodes'][0]['guests'][0]
    assert_equal 'Elijah Schaffer', response['episodes'][0]['guests'][1]

    assert_match(/^The Afghan War/, response['episodes'][1]['title'])
    assert_equal 1, response['episodes'][1]['guests'].length
    assert_equal 'Libby Emmons', response['episodes'][1]['guests'][0]
    assert_match(/^<p>Tim, Ian, and Lydia/, response['episodes'][1]['description'])
  end
end
