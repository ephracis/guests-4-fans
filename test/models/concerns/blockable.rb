# frozen_string_literal: true

module BlockableTests
  extend ActiveSupport::Testing::Declarative

  test 'should block resource' do
    user = create(:user)
    resource = create(@factory_name, blocked_at: nil, blocked_by: nil)

    assert_not resource.blocked?

    freeze_time do
      resource.block(user)
      assert resource.blocked?
      assert_equal user, resource.blocked_by
      assert_equal DateTime.now, resource.blocked_at
    end
  end
end
