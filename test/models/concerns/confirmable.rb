# frozen_string_literal: true

module ConfirmableTests
  extend ActiveSupport::Testing::Declarative

  test 'should confirm resource' do
    user = create(:user)
    resource = create(@factory_name, confirmed_at: nil, confirmed_by: nil)

    assert_not resource.confirmed?

    freeze_time do
      resource.confirm(user)

      assert resource.confirmed?
      assert_equal user, resource.confirmed_by
      assert_equal DateTime.now, resource.confirmed_at
    end
  end

  test 'should automatically confirm when root' do
    role = Role.find_or_create_by(name: :root, title: 'Root')
    user = create(:user, roles: [role])
    resource = create(@factory_name, confirmed_at: nil, confirmed_by: nil)
    resource.auto_confirm!
    assert_equal user, resource.confirmed_by
  end

  test 'should automatically confirm when admin' do
    role = Role.find_or_create_by(name: :admin, title: 'Admin')
    user = create(:user, roles: [role])
    resource = create(@factory_name, confirmed_at: nil, confirmed_by: nil)
    resource.auto_confirm!
    assert_equal user, resource.confirmed_by
  end

  test 'should automatically confirm when normal' do
    create(:user, roles: [])
    resource = create(@factory_name, confirmed_at: nil, confirmed_by: nil)
    resource.auto_confirm!
    assert_equal User.first, resource.confirmed_by
  end
end
