# frozen_string_literal: true

module TopicableTests
  extend ActiveSupport::Testing::Declarative

  test 'should assign topics' do
    resource = create(@factory_name)
    assert_equal 0, resource.topics.size
    resource.topics << create(:topic)
    assert_equal 1, resource.topics.size
  end
end
