# frozen_string_literal: true

module YoutubeBackendTests
  extend ActiveSupport::Testing::Declarative

  test 'should fetch episodes from youtube' do
    # mock http calls
    mock_channel_json = File.read('test/fixtures/files/sources/youtube_channel.json')
    mock_playlist_json = File.read('test/fixtures/files/sources/youtube_playlist.json')
    mock_http = mock
    mock_http.expects(:request)
             .twice
             .returns(mock(body: mock_channel_json))
             .then
             .returns(mock(body: mock_playlist_json))
    mock_http.expects('use_ssl=').twice.with(true)
    Net::HTTP.expects(:new).twice.with('www.googleapis.com', 443).returns(mock_http)

    # call fetch
    source = create(
      :youtube_source,
      filter_pattern: '^Timcast IRL - ',
      title_pattern: 'Timcast IRL - (?<title>.*)',
      guests_pattern: '(w|ft)/(?<names>.*)'
    )
    response = source.fetch

    # assert response
    assert_equal 'youtube', response['source']['kind']
    assert_equal 8, response['episodes'].length
    assert_equal 'o6Zmy-2je5E', response['episodes'][0]['source_id']

    assert_match(/^Biden/, response['episodes'][0]['title'])
    assert_equal 1, response['episodes'][0]['guests'].length
    assert_equal 'Joey Salads', response['episodes'][0]['guests'][0]

    assert_match(/^Larry/, response['episodes'][1]['title'])
    assert_equal 2, response['episodes'][1]['guests'].length
    assert_equal 'Sean Parnell', response['episodes'][1]['guests'][0]
    assert_equal 'Peter Quinones', response['episodes'][1]['guests'][1]
    assert_match(/\n\nGuests: \n/, response['episodes'][1]['description'])
  end
end
