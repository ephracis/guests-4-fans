# frozen_string_literal: true

require 'test_helper'

class BookTest < ActiveSupport::TestCase
  include ConfirmableTests
  include TopicableTests
  include ImageableTests
  include RateableTests

  setup do
    @factory_name = :book
  end

  test 'should save book' do
    book = build(:book)
    assert book.valid?
    assert book.save
  end

  test 'should associate book with authors' do
    books = create_list(:book, 3, authors: [])
    people = create_list(:person, 3)

    books[0].authors << people[0]
    books[0].authors << people[1]
    books[1].authors << people[1]

    assert_equal 2, books[0].authors.count
    assert_equal 1, books[1].authors.count
    assert_equal 0, books[2].authors.count
    assert_equal 1, people[0].books.count
    assert_equal 2, people[1].books.count
    assert_equal 0, people[2].books.count
  end

  test 'should clean name' do
    str1 = "This is you're  string"
    str2 = 'This is you’re string'
    assert_equal Book.clean_name(str1), Book.clean_name(str2)
  end

  test 'should find same title' do
    book = create(:book, name: "We're ALWAYS winning")
    assert_equal book, Book.with_name('We’re   always winning ').first
  end

  test 'should get external links' do
    book = create(:book, amazon: nil)
    assert_not book.external_links?
    book.amazon = 'something'
    assert book.external_links?
  end
end
