# frozen_string_literal: true

require 'test_helper'

class VoteTest < ActiveSupport::TestCase
  test 'should save vote' do
    vote = build(:vote)
    assert vote.valid?
    assert vote.save
  end

  test 'should require IP or user' do
    vote = build(:vote, ip_address: nil, user: nil)
    assert_not vote.valid?

    vote.ip_address = '127.0.0.1'
    assert vote.valid?

    vote.ip_address = nil
    vote.user = create(:user)
    assert vote.valid?
  end

  test 'should require unique IP' do
    create(:vote, user: nil, ip_address: '127.0.0.1')
    vote = build(:vote, user: nil, ip_address: '127.0.0.1')
    assert_not vote.valid?

    vote.ip_address = '192.168.0.1'
    assert vote.valid?
  end
end
