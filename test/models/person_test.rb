# frozen_string_literal: true

require 'test_helper'

class PersonTest < ActiveSupport::TestCase
  # include ConfirmableTests
  # include BlockableTests
  # include TopicableTests
  # include ImageableTests
  # include RateableTests
  include AliasableTests

  setup do
    @factory_name = :person
    @aliased_field = :name
  end

  test 'should save person' do
    person = build(:person)
    assert person.valid?
    assert person.save
  end

  test 'should cast to string' do
    person = create(:person, name: 'Mr Person')
    assert_equal 'Mr Person', person.to_s
  end

  test 'search should find with exact match' do
    create(:person)
    person = create(:person, name: 'Very Unique Name', slug: 'very-unique-name')
    results = Person.search(nil)
    assert_equal Person.all.length, results.length

    results = Person.search(person.name)
    assert_equal 1, results.length
    assert_equal person.id, results.first.id
  end

  test 'should generate slug' do
    create(:person, name: 'Test Person', slug: 'test-person')
    create(:person, name: 'Test Person 1', slug: 'test-person1')

    person = build(:person, name: 'Test-Person', slug: nil)
    assert_not person.valid?
    person.generate_slug
    assert person.valid?
    assert_equal 'test-person2', person.slug
  end

  test 'is favorited' do
    user1 = create(:user)
    user2 = create(:user)
    person = create(:person)

    create(:favorite, user: user1, favoritable: person)

    assert person.favorited_by?(user1), 'Did not report guest as favorited by user'
    assert_not person.favorited_by?(user2), 'Incorrectly reported guest as favorited by user'
  end

  test 'should delete person' do
    person = create(:person)
    create(:appearance, person: person)
    assert_difference 'Appearance.count', -1 do
      assert_difference 'Person.count', -1 do
        assert person.destroy
      end
    end
  end

  test 'should get vote count' do
    person = create(:person)
    create_list(:vote, 3)
    show = create(:show)

    assert_equal 0, person.vote_count
    create(:vote, person: person, show: show)
    person.reload
    assert_equal 1, person.vote_count
    assert_equal 1, Person.order_by_votes[0].vote_count
  end

  test 'should get any votes' do
    person = create(:person)
    create_list(:vote, 3)

    assert_not person.any_votes_for?
    create(:vote, person: person)
    person.reload
    assert person.any_votes_for?
  end

  test 'should get votes for show' do
    show = create(:show)
    person = create(:person)
    create_list(:vote, 3)

    assert_not person.any_votes_for?(show: show)
    create(:vote, person: person, show: show)
    person.reload
    assert person.any_votes_for?(show: show)
  end

  test 'should get votes for ip address' do
    person = create(:person)
    create_list(:vote, 3, ip_address: '1.2.3.4')

    assert_not person.any_votes_for?(user: '1.1.1.1')
    create(:vote, person: person, ip_address: '1.1.1.1')
    person.reload
    assert person.any_votes_for?(user: '1.1.1.1')
  end

  test 'should get votes for user' do
    user = create(:user)
    person = create(:person)
    create_list(:vote, 3)

    assert_not person.any_votes_for?(user: user)
    create(:vote, person: person, user: user)
    person.reload
    assert person.any_votes_for?(user: user)
  end

  test 'should order by votes' do
    shows = create_list(:show, 2)

    # test edge case empty
    assert_equal [], Person.order_by_votes
    assert_equal [], shows[0].people.order_by_votes
    assert_equal [], shows[0].new_people.order_by_votes

    # create three people and give them different count of votes
    people = create_list(:person, 3)
    create_list(:vote, 3, person: people[0], show: shows[0])
    create_list(:vote, 5, person: people[1], show: shows[0])

    # assert ordered list for different relations
    [Person, shows[0].new_people, shows[1].new_people].each do |resource|
      assert_equal 3, resource.order_by_votes.length
      assert_equal people[1], resource.order_by_votes[0]
      assert_equal people[0], resource.order_by_votes[1]
      assert_equal people[2], resource.order_by_votes[2]
    end

    # edge cases
    assert_equal [], shows[0].people.order_by_votes
    assert_equal [], shows[1].people.order_by_votes
  end

  test 'should order by votes on show' do
    shows = create_list(:show, 2)

    # test edge case empty
    assert_equal [], Person.order_by_votes(shows[0])
    assert_equal [], shows[0].people.order_by_votes(shows[0])
    assert_equal [], shows[0].new_people.order_by_votes(shows[0])

    # create three people and give them different count of votes
    people = create_list(:person, 3)
    create_list(:vote, 3, person: people[0], show: shows[0])
    create_list(:vote, 5, person: people[1], show: shows[0])
    create_list(:vote, 3, person: people[0], show: shows[1])
    create_list(:vote, 1, person: people[2], show: shows[1])

    [Person, shows[0].new_people, shows[1].new_people].each do |resource|
      assert_equal 3, resource.order_by_votes(shows[0]).length
      assert_equal people[1], resource.order_by_votes(shows[0])[0]
      assert_equal people[0], resource.order_by_votes(shows[0])[1]
      assert_equal people[2], resource.order_by_votes(shows[0])[2]
    end

    # edge cases
    assert_equal [], shows[0].people.order_by_votes(shows[0])
    assert_equal [], shows[1].people.order_by_votes(shows[0])
  end

  test 'should mark person as duplicate' do
    person1 = create(:person, name: 'Person Testson', description: nil)
    person2 = create(:person, name: 'P. Testson', description: 'A nice description.')

    create(:vote, person: person1)
    create(:vote, person: person2)
    create(:vote, person: person2)

    assert_difference 'Person.count', -1 do
      assert_difference 'person1.votes.count', 2 do
        person2.duplicate_of! person1
      end
    end

    assert_equal 'Person Testson', person1.name
    assert_equal 'A nice description.', person1.description
    assert_equal person1, Slug.find_by(name: person2.slug, slugable_type: 'Person').slugable
    assert_equal person1, Person.find_by_alias(person2.name)
  end

  test 'should not mark as duplicate of an episode' do
    person = create(:person)
    show = create(:show)

    assert_raises RuntimeError do
      person.duplicate_of! show
    end
  end
end
