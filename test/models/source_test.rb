# frozen_string_literal: true

require 'test_helper'

class SourceTest < ActiveSupport::TestCase
  include YoutubeBackendTests
  include SpotifyBackendTests

  test 'should save source' do
    source = build(:source)
    assert source.valid?
    assert source.save
  end

  test 'should validate kind' do
    source = build(:source)

    source.kind = :spotify
    assert source.valid?

    source.kind = :another
    assert_not source.valid?

    source.kind = nil
    assert_not source.valid?
  end

  test 'should not create duplicate source for show' do
    show = create(:show)
    create(:source, show: show, kind: :spotify)
    source = build(:source, show: show, kind: :spotify)
    assert_not source.valid?
    assert_not source.save
  end

  test 'should parse guests in titles' do
    test_cases = YAML.load_file('test/fixtures/files/sources/episode_title_tests.yml')
    test_cases.each do |test_case|
      source = create(:source)
      source.guests_pattern = test_case['guests_pattern'] if test_case['guests_pattern'].present?
      source.split_pattern = test_case['split_pattern'] if test_case['split_pattern'].present?

      test_case['titles'].each do |title|
        assert_equal title['guests'], source.find_guests_in_title(title['text'])
      end
    end
  end

  test 'should get default foreign id from show' do
    show = create(:show, spotify: 'my-spotify-id', youtube: 'my-youtube-id')
    source = create(:source, show: show)
    assert_equal 'my-spotify-id', source.default_foreign_id

    source.kind = 'youtube'
    assert_equal 'my-youtube-id', source.default_foreign_id
  end
end
