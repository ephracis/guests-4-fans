# frozen_string_literal: true

require 'test_helper'

class EditTest < ActiveSupport::TestCase
  test 'should apply edit' do
    user = create(:user)
    show = create(:show, title: 'My Title')
    edit = create(:edit, editable: show, name: 'title', value: 'New Edited Title')

    assert edit.pending?
    assert_not edit.applied?
    assert_not edit.discarded?

    assert_difference 'Edit.count' do
      edit.apply!(user)
      show.reload
    end

    # assert_not edit.pending?
    assert edit.applied?
    assert_not edit.discarded?

    assert_equal 'New Edited Title', show.title
    assert_equal 'title', edit.previous.name
    assert_equal 'My Title', edit.previous.value
    assert_equal 2, show.edits.count
  end

  test 'should discard edit' do
    user = create(:user)
    show = create(:show, title: 'My Title')
    edit = create(:edit, editable: show, name: 'title', value: 'New Edited Title')

    assert edit.pending?
    assert_not edit.applied?
    assert_not edit.discarded?

    assert_no_difference 'Edit.count' do
      edit.discard!(user)
      show.reload
    end

    assert_not edit.pending?
    assert_not edit.applied?
    assert edit.discarded?

    assert_equal 'My Title', show.title
    assert_equal 1, show.discarded_edits.count
  end

  test 'should not create duplicates' do
    user = create(:user)
    person = create(:person)
    create(:edit, editable: person, created_by: user, name: 'description', value: 'something')
    edit = build(:edit, editable: person, created_by: user, name: 'description', value: 'something else')
    assert_not edit.valid?
    assert_not edit.save
  end

  test 'should build chain' do
    episode = create(:episode)
    edit1 = create(:edit, editable: episode)
    edit2 = create(:edit, editable: episode, previous: edit1)
    edit3 = create(:edit, editable: episode, previous: edit2)

    assert_equal edit2, edit1.next
    assert_equal edit3, edit2.next
  end
end
