# frozen_string_literal: true

require 'test_helper'

class HostingTest < ActiveSupport::TestCase
  test 'person can host a show' do
    person = create(:person)
    show = create(:show)
    hosting = Hosting.new(person: person, show: show)

    assert hosting.valid?
    assert hosting.save
  end

  test 'person cannot host a show twice' do
    person = create(:person)
    show = create(:show)
    Hosting.create(person: person, show: show)
    hosting = Hosting.new(person: person, show: show)

    assert_not hosting.valid?
    assert_not hosting.save
  end

  test 'can access people hosting a show' do
    people = create_list(:person, 5)
    shows = create_list(:show, 3)

    create(:hosting, show: shows[0], person: people[0])
    create(:hosting, show: shows[0], person: people[1])
    create(:hosting, show: shows[0], person: people[2])
    create(:hosting, show: shows[1], person: people[2])
    create(:hosting, show: shows[1], person: people[3])

    assert_equal 3, shows[0].hosts.count
    assert_equal 2, shows[1].hosts.count
    assert_equal 0, shows[2].hosts.count

    assert_equal people[0], shows[0].hosts.first
    assert_equal people[2], shows[1].hosts.first
  end

  test 'can access hosted shows for a person' do
    show = create(:show)
    people = create_list(:person, 2)

    create(:hosting, person: people[0], show: show)

    assert_equal 1, people[0].hosted_shows.count
    assert_equal 0, people[1].hosted_shows.count
    assert_equal show, people[0].hosted_shows.first
  end
end
