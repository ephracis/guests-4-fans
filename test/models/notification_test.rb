# frozen_string_literal: true

require 'test_helper'

class NotificationTest < ActiveSupport::TestCase
  test 'should save notification' do
    notification = build(:notification)
    assert notification.valid?
    assert notification.save
  end
end
