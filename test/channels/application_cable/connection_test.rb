# frozen_string_literal: true

require 'test_helper'

class ApplicationCable::ConnectionTest < ActionCable::Connection::TestCase
  test 'connects with params' do
    connect params: { id: 42 }
    assert_equal 'id=42', connection.env['QUERY_STRING']
  end
end
