# frozen_string_literal: true

require 'test_helper'

class SourcePolicyTest < ActiveSupport::TestCase
  setup do
    @user_root = create(:root_user)
    @user_admin = create(:admin_user)
    @user_limited = create(:limited_user)
    @user_normal = create(:user)
    @user_blocked = create(:blocked_user)

    @resource = create(:source)
  end

  def test_create
    assert_permit @user_root, @resource, :create
    assert_permit @user_admin, @resource, :create
    refute_permit @user_normal, @resource, :create
    refute_permit @user_limited, @resource, :create
    refute_permit @user_blocked, @resource, :create
  end

  def test_update
    assert_permit @user_root, @resource, :update
    assert_permit @user_admin, @resource, :update
    refute_permit @user_normal, @resource, :update
    refute_permit @user_limited, @resource, :update
    refute_permit @user_blocked, @resource, :update
  end

  def test_destroy
    assert_permit @user_root, @resource, :destroy
    assert_permit @user_admin, @resource, :destroy
    refute_permit @user_normal, @resource, :destroy
    refute_permit @user_limited, @resource, :destroy
    refute_permit @user_blocked, @resource, :destroy
  end

  def test_fetch
    assert_permit @user_root, @resource, :fetch
    assert_permit @user_admin, @resource, :fetch
    refute_permit @user_normal, @resource, :fetch
    refute_permit @user_limited, @resource, :fetch
    refute_permit @user_blocked, @resource, :fetch
  end

  def test_generate
    assert_permit @user_root, @resource, :generate
    assert_permit @user_admin, @resource, :generate
    refute_permit @user_normal, @resource, :generate
    refute_permit @user_limited, @resource, :generate
    refute_permit @user_blocked, @resource, :generate
  end
end
