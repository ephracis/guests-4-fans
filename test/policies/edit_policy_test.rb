# frozen_string_literal: true

require 'test_helper'

class EditPolicyTest < ActiveSupport::TestCase
  setup do
    @user_root = create(:root_user)
    @user_admin = create(:admin_user)
    @user_limited = create(:limited_user)
    @user_normal = create(:user)
    @user_blocked = create(:blocked_user)

    @resource = create(:edit)
  end

  def test_update
    assert_permit @user_root, @resource, :update
    assert_permit @user_admin, @resource, :update
    refute_permit @user_normal, @resource, :update
    refute_permit @user_limited, @resource, :update
    refute_permit @user_blocked, @resource, :update
    assert_permit @resource.created_by, @resource, :update
  end

  def test_destroy
    assert_permit @user_root, @resource, :destroy
    assert_permit @user_admin, @resource, :destroy
    refute_permit @user_normal, @resource, :destroy
    refute_permit @user_limited, @resource, :destroy
    refute_permit @user_blocked, @resource, :destroy
    assert_permit @resource.created_by, @resource, :destroy
  end

  def test_apply
    assert_permit @user_root, @resource, :apply
    assert_permit @user_admin, @resource, :apply
    refute_permit @user_normal, @resource, :apply
    refute_permit @user_limited, @resource, :apply
    refute_permit @user_blocked, @resource, :apply
    refute_permit @resource.created_by, @resource, :apply
  end

  def test_discard
    assert_permit @user_root, @resource, :discard
    assert_permit @user_admin, @resource, :discard
    refute_permit @user_normal, @resource, :discard
    refute_permit @user_limited, @resource, :discard
    refute_permit @user_blocked, @resource, :discard
    refute_permit @resource.created_by, @resource, :discard
  end
end
