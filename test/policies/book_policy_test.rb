# frozen_string_literal: true

require 'test_helper'

class BookPolicyTest < ActiveSupport::TestCase
  setup do
    @user_root = create(:root_user)
    @user_admin = create(:admin_user)
    @user_limited = create(:limited_user)
    @user_normal = create(:user)
    @user_blocked = create(:blocked_user)

    @resource = create(:book)
  end

  def test_create
    assert_permit @user_root, @resource, :create
    assert_permit @user_admin, @resource, :create
    assert_permit @user_normal, @resource, :create
    refute_permit @user_limited, @resource, :create
    refute_permit @user_blocked, @resource, :create
  end

  def test_edit
    assert_permit @user_root, @resource, :edit
    assert_permit @user_admin, @resource, :edit
    refute_permit @user_normal, @resource, :edit
    refute_permit @user_limited, @resource, :edit
    refute_permit @user_blocked, @resource, :edit
  end

  def test_update
    assert_permit @user_root, @resource, :update
    assert_permit @user_admin, @resource, :update
    refute_permit @user_normal, @resource, :update
    refute_permit @user_limited, @resource, :update
    refute_permit @user_blocked, @resource, :update
  end

  def test_destroy
    assert_permit @user_root, @resource, :destroy
    assert_permit @user_admin, @resource, :destroy
    refute_permit @user_normal, @resource, :destroy
    refute_permit @user_limited, @resource, :destroy
    refute_permit @user_blocked, @resource, :destroy
  end

  def test_confirm
    assert_permit @user_root, @resource, :confirm
    assert_permit @user_admin, @resource, :confirm
    refute_permit @user_normal, @resource, :confirm
    refute_permit @user_limited, @resource, :confirm
    refute_permit @user_blocked, @resource, :confirm
  end

  def test_block
    assert_permit @user_root, @resource, :block
    assert_permit @user_admin, @resource, :block
    refute_permit @user_normal, @resource, :block
    refute_permit @user_limited, @resource, :block
    refute_permit @user_blocked, @resource, :block
  end
end
