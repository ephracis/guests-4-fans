# frozen_string_literal: true

require 'test_helper'

class RolePolicyTest < ActiveSupport::TestCase
  setup do
    @user_root = create(:root_user)
    @user_admin = create(:admin_user)
    @user_limited = create(:limited_user)
    @user_normal = create(:user)
    @user_blocked = create(:blocked_user)

    @resource = create(:role)
  end

  def test_create
    assert_permit @user_root, @resource, :create
    refute_permit @user_admin, @resource, :create
    refute_permit @user_normal, @resource, :create
    refute_permit @user_limited, @resource, :create
    refute_permit @user_blocked, @resource, :create
  end

  def test_update
    assert_permit @user_root, @resource, :update
    refute_permit @user_admin, @resource, :update
    refute_permit @user_normal, @resource, :update
    refute_permit @user_limited, @resource, :update
    refute_permit @user_blocked, @resource, :update
  end

  def test_destroy
    assert_permit @user_root, @resource, :destroy
    refute_permit @user_admin, @resource, :destroy
    refute_permit @user_normal, @resource, :destroy
    refute_permit @user_limited, @resource, :destroy
    refute_permit @user_blocked, @resource, :destroy
  end
end
