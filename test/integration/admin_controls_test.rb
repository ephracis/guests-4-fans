# frozen_string_literal: true

require 'test_helper'

class AdminControlsTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @root = create(:root_user)
    @root.confirm
    @admin = create(:admin_user)
    @admin.confirm
    @limited = create(:limited_user)
    @limited.confirm
    @user = create(:user)
    @user.confirm
  end

  test 'administrative controls for root users' do
    sign_in @root

    user = create(:user)
    get user_url(user)
    assert_select '#sidebar', text: /Remove/, count: 1

    get shows_url
    assert_select '.container-lg', text: /Add show/, count: 1

    show = create(:show)
    get show_url(show)
    assert_select '#sidebar', text: /Edit/, count: 1
    assert_select '#sidebar', text: /Remove/, count: 1
    assert_select '.card-footer', text: /Add episode/, count: 1

    episode = create(:episode, show: show)
    get show_episode_url(show, episode)
    assert_select '#sidebar', text: /Edit/, count: 1
    assert_select '#sidebar', text: /Remove/, count: 1

    person = create(:person)
    get person_url(person)
    assert_select '.profile a[title="Edit"]', count: 1
    assert_select '.profile a[title="Remove"]', count: 1
  end

  test 'administrative controls for admin users' do
    sign_in @root

    user = create(:user)
    get user_url(user)
    assert_select '#sidebar', text: /Remove/, count: 1

    get shows_url
    assert_select '.container-lg', text: /Add show/, count: 1

    show = create(:show)
    get show_url(show)
    assert_select '#sidebar', text: /Edit/, count: 1
    assert_select '#sidebar', text: /Remove/, count: 1
    assert_select '.card-footer', text: /Add episode/, count: 1

    episode = create(:episode, show: show)
    get show_episode_url(show, episode)
    assert_select '#sidebar', text: /Edit/, count: 1
    assert_select '#sidebar', text: /Remove/, count: 1

    person = create(:person)
    get person_url(person)
    assert_select '.profile a[title="Edit"]', count: 1
    assert_select '.profile a[title="Remove"]', count: 1
  end

  test 'administrative controls for normal users' do
    sign_in @user

    user = create(:user)
    get user_url(user)
    assert_select '#sidebar', text: /Remove/, count: 0

    get shows_url
    assert_select '.container-lg', text: /Add show/, count: 1

    show = create(:show)
    get show_url(show)
    assert_select '#sidebar', text: /Remove/, count: 0
    assert_select '#episodes', text: /Add episode/, count: 0

    episode = create(:episode, show: show)
    get show_episode_url(show, episode)
    assert_select '#sidebar', text: /Remove/, count: 0

    person = create(:person)
    get person_url(person)
    assert_select '#sidebar', text: /Remove/, count: 0
  end

  test 'administrative controls for limited users' do
    sign_in @limited

    user = create(:user)
    get user_url(user)
    assert_select '#sidebar', text: /Remove/, count: 0

    get shows_url
    assert_select '.container', text: /Add show/, count: 0

    show = create(:show)
    get show_url(show)
    assert_select '#sidebar', text: /Edit/, count: 0
    assert_select '#sidebar', text: /Remove/, count: 0
    assert_select '#episodes', text: /Add episode/, count: 0

    episode = create(:episode, show: show)
    get show_episode_url(show, episode)
    assert_select '#sidebar', text: /Edit/, count: 0
    assert_select '#sidebar', text: /Remove/, count: 0

    person = create(:person)
    get person_url(person)
    assert_select '#sidebar', text: /Edit/, count: 0
    assert_select '#sidebar', text: /Remove/, count: 0
  end
end
