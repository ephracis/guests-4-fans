# frozen_string_literal: true

require 'test_helper'

class AdminNavbarTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @user = create(:user)
    @user.confirm
    sign_in @user
  end

  test 'should not show admin when there is another user with root access' do
    admin = create(:user)
    admin.roles << Role.create(name: :root, title: 'Root')
    get root_url
    assert_select 'nav', text: /Admin/, count: 0
  end

  test 'should show admin when sign in with root access' do
    create(:user)
    @user.roles << Role.create(name: :root, title: 'Root')
    get root_url
    assert_select 'nav', text: /Admin/, count: 1
  end
end
