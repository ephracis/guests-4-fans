# frozen_string_literal: true

require 'test_helper'

class UserAuthTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  test 'should get sign in page' do
    get new_user_session_url
    assert_select 'input#user_login'
    assert_select 'input#user_password'
  end

  test 'should get sign up page' do
    get new_user_registration_url
    assert_select 'input#user_email'
    assert_select 'input#user_username'
    assert_select 'input#user_password'
    assert_select 'input#user_password_confirmation'
  end

  test 'should register user' do
    assert_difference 'User.count' do
      post user_registration_url, params: {
        user: {
          email: 'test@mail.com',
          username: 'test',
          password: 'changeme',
          password_confirmation: 'changeme'
        }
      }
      assert_redirected_to root_url
    end
  end

  test 'should not register invalid user' do
    create(:user, email: 'taken@mail.com')
    user_params = {
      email: 'test@mail.com',
      username: 'test',
      password: 'changeme',
      password_confirmation: 'changeme'
    }

    assert_no_difference 'User.count' do
      post user_registration_url, params: { user: user_params.merge({ password: 'changeme1' }) }
      assert_response :success
      assert_select '.alert', /Password confirmation doesn't match Password/

      post user_registration_url, params: { user: user_params.merge({ username: 't' }) }
      assert_response :success
      assert_select '.alert', /Username is too short/

      post user_registration_url, params: { user: user_params.merge({ email: 'taken@mail.com' }) }
      assert_response :success
      assert_select '.alert', /Email has already been taken/
    end
  end

  test 'should not allow bots to register' do
    RegistrationsController.any_instance.expects(:verify_recaptcha).returns(false)

    assert_no_difference 'User.count' do
      post user_registration_url, params: {
        user: {
          email: 'test@mail.com',
          username: 'test',
          password: 'changeme',
          password_confirmation: 'changeme'
        }
      }
      assert_response :success
      assert_select '.alert', /reCAPTCHA verification failed, please try again./
    end
  end

  test 'should sign in with email' do
    create(:user, email: 'test@mail.com', password: 'changeme')

    post user_session_url, params: {
      user: {
        login: 'test@mail.com',
        password: 'changeme'
      }
    }
    assert_redirected_to root_url
    follow_redirect!
    assert_select '.alert', /Signed in successfully./
  end

  test 'should sign in with username' do
    create(:user, username: 'test', password: 'changeme')

    post user_session_url, params: {
      user: {
        login: 'test',
        password: 'changeme'
      }
    }
    assert_redirected_to root_url
    follow_redirect!
    assert_select '.alert', /Signed in successfully./
  end

  test 'should not sign in with invalid credentials' do
    create(:user, email: 'test@mail.com', password: 'changeme')

    post user_session_url, params: {
      user: {
        login: 'test@mail.com',
        password: 'changeme2'
      }
    }
    assert_response :success
    assert_select '.alert', /Invalid Login or password./
  end

  test 'should not sign in bots' do
    SessionsController.any_instance.expects(:verify_recaptcha).returns(false)
    create(:user, email: 'test@mail.com', password: 'changeme')

    post user_session_url, params: {
      user: {
        login: 'test@mail.com',
        password: 'changeme'
      }
    }
    assert_response :success
    assert_select '.alert', /reCAPTCHA verification failed, please try again./
  end

  test 'should create through oauth' do
    OmniAuth.config.test_mode = true
    OmniAuth.config.mock_auth[:google_oauth2] = OmniAuth::AuthHash.new(
      {
        provider: 'google_oauth2',
        uid: '1234',
        info: {
          email: 'mrtest@mail.com',
          name: 'Mr. Test'
        }
      }
    )
    assert_difference 'User.count' do
      get user_google_oauth2_omniauth_callback_url
    end
    assert_redirected_to root_url
    assert_equal 'mrtest', User.last.username
    assert_equal 'Mr. Test', User.last.fullname
    assert_equal 'mrtest@mail.com', User.last.email
    assert User.last.no_password
    assert User.last.confirmed?
  end

  test 'should validate oauth users' do
    OmniAuth.config.test_mode = true
    OmniAuth.config.mock_auth[:google_oauth2] = OmniAuth::AuthHash.new(
      {
        provider: 'google_oauth2',
        uid: '1234',
        info: {
          email: 'x@mail.com',
          name: 'x'
        }
      }
    )
    assert_no_difference 'User.count' do
      get user_google_oauth2_omniauth_callback_url
    end
    assert_redirected_to new_user_registration_url
  end

  test 'should handle oauth errors' do
    OmniAuth.config.test_mode = true
    OmniAuth.config.mock_auth[:google_oauth2] = :invalid_credentials
    OmniAuth.config.logger = Rails.logger
    assert_no_difference 'User.count' do
      get user_google_oauth2_omniauth_callback_url
    end
    assert_redirected_to root_url
  end
end
