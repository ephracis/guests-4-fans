# frozen_string_literal: true

require 'test_helper'

class UserMailTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  test 'should resend confirmation' do
    create(:user, email: 'test@mail.com', confirmed_at: nil)
    post user_confirmation_url, params: { user: { email: 'test@mail.com' } }
    assert_redirected_to new_user_session_url
    follow_redirect!
    assert_select '.alert',
                  /You will receive an email with instructions for how to confirm your email address in a few minutes./
  end

  test 'should not resend confirmation when confirmed' do
    create(:user, email: 'test@mail.com')
    post user_confirmation_url, params: { user: { email: 'test@mail.com' } }
    assert_response :success
    assert_select '.alert', /Email was already confirmed, please try signing in/
  end

  test 'should not resend incorrect email' do
    post user_confirmation_url, params: { user: { email: 'test@mail.com' } }
    assert_response :success
    assert_select '.alert', /Email not found/
  end

  test 'should not allow bots to resend confirmations' do
    ConfirmationsController.any_instance.expects(:verify_recaptcha).returns(false)
    post user_confirmation_url, params: { user: { email: 'test@mail.com' } }
    assert_response :success
    assert_select '.alert', /reCAPTCHA verification failed, please try again./
  end

  test 'should send password reset' do
    create(:user, email: 'test@mail.com')
    post user_password_url, params: { user: { email: 'test@mail.com' } }
    assert_redirected_to new_user_session_url
    follow_redirect!
    assert_select '.alert',
                  /You will receive an email with instructions on how to reset your password in a few minutes./
  end

  test 'should not send password reset for invalid email' do
    post user_password_url, params: { user: { email: 'test@mail.com' } }
    assert_response :success
    assert_select '.alert', /Email not found/
  end

  test 'should not allow bots to send password reset' do
    PasswordsController.any_instance.expects(:verify_recaptcha).returns(false)
    post user_password_url, params: { user: { email: 'test@mail.com' } }
    assert_response :success
    assert_select '.alert', /reCAPTCHA verification failed, please try again./
  end

  test 'should edit registration' do
    user = create(:user, email: 'test@mail.com', username: 'test')
    sign_in user

    get edit_user_registration_url
    assert_response :success

    patch user_registration_url, params: { user: { username: 'newname' } }
    assert_redirected_to edit_user_registration_url
    assert_equal 'newname', User.find(user.id).username
  end
end
