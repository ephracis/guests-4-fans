# frozen_string_literal: true

require 'test_helper'

class BrowseTopicsTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  test 'should show topic' do
    topic = create(:topic)
    show = create(:show, topics: [topic])
    episode = create(:episode, show: show, topics: [topic])
    person = create(:person, topics: [topic])
    book = create(:book, authors: [person], topics: [topic])

    get topic_url(topic)
    assert_response :success

    assert_select '.card .card-header', text: 'Episodes'
    assert_select '.card .card-header', text: 'People'
    assert_select '.card .card-header', text: 'Shows'
    assert_select '.card .card-header', text: 'Books'
    assert_select '.list-group-item h5', text: show.title
    assert_select '.list-group-item h5', text: episode.title
    assert_select '.list-group-item h5', text: person.name
    assert_select '.card-body a', text: book.name
  end
end
