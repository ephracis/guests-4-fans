# frozen_string_literal: true

require 'test_helper'

class AchievementsTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  test 'should grant badge on user update' do
    user = create(:user, twitter: nil, facebook: nil, fullname: 'Test', country: nil, email_notifications: false)
    sign_in user

    put user_registration_path, params: { user: { fullname: 'Test' } }
    user.reload
    assert_equal 0, user.badges.count

    put user_registration_path, params: { user: { fullname: 'Tester' } }
    user.reload
    assert_equal 1, user.badges.count
    assert_equal 'user-fullname', user.badges.last.name

    put user_registration_path, params: { user: { country: 'Sweden' } }
    user.reload
    assert_equal 2, user.badges.count
    assert_equal 'user-nation', user.badges.last.name

    put user_registration_path, params: { user: { email_notifications: true } }
    user.reload
    assert_equal 3, user.badges.count
    assert_equal 'user-email', user.badges.last.name

    put user_registration_path, params: { user: { twitter: 'test', facebook: 'test' } }
    user.reload
    assert_equal 4, user.badges.count
    assert_equal 'user-social', user.badges.last.name
  end

  test 'should grant badge on first vote' do
    user = create(:user)
    show = create(:show)
    person = create(:person)

    sign_in user
    assert_difference 'user.reload.badges.count' do
      post votes_url(format: :json), params: { vote: { value: 1, person_id: person.id, show_id: show.id } }
    end
    user.reload

    assert_equal 'voter', user.badges.last.name
    assert_equal 1, user.badges.last.level
  end

  test 'should grant badge on tenth vote' do
    user = create(:user)
    show = create(:show)
    person = create(:person)
    create_list(:vote, 9, user: user)

    sign_in user
    assert_difference 'user.reload.badges.count', 3 do
      post votes_url(format: :json), params: { vote: { value: 1, person_id: person.id, show_id: show.id } }
    end
    user.reload

    assert_equal 'voter', user.badges.last.name
    assert_equal 3, user.badges.last.level
  end

  test 'should grant badge on first person' do
    user = create(:user)
    admin = create(:admin_user)

    sign_in user
    post people_url, params: { person: { name: 'My Person' } }
    person = Person.last
    assert_redirected_to person_url(person)
    user.reload
    assert_equal 0, user.badges.count

    sign_in admin
    put person_url(person), params: { person: { confirmed: '1' } }
    assert_redirected_to admin_unconfirmed_url
    person.reload
    assert person.confirmed?
    assert_equal admin, person.confirmed_by

    user.reload
    assert_equal 1, user.badges.count
    assert_equal 'person-creator', user.badges.last.name
    assert_equal 1, user.badges.last.level
  end

  test 'should grant badge on first show' do
    user = create(:user)
    admin = create(:admin_user)

    sign_in user
    post shows_url, params: { show: { title: 'My Show' } }
    show = Show.last
    assert_redirected_to show_url(show)
    user.reload
    assert_equal 0, user.badges.count

    sign_in admin
    put show_url(show), params: { show: { confirmed: '1' } }
    assert_redirected_to admin_unconfirmed_url
    show.reload
    assert show.confirmed?
    assert_equal admin, show.confirmed_by

    user.reload
    assert_equal 1, user.badges.count
    assert_equal 'show-creator', user.badges.last.name
    assert_equal 1, user.badges.last.level
  end

  test 'should grant badge on first episode' do
    user = create(:user)
    show = create(:show)
    admin = create(:admin_user)

    sign_in user
    post show_episodes_url(show), params: { episode: { title: 'My Episode' } }
    episode = Episode.last
    assert_redirected_to show_episode_url(show, episode)
    user.reload
    assert_equal 0, user.badges.count

    sign_in admin
    put show_episode_url(show, episode), params: { episode: { confirmed: '1' } }
    assert_redirected_to admin_unconfirmed_url
    episode.reload
    assert episode.confirmed?
    assert_equal admin, episode.confirmed_by

    user.reload
    assert_equal 1, user.badges.count
    assert_equal 'episode-creator', user.badges.last.name
    assert_equal 1, user.badges.last.level
  end

  test 'should grant badge on first edit' do
    user = create(:user)
    show = create(:show)
    admin = create(:admin_user)

    sign_in user
    put show_url(show), params: { show: { title: 'New Title' } }
    assert_redirected_to show_url(show)
    user.reload
    assert_equal 0, user.badges.count

    sign_in admin
    patch apply_edit_url(Edit.last)
    assert_redirected_to show_url(show)
    show.reload
    assert_equal 'New Title', show.title

    user.reload
    assert_equal 1, user.badges.count
    assert_equal 'editor', user.badges.last.name
    assert_equal 1, user.badges.last.level
  end
end
