# frozen_string_literal: true

require 'test_helper'

class UserEditTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  test 'should edit registration' do
    user = create(:user, email: 'test@mail.com', username: 'test')
    sign_in user

    get edit_user_registration_url
    assert_response :success

    patch user_registration_url, params: { user: { username: 'newname' } }
    assert_redirected_to edit_user_registration_url
    assert_equal 'newname', User.find(user.id).username
  end
end
