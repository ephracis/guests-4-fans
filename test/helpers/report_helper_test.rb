# frozen_string_literal: true

require 'test_helper'

class ReportHelperTest < ActionView::TestCase
  test 'should get report url' do
    report = create(:report)
    assert_equal "http://test.host/admin/reports/#{report.id}", report_url(report)
  end

  test 'should get report path' do
    report = create(:report)
    assert_equal "/admin/reports/#{report.id}", report_path(report)
  end
end
