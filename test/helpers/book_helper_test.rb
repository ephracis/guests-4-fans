# frozen_string_literal: true

require 'test_helper'

class BookHelperTest < ActionView::TestCase
  test 'should get book image' do
    test_image = File.open('test/fixtures/files/images/timcast.png')
    URI::HTTP.any_instance.expects(:open).returns(test_image)
    book = create(:book, image: nil)
    assert_equal '/images/placeholder-book.png', book_image(book)
    book.attach_image 'http://example.com/image.jpg'
    assert_match 'image.jpg', File.basename(book_image(book))
  end
end
