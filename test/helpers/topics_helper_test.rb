# frozen_string_literal: true

require 'test_helper'

class TopicsHelperTest < ActionView::TestCase
  test 'should get topics' do
    show = create(:show)
    show.topics << create(:topic)
    show.topics << create(:topic)

    topic_list = topics_for(show)

    assert_match(/<ul class="list-inline topic-list"/, topic_list)
    assert_match(/<li class="list-inline-item topic-list-item"/, topic_list)
    assert_match(/<a class="badge/, topic_list)
  end
end
