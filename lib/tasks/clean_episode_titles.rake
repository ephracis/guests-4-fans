# frozen_string_literal: true

desc 'Clean titles in episodes'
task :clean_episode_titles, [:show_slug] => [:environment] do |_task, args|
  show = Show.find_by!(slug: args[:show_slug])

  if show.sources.any?
    source = show.sources.first

    if source.title_pattern.present?

      print "Cleaning #{show.title}"
      show.episodes.each do |episode|
        new_title = source.clean_title(episode.title)
        if new_title.present? && new_title.length > 2
          episode.update(title: new_title)
          print '.'
        else
          print '!'
        end
      end
    else
      puts "Skipping #{show.title}: No title pattern in source."
    end
  else
    puts "Skipping #{show.title}: No source found."
  end
end
