# frozen_string_literal: true

namespace :fetch do
  desc 'Fetch episodes from external source'
  task episodes: :environment do
    Source.where(scheduled: true).each do |source|
      log_info "Fetching from #{source.kind.capitalize} for #{source.show.title}"
      begin
        source.show.episodes.from_source source.fetch
      rescue StandardError => e
        log_error "Error fetching episodes: #{e.message}"
      end
    end
  end
end

def log_info(msg)
  Rails.logger.info msg
  puts msg
end

def log_error(msg)
  Rails.logger.error msg
  puts msg
end

task fetch: ['fetch:episodes']
