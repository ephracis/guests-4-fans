# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2022_02_26_131030) do
  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.integer "record_id", null: false
    t.integer "blob_id", null: false
    t.datetime "created_at", precision: nil, null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.string "service_name", null: false
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", precision: nil, null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.integer "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "aliases", force: :cascade do |t|
    t.string "name"
    t.string "aliasable_type", null: false
    t.integer "aliasable_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["aliasable_type", "aliasable_id"], name: "index_aliases_on_aliasable"
  end

  create_table "appearances", force: :cascade do |t|
    t.integer "person_id", null: false
    t.integer "episode_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["episode_id"], name: "index_appearances_on_episode_id"
    t.index ["person_id"], name: "index_appearances_on_person_id"
  end

  create_table "application_settings", force: :cascade do |t|
    t.integer "max_votes_per_show", default: 5
    t.integer "max_votes_per_ip", default: 1
  end

  create_table "badges_sashes", force: :cascade do |t|
    t.integer "badge_id"
    t.integer "sash_id"
    t.boolean "notified_user", default: false
    t.datetime "created_at", precision: nil
    t.index ["badge_id", "sash_id"], name: "index_badges_sashes_on_badge_id_and_sash_id"
    t.index ["badge_id"], name: "index_badges_sashes_on_badge_id"
    t.index ["sash_id"], name: "index_badges_sashes_on_sash_id"
  end

  create_table "books", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.text "description"
    t.integer "created_by_id", null: false
    t.datetime "confirmed_at", precision: nil
    t.integer "confirmed_by_id"
    t.datetime "blocked_at", precision: nil
    t.integer "blocked_by_id"
    t.datetime "published_at", precision: nil
    t.string "amazon"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["blocked_by_id"], name: "index_books_on_blocked_by_id"
    t.index ["confirmed_by_id"], name: "index_books_on_confirmed_by_id"
    t.index ["created_by_id"], name: "index_books_on_created_by_id"
  end

  create_table "books_people", id: false, force: :cascade do |t|
    t.integer "person_id", null: false
    t.integer "book_id", null: false
  end

  create_table "edits", force: :cascade do |t|
    t.string "name"
    t.string "value"
    t.integer "created_by_id", null: false
    t.string "editable_type", null: false
    t.integer "editable_id", null: false
    t.boolean "discarded", default: false
    t.datetime "discarded_at", precision: nil
    t.integer "discarded_by_id"
    t.boolean "applied", default: false
    t.datetime "applied_at", precision: nil
    t.integer "applied_by_id"
    t.integer "previous_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["applied_by_id"], name: "index_edits_on_applied_by_id"
    t.index ["created_by_id"], name: "index_edits_on_created_by_id"
    t.index ["discarded_by_id"], name: "index_edits_on_discarded_by_id"
    t.index ["editable_type", "editable_id"], name: "index_edits_on_editable"
    t.index ["previous_id"], name: "index_edits_on_previous_id"
  end

  create_table "episodes", force: :cascade do |t|
    t.string "slug"
    t.string "title"
    t.string "youtube"
    t.string "rumble"
    t.string "description"
    t.integer "show_id", null: false
    t.integer "length"
    t.datetime "aired_at", precision: nil
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "spotify"
    t.string "apple_podcast"
    t.string "website"
    t.integer "created_by_id"
    t.integer "confirmed_by_id"
    t.datetime "confirmed_at", precision: nil
    t.integer "blocked_by_id"
    t.datetime "blocked_at", precision: nil
    t.boolean "generated", default: false
    t.index ["blocked_by_id"], name: "index_episodes_on_blocked_by_id"
    t.index ["confirmed_by_id"], name: "index_episodes_on_confirmed_by_id"
    t.index ["created_by_id"], name: "index_episodes_on_created_by_id"
    t.index ["show_id"], name: "index_episodes_on_show_id"
  end

  create_table "favorites", force: :cascade do |t|
    t.integer "user_id", null: false
    t.string "favoritable_type", null: false
    t.integer "favoritable_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["favoritable_type", "favoritable_id"], name: "index_favorites_on_favoritable"
    t.index ["user_id"], name: "index_favorites_on_user_id"
  end

  create_table "hostings", force: :cascade do |t|
    t.integer "person_id", null: false
    t.integer "show_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["person_id"], name: "index_hostings_on_person_id"
    t.index ["show_id"], name: "index_hostings_on_show_id"
  end

  create_table "merit_actions", force: :cascade do |t|
    t.integer "user_id"
    t.string "action_method"
    t.integer "action_value"
    t.boolean "had_errors", default: false
    t.string "target_model"
    t.integer "target_id"
    t.text "target_data"
    t.boolean "processed", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["processed"], name: "index_merit_actions_on_processed"
  end

  create_table "merit_activity_logs", force: :cascade do |t|
    t.integer "action_id"
    t.string "related_change_type"
    t.integer "related_change_id"
    t.string "description"
    t.datetime "created_at", precision: nil
  end

  create_table "merit_score_points", force: :cascade do |t|
    t.integer "score_id"
    t.bigint "num_points", default: 0
    t.string "log"
    t.datetime "created_at", precision: nil
    t.index ["score_id"], name: "index_merit_score_points_on_score_id"
  end

  create_table "merit_scores", force: :cascade do |t|
    t.integer "sash_id"
    t.string "category", default: "default"
    t.index ["sash_id"], name: "index_merit_scores_on_sash_id"
  end

  create_table "notifications", force: :cascade do |t|
    t.integer "user_id", null: false
    t.string "title"
    t.text "content"
    t.boolean "read"
    t.string "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_notifications_on_user_id"
  end

  create_table "people", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.integer "created_by_id", null: false
    t.integer "blocked_by_id"
    t.integer "confirmed_by_id"
    t.datetime "confirmed_at", precision: nil
    t.datetime "blocked_at", precision: nil
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "facebook"
    t.string "twitter"
    t.string "parler"
    t.string "website"
    t.boolean "generated", default: false
    t.text "description"
    t.string "wikipedia"
    t.index ["blocked_by_id"], name: "index_people_on_blocked_by_id"
    t.index ["confirmed_by_id"], name: "index_people_on_confirmed_by_id"
    t.index ["created_by_id"], name: "index_people_on_created_by_id"
  end

  create_table "pseudonyms", force: :cascade do |t|
    t.string "name"
    t.integer "person_id", null: false
    t.index ["person_id"], name: "index_pseudonyms_on_person_id"
  end

  create_table "ratings", force: :cascade do |t|
    t.integer "user_id", null: false
    t.string "rateable_type", null: false
    t.integer "rateable_id", null: false
    t.integer "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["rateable_type", "rateable_id"], name: "index_ratings_on_rateable"
    t.index ["user_id"], name: "index_ratings_on_user_id"
  end

  create_table "reports", force: :cascade do |t|
    t.text "comment"
    t.integer "user_id", null: false
    t.string "category"
    t.string "reportable_type", null: false
    t.integer "reportable_id", null: false
    t.boolean "read", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["reportable_type", "reportable_id"], name: "index_reports_on_reportable"
    t.index ["user_id"], name: "index_reports_on_user_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_roles_on_name", unique: true
  end

  create_table "roles_users", id: false, force: :cascade do |t|
    t.integer "role_id", null: false
    t.integer "user_id", null: false
    t.index ["role_id", "user_id"], name: "index_roles_users_on_role_id_and_user_id", unique: true
    t.index ["user_id", "role_id"], name: "index_roles_users_on_user_id_and_role_id"
  end

  create_table "sashes", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "shows", force: :cascade do |t|
    t.string "slug"
    t.string "title"
    t.string "youtube"
    t.string "twitter"
    t.string "facebook"
    t.string "rumble"
    t.string "parler"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "website"
    t.string "spotify"
    t.string "apple_podcast"
    t.integer "created_by_id"
    t.integer "confirmed_by_id"
    t.datetime "confirmed_at", precision: nil
    t.integer "blocked_by_id"
    t.datetime "blocked_at", precision: nil
    t.string "wikipedia"
    t.index ["blocked_by_id"], name: "index_shows_on_blocked_by_id"
    t.index ["confirmed_by_id"], name: "index_shows_on_confirmed_by_id"
    t.index ["created_by_id"], name: "index_shows_on_created_by_id"
  end

  create_table "slugs", force: :cascade do |t|
    t.string "name"
    t.datetime "last_hit_at", precision: nil
    t.string "slugable_type", null: false
    t.integer "slugable_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["slugable_type", "slugable_id"], name: "index_slugs_on_slugable"
  end

  create_table "sources", force: :cascade do |t|
    t.integer "show_id", null: false
    t.string "foreign_id"
    t.string "kind"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "guests_pattern", default: "w/(?<names>.*)"
    t.string "split_pattern", default: " and |&|,"
    t.boolean "parse_title_for_guests", default: true
    t.boolean "scheduled"
    t.string "filter_pattern"
    t.string "title_pattern"
    t.string "youtube_kind", default: "channel"
    t.index ["show_id"], name: "index_sources_on_show_id"
  end

  create_table "topic_topicables", force: :cascade do |t|
    t.integer "topic_id"
    t.string "topicable_type"
    t.integer "topicable_id"
    t.index ["topic_id"], name: "index_topic_topicables_on_topic_id"
    t.index ["topicable_type", "topicable_id"], name: "index_topic_topicables_on_topicable"
  end

  create_table "topics", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at", precision: nil
    t.datetime "remember_created_at", precision: nil
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at", precision: nil
    t.datetime "last_sign_in_at", precision: nil
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at", precision: nil
    t.datetime "confirmation_sent_at", precision: nil
    t.string "unconfirmed_email"
    t.string "username"
    t.string "fullname"
    t.string "time_zone", default: "UTC"
    t.string "country"
    t.string "twitter"
    t.string "facebook"
    t.string "parler"
    t.string "youtube"
    t.string "rumble"
    t.string "linkedin"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "no_password", default: false
    t.string "locale"
    t.boolean "email_notifications", default: true
    t.integer "sash_id"
    t.integer "level", default: 0
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "votes", force: :cascade do |t|
    t.integer "value"
    t.integer "person_id", null: false
    t.integer "show_id", null: false
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "fulfilled", default: false, null: false
    t.string "ip_address"
    t.index ["person_id"], name: "index_votes_on_person_id"
    t.index ["show_id"], name: "index_votes_on_show_id"
    t.index ["user_id"], name: "index_votes_on_user_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "appearances", "episodes"
  add_foreign_key "appearances", "people"
  add_foreign_key "books", "users", column: "blocked_by_id"
  add_foreign_key "books", "users", column: "confirmed_by_id"
  add_foreign_key "books", "users", column: "created_by_id"
  add_foreign_key "edits", "edits", column: "previous_id"
  add_foreign_key "edits", "users", column: "applied_by_id"
  add_foreign_key "edits", "users", column: "created_by_id"
  add_foreign_key "edits", "users", column: "discarded_by_id"
  add_foreign_key "episodes", "shows"
  add_foreign_key "episodes", "users", column: "blocked_by_id"
  add_foreign_key "episodes", "users", column: "confirmed_by_id"
  add_foreign_key "episodes", "users", column: "created_by_id"
  add_foreign_key "favorites", "users"
  add_foreign_key "hostings", "people"
  add_foreign_key "hostings", "shows"
  add_foreign_key "notifications", "users"
  add_foreign_key "people", "users", column: "blocked_by_id"
  add_foreign_key "people", "users", column: "confirmed_by_id"
  add_foreign_key "people", "users", column: "created_by_id"
  add_foreign_key "pseudonyms", "people"
  add_foreign_key "ratings", "users"
  add_foreign_key "reports", "users"
  add_foreign_key "shows", "users", column: "blocked_by_id"
  add_foreign_key "shows", "users", column: "confirmed_by_id"
  add_foreign_key "shows", "users", column: "created_by_id"
  add_foreign_key "sources", "shows"
  add_foreign_key "votes", "people"
  add_foreign_key "votes", "shows"
  add_foreign_key "votes", "users"
end
