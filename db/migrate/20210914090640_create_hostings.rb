class CreateHostings < ActiveRecord::Migration[6.1]
  def change
    create_table :hostings do |t|
      t.references :person, null: false, foreign_key: true
      t.references :show, null: false, foreign_key: true

      t.timestamps
    end
  end
end
