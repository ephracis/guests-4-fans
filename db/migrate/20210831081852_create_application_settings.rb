class CreateApplicationSettings < ActiveRecord::Migration[6.1]
  def change
    create_table :application_settings do |t|
      t.integer :max_votes_per_show, default: 5
    end
  end
end
