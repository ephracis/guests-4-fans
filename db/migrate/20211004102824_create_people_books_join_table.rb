class CreatePeopleBooksJoinTable < ActiveRecord::Migration[6.1]
  def change
    create_join_table :people, :books
  end
end
