class CreatePeople < ActiveRecord::Migration[6.1]
  def change
    create_table :people do |t|
      t.string :name
      t.string :slug
      t.string :image
      t.belongs_to :created_by, null: false, foreign_key: { to_table: :users }
      t.belongs_to :blocked_by, null: true, foreign_key: { to_table: :users }
      t.belongs_to :confirmed_by, null: true, foreign_key: { to_table: :users }
      t.datetime :confirmed_at, null: true
      t.datetime :blocked_at, null: true

      t.timestamps
    end
  end
end
