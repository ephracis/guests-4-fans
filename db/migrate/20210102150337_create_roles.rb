class CreateRoles < ActiveRecord::Migration[6.1]
  def change
    create_table :roles do |t|
      t.string :name
      t.string :title

      t.timestamps
    end
    add_index :roles, :name, unique: true
  end
end
