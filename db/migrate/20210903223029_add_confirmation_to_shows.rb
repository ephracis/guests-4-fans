class AddConfirmationToShows < ActiveRecord::Migration[6.1]
  def change
    add_reference :shows, :created_by, null: true, foreign_key: { to_table: :users }
    add_reference :shows, :confirmed_by, null: true, foreign_key: { to_table: :users }
    add_column :shows, :confirmed_at, :datetime
    add_reference :shows, :blocked_by, null: true, foreign_key: { to_table: :users }
    add_column :shows, :blocked_at, :datetime

    add_reference :episodes, :created_by, null: true, foreign_key: { to_table: :users }
    add_reference :episodes, :confirmed_by, null: true, foreign_key: { to_table: :users }
    add_column :episodes, :confirmed_at, :datetime
    add_reference :episodes, :blocked_by, null: true, foreign_key: { to_table: :users }
    add_column :episodes, :blocked_at, :datetime
  end
end
