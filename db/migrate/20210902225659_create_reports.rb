class CreateReports < ActiveRecord::Migration[6.1]
  def change
    create_table :reports do |t|
      t.text :comment
      t.references :user, null: false, foreign_key: true
      t.string :category
      t.references :reportable, polymorphic: true, null: false
      t.boolean :read, default: false

      t.timestamps
    end
  end
end
