class CreateBooks < ActiveRecord::Migration[6.1]
  def change
    create_table :books do |t|
      t.string :name
      t.string :slug
      t.text :description
      t.belongs_to :created_by, null: false, foreign_key: { to_table: :users }
      t.timestamp :confirmed_at
      t.belongs_to :confirmed_by, null: true, foreign_key: { to_table: :users }
      t.timestamp :blocked_at
      t.belongs_to :blocked_by, null: true, foreign_key: { to_table: :users }
      t.timestamp :published_at
      t.string :amazon

      t.timestamps
    end
  end
end
