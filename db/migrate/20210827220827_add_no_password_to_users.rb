class AddNoPasswordToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :no_password, :boolean, default: false
  end
end
