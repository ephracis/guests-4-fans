class ChangeDefaultSplitPattern < ActiveRecord::Migration[6.1]
  def change
    change_column_default :sources, :split_pattern, ' and |&|,'
  end
end
