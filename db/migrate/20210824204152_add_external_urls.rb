class AddExternalUrls < ActiveRecord::Migration[6.1]
  def change
    add_column :shows, :website, :string
    add_column :shows, :spotify, :string
    add_column :shows, :apple_podcast, :string

    add_column :people, :facebook, :string
    add_column :people, :twitter, :string
    add_column :people, :parler, :string
    add_column :people, :website, :string

    add_column :episodes, :spotify, :string
    add_column :episodes, :apple_podcast, :string
    add_column :episodes, :website, :string
  end
end
