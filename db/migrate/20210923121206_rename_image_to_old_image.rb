class RenameImageToOldImage < ActiveRecord::Migration[6.1]
  def change
    rename_column :people, :image, :old_image
    rename_column :shows, :image, :old_image
    rename_column :episodes, :image, :old_image
  end
end
