class CreateRoleUsers < ActiveRecord::Migration[6.1]
  def change
    create_join_table :roles, :users do |t|
      t.index %i[role_id user_id], unique: true
      t.index %i[user_id role_id]
    end
  end
end
