class CreateSlugs < ActiveRecord::Migration[6.1]
  def change
    create_table :slugs do |t|
      t.string :name
      t.timestamp :last_hit_at
      t.references :slugable, polymorphic: true, null: false
      t.timestamps
    end
  end
end
