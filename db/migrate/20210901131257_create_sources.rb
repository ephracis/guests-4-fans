class CreateSources < ActiveRecord::Migration[6.1]
  def change
    create_table :sources do |t|
      t.references :show, null: false, foreign_key: true
      t.string :foreign_id
      t.string :kind

      t.timestamps
    end
  end
end
