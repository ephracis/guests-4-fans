class CreateEdits < ActiveRecord::Migration[6.1]
  def change
    create_table :edits do |t|
      t.string :name
      t.string :value
      t.belongs_to :created_by, null: false, foreign_key: { to_table: :users }
      t.references :editable, polymorphic: true, null: false
      t.boolean :discarded, default: false
      t.timestamp :discarded_at
      t.belongs_to :discarded_by, null: true, foreign_key: { to_table: :users }
      t.boolean :applied, default: false
      t.timestamp :applied_at
      t.belongs_to :applied_by, null: true, foreign_key: { to_table: :users }
      t.belongs_to :previous, null: true, foreign_key: { to_table: :edits }

      t.timestamps
    end
  end
end
