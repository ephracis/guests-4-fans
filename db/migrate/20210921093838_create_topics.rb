class CreateTopics < ActiveRecord::Migration[6.1]
  def change
    create_table :topics do |t|
      t.string :name
      t.string :slug

      t.timestamps
    end

    create_table :topic_topicables do |t|
      t.references :topic, index: true
      t.references :topicable, polymorphic: true, index: true
    end
  end
end
