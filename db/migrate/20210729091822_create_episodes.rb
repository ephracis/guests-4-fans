class CreateEpisodes < ActiveRecord::Migration[6.1]
  def change
    create_table :episodes do |t|
      t.string :name
      t.string :title
      t.string :youtube
      t.string :rumble
      t.string :description
      t.string :image
      t.references :show, null: false, foreign_key: true
      t.integer :length
      t.timestamp :aired_at

      t.timestamps
    end
  end
end
