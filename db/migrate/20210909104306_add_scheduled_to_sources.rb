class AddScheduledToSources < ActiveRecord::Migration[6.1]
  def change
    add_column :sources, :scheduled, :boolean, default: false
  end
end
