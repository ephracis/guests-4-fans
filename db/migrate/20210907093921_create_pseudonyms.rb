class CreatePseudonyms < ActiveRecord::Migration[6.1]
  def change
    create_table :pseudonyms do |t|
      t.string :name
      t.belongs_to :person, null: false, foreign_key: true
    end
  end
end
