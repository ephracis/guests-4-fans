class AddWikipediaToPeople < ActiveRecord::Migration[6.1]
  def change
    add_column :people, :wikipedia, :string
    add_column :shows, :wikipedia, :string
  end
end
