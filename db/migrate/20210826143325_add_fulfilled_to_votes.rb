class AddFulfilledToVotes < ActiveRecord::Migration[6.1]
  def change
    add_column :votes, :fulfilled, :boolean, default: false, null: false
  end
end
