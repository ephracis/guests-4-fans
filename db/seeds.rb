# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

[
  { name: 'root', title: 'Root' },
  { name: 'admin', title: 'Administrator' },
  { name: 'limited', title: 'Limited' },
].each do |role|
  x = Role.find_or_create_by(name: role[:name])
  x.update(role)
  x.save
end

ApplicationSettings.create! unless ApplicationSettings.any?
