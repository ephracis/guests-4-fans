require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module ShocaDB
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    config.generators do |g|
      g.orm             :active_record
      g.test_framework  :test_unit, fixture: false
      g.stylesheets     false
    end

    # Custom error pages
    config.exceptions_app = self.routes

    # Fallback locale when translated string is missing
    config.i18n.fallbacks = [:en]
    
    # Remove 7.0 deprecation warnings
    config.active_record.legacy_connection_handling = false
    config.action_controller.urlsafe_csrf_tokens = true
  end
end
