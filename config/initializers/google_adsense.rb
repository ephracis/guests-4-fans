# frozen_string_literal: true
module GoogleAdsense
  CLIENT_ID = Rails.application.credentials.dig :google_adsense, Rails.env.to_sym, :client_id
  JS_URL = "https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=#{GoogleAdsense::CLIENT_ID}"
end
