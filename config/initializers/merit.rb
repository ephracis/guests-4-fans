# Use this hook to configure merit parameters
Merit.setup do |config|
  # Check rules on each request or in background
  # config.checks_on_each_request = true

  # Add application observers to get notifications when reputation changes.
  # config.add_observer 'MyObserverClassName'

  # Define :user_model_name. This model will be used to grant badge if no
  # `:to` option is given. Default is 'User'.
  # config.user_model_name = 'User'

  # Define :current_user_method. Similar to previous option. It will be used
  # to retrieve :user_model_name object if no `:to` option is given. Default
  # is "current_#{user_model_name.downcase}".
  # config.current_user_method = 'current_user'
end

# Create application badges (uses https://github.com/norman/ambry)
Rails.application.reloader.to_prepare do
  badges = []

  [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].each do |level|
    badges << { name: 'voter', level: level }
    badges << { name: 'person-creator', level: level }
    badges << { name: 'episode-creator', level: level }
    badges << { name: 'show-creator', level: level }
    badges << { name: 'book-creator', level: level }
    badges << { name: 'editor', level: level }
  end

  badges << { name: 'registrator' }
  badges << { name: 'user-fullname' }
  badges << { name: 'user-social' }
  badges << { name: 'user-nation' }
  badges << { name: 'user-email' }
  badges << { name: 'omniauth' }

  badges.each_with_index do |attrs, index|
    attrs[:id] = index + 1
    Merit::Badge.create!(attrs)

    # begin
    #   badge = Merit::Badge.find(attrs[:id])
    #   if badge.name.to_s != attrs[:name].to_s
    #     raise "Badge mismatch: #{badge.name} (#{badge.id}) != #{attrs[:name]} (#{attrs[:id]})"
    #   end
    # rescue Ambry::NotFoundError
    #   Merit::Badge.create!(attrs)
    # end
  end
end
