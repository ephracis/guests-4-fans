# frozen_string_literal: true

module ShocaDB
  class Application
    VERSION = '1.5.0'

    def version
      VERSION
    end
  end
end
