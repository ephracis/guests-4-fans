const { environment } = require('@rails/webpacker')
const { VueLoaderPlugin } = require('vue-loader')
const customConfig = require('./custom')
const vue = require('./loaders/vue')
const pug = require('./loaders/pug')
const webpack = require('webpack')

environment.plugins.append(
  'Provide',
  new webpack.ProvidePlugin({
    $: 'jquery/src/jquery',
    jQuery: 'jquery/src/jquery',
    Popper: ['popper.js', 'default']
  })
)

environment.config.merge(customConfig)
environment.plugins.prepend('VueLoaderPlugin', new VueLoaderPlugin())
environment.loaders.prepend('vue', vue)
environment.loaders.prepend('pug', pug)
module.exports = environment
