# Update dependencies

It is good practice to continuously keep all dependencies up to date. This can be done within policies (for example
`^2.1.0` would only upgrade patch version, never minor to `2.2.0` or major to `3.0.0`), or it could be done by
upgrading to the very latest version available.

Obviously the former is much easier than the latter, but if possible we should always strive for the latter.

## Within policies

To update Ruby gems:

```shell
bundle update
```

To update Node packages:

```shell
yarn upgrade
```

## To the latest available

Node packages can be automatically bumped using `npm-check-update`:

```shell
# ensure you have ncu installed
npm install -g npm-check-updates

# bump packages in package.json
ncu -u

# then install the new packages and generate a new lock file
yarn install
```

For Ruby we currently do not have any policies in `Gemfile` that needs to be edited so these are always upgraded to
latest version when running `bundle update`. If this causes a problem in the future, the method would be to manually
edit the policy and then rerun `bundle update`.
