# Deployment

Deployment is usually done automatically as part of the [release process](Release.md) but
here follows the steps necessary for deploying manually, for example if you have created
a fork of this project and want to make your own deployment.

## Set up credentials

When deploying to production you must have an proper credentials file and the corresponding key to that file. If you
do not have access to our encryption key, start by creating your own credentials file:

```shell
rm config/credentials.yml.enc
bin/rails credentials:edit
```

Ensure that the file looks like this:

```yaml
secret_key_base: xxx

recaptcha:
  site_key: xxx
  secret_key: xxx

sendgrid:
  api_key: xxx

google:
  api_id: xxx
  api_secret: xxx
  app_key: xxx

spotify:
  client_id: xxx
  client_secret: xxx

google_analytics:
  development:
    tracking_id: G-XXXXXXXXXX
  production:
    tracking_id: G-XXXXXXXXXX

google_adsense:
  development:
    client_id: ca-pub-XXXXXXXXXXXXXXXX
  production:
    client_id: ca-pub-XXXXXXXXXXXXXXXX

google_cloud:
  project_id: shocadb
  sitemap:
    bucket: sitemap.example.com
  activestorage:
    bucket: images.example.com
    private_key_id: XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    private_key: |
      -----BEGIN PRIVATE KEY-----
      XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      ...
      XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
      -----END PRIVATE KEY-----
    client_email: XXX@XXX.iam.gserviceaccount.com
    client_id: 000000000000000000000
    client_x509_cert_url: https://www.googleapis.com/robot/v1/metadata/x509/XXX%40XXX.iam.gserviceaccount.com

newrelic:
  license_key: xxx

sentry:
  dsn: https://XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX@XXXXXXXX.ingest.sentry.io/XXXXXXX
```

## Deploy to Heroku

Install [Heroku CLI](https://devcenter.heroku.com/articles/heroku-command-line). Then clone or download the repository
and run the following:

```shell
heroku login
heroku config:set --app=<app-name> RAILS_MASTER_KEY=`cat config/master.key`
heroku git:remote --app=<app-name>
git push heroku master
heroku run --app=<app-name> --exit-code -- rails db:migrate
```
