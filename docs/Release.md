# Release management

This document outlines the steps needed to make a new release.

## Create a release commit

### Branch to use

If updating a major or minor release (i.e. going from `1.3.0` to `1.4.0`, or from `1.4.0` to `2.0.0`), a commit is
added to the `master` branch.

If the release is a patch release, the commit is instead added to the release branch for the major release.

For example, going from `1.3.0` to `1.3.1`, the commit is added to the branch `1-3-stable`.
Going from from `1.4.2` to `1.4.3`, the commit is added to the branch `1-4-stable`.

### Bump version in app

Update the file `config/initializers/version.rb` with the new version.

For example, when going from `1.3.0` to `1.4.0`:

```diff
diff --git a/config/initializers/version.rb b/config/initializers/version.rb
index a701959..2657a44 100644
--- a/config/initializers/version.rb
+++ b/config/initializers/version.rb
@@ -2,7 +2,7 @@

 module ShocaDB
   class Application
-    VERSION = '1.3.0'
+    VERSION = '1.4.0'

     def version
       VERSION
```

### Update Changelog

The file `CHANGELOG.md` need to be updated with the new release. This is done in two places:

1. The title `Unreleased` is changed to the name of the release, and the date.
1. A link reference is added to the bottom for the diff of the release, comparing it to the previous release.

For example, when going from `1.3.0` to `1.4.0`:

```diff
diff --git a/CHANGELOG.md b/CHANGELOG.md
index 8f88348..90beb72 100644
--- a/CHANGELOG.md
+++ b/CHANGELOG.md
@@ -4,7 +4,7 @@ All notable changes to this project will be documented in this file.
 The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
 and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

-## [Unreleased]
+## [1.4.0] - 2021-10-13
 ### Added
 - Ability to add books a person has authored.
 - Ability to earn achievements by creating and editing resources.
@@ -247,7 +247,8 @@ and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0
 - Guests.
 - Episodes.

-[Unreleased]: https://gitlab.com/ephracis/shocadb/compare/1.3.0...HEAD
+[Unreleased]: https://gitlab.com/ephracis/shocadb/compare/1.4.0...HEAD
+[1.4.0]: https://gitlab.com/ephracis/shocadb/compare/1.3.0...1.4.0
 [1.3.0]: https://gitlab.com/ephracis/shocadb/compare/1.2.0...1.3.0
 [1.2.0]: https://gitlab.com/ephracis/shocadb/compare/1.1.0...1.2.0
 [1.1.0]: https://gitlab.com/ephracis/shocadb/compare/1.0.0...1.1.0
```

### Commit message

Just use the message `Bump to VERSION`.

For example, when going from `1.3.0` to `1.4.0`:

```shell
git commit -a -m 'Bump to 1.4.0'
```

Then push this commit to the relevant branch.

### Create release branch

If the release is a major or minor version (i.e. going from `1.3.0` to `1.4.0`, or from `1.4.0` to `2.0.0`),
create a new release branch for that version.

For example, when going from `1.3.0` to `1.4.0`:

```shell
git checkout -b 1-4-stable
```

Then push this branch.

## Create release in GitLab

After the release commit has been pushed to GitLab, create a new Release:

1. Go to `Deployments -> Releases`.
1. Click on `New release`.
1. Under `Tag name`, type the version of the release. For example `1.4.0`.
1. If the release is a patch release, select the correct release branch from `Create from`.
1. Under `Release title`, type the version of the release. For example `1.4.0`.
1. Under `Milestone`, select the milestone for the release. For example `1.4`.
1. Under `Release notes`, copy the sections for the release from `CHANGELOG.md`. For example:

    ```markdown
    ### Added
    - Ability to add books a person has authored.
    - Ability to earn achievements by creating and editing resources.
    - Clicking on a topic shows a page with all resources for that topic.
    - Episode description is shown when viewing a description.
    - Episode lists contain information about the show and when the episode aired.
    - Improved formatting of episode descriptions.
    - Indicator showing if an episode will be merged when generating from an external source.
    - Links to Wikipedia for people and shows.
    - Redesign of all lists.
    - The page for viewing a person has been redesigned and split into separate pages.
    - When viewing a person for a specific show, if the person has appeared on the show, embedded media is shown for that episode.

    ### Fixed
    - Incorrect badge in admin area for imageless resources.
    - List of guests not yet on show contained previous guests.
    - Nothing happened when trying to update a resource with an image, without changing the image.
    - Only a maximum of ten episodes were listed when viewing a show.
    ```
1. Click on `Create release`.

## Verify the release

Wait for the pipelines in GitLab to finish successfully. Then go to [the Admin dashboard](https://shocadb.com/admin/dashboard) and verify that the app is running the new release.
