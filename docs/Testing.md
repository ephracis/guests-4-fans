# Testing

## Automatic testing

```shell
# test the backend
bin/rails test

# test the frontend
yarn test
```

To run the test against Postgres instead of the default SQLite:

```shell
podman-compose up -d
export TEST_DATABASE_URL=$(hostname)
bin/rails test
```

To run system tests which will run on a headless Chrome browser:

```shell
bin/rails test:system
```

## Manual testing

You can spin up the web server on your local machine. It is recommended to first
populate the database with some fake data:

```shell
bin/rails faker
```

Then start the web server:

```shell
bundle exec foreman start -f Procfile.dev
```

The web app is now available on http://localhost:5000.
You may sign in using the username `admin@mail.com` and password `changeme`.
