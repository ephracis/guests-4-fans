# frozen_string_literal: true

module TopicsHelper
  def topics_for(resource)
    content_tag(:ul, class: 'list-inline topic-list') do
      resource.topics.each do |topic|
        item = content_tag(:li, class: 'list-inline-item topic-list-item') do
          link_to topic.name, topic, class: 'badge badge-pill badge-primary topic-badge'
        end
        concat item
      end
    end
  end
end
