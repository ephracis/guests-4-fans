# frozen_string_literal: true

module PersonHelper
  def person_image(person)
    if person.image.attached?
      url_for(person.image)
    else
      image_url('placeholder-person.jpg')
    end
  end
end
