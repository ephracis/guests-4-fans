# frozen_string_literal: true

module ShowHelper
  def show_meta_description(show)
    "Check out the latest guests for #{show.title} and vote for your favorite guests to appear or return to the show."
  end

  def show_meta_keywords(show)
    [
      'podcast',
      'show',
      'guests',
      show.title
    ].join(',')
  end

  def show_image(show)
    if show.image.attached?
      url_for(show.image)
    else
      image_url('placeholder-show.svg')
    end
  end
end
