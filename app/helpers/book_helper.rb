# frozen_string_literal: true

module BookHelper
  def book_image(book)
    if book.image.attached?
      url_for(book.image)
    else
      image_url('placeholder-book.png')
    end
  end
end
