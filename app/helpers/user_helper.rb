# frozen_string_literal: true

module UserHelper
  # Generate a Gravatar image for a given user.
  def user_gravatar(user, size = 30, options = {})
    options = {
      alt: user.fullname,
      class: 'rounded-circle border',
      width: size,
      height: size,
      loading: :eager
    }.merge(options)

    gravatar_id = Digest::MD5.hexdigest(user.email.downcase)
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?d=identicon"

    image_tag(gravatar_url, options)
  end

  def map_locale_names
    I18n.available_locales.map do |locale|
      [LANGUAGES[locale], locale]
    end
  end

  def locale_select_options(default)
    options_for_select([[t('user.locale_automatic'), '']] + map_locale_names, default)
  end
end
