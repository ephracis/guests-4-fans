# frozen_string_literal: true

module ApplicationHelper
  def title(text)
    content_for :title, text
  end

  def meta_tag(tag, text)
    content_for :"meta_#{tag}", text
  end

  def active_tab(tab_name)
    @active_tab = tab_name
  end

  def active_if(tab_name)
    @active_tab == tab_name ? 'active' : ''
  end

  def yield_meta_tag(tag, default_text = '')
    content_for?(:"meta_#{tag}") ? content_for(:"meta_#{tag}") : default_text
  end

  def default_meta_description
    'Follow your favorite guests and vote for them to appear on your top podcast shows.'
  end

  def default_meta_keywords
    %w[
      podcast
      show
      guests
    ].join(',')
  end

  def unconfirmed_count
    @unconfirmed_count || 0
  end

  def link_to_cancel(resource, path_when_new = nil, path_when_edit = nil)
    if @cancel_path.present?
      link_to t('form.cancel'), @cancel_path
    elsif resource.persisted?
      link_to t('form.cancel'), path_when_edit || url_for(resource)
    else
      link_to t('form.cancel'), path_when_new || send("#{resource.class.table_name}_path")
    end
  end

  # Generate a link in the navbar
  #
  # The link will be marked as active based on the `page_matcher` which
  # can be a hash describing controller and action, or an argument sent
  # to `current_page?`.
  def navbar_link(title, path, page_matcher = nil, &block)
    opts = { class: 'nav-link d-flex flex-row' }
    page_matcher ||= path
    opts = { class: 'nav-link d-flex flex-row active' } if matches_page?(page_matcher)

    if block_given?
      link_to path, opts, &block
    else
      link_to title, path, opts
    end
  end

  # Display date and time in the current user's configured time zone.
  def display_time(time, format = :default)
    return unless time

    time = time.in_time_zone(current_user.time_zone || 'UTC') if current_user && time.is_a?(DateTime)
    l(time, format: format)
  end

  # Display date in the current user's configured time zone.
  def display_date(date, format = :default)
    display_time date.to_date, format if date
  end

  def link_to_share(service, share_url, share_text)
    url = CGI.escape(share_url)
    text = CGI.escape(share_text)
    case service.to_sym
    when :facebook
      "https://facebook.com/sharer.php?u=#{url}&p[title]=#{text}"
    when :twitter
      "https://twitter.com/share?text=#{text}&url=#{url}"
    when :pinterest
      "https://pinterest.com/pin/create/button/?url=#{url}&description=#{text}"
    else
      raise ArgumentError, "Service #{service} is not supported"
    end
  end

  private

  def matches_page?(page_matcher)
    return current_page?(page_matcher) unless page_matcher.is_a? Hash

    if page_matcher[:controller] && page_matcher[:action]
      current_page?(page_matcher)
    elsif page_matcher[:controller]
      params[:controller].to_s == page_matcher[:controller].to_s
    else
      params[:action].to_s == page_matcher[:action].to_s
    end
  rescue StandardError
    false
  end
end
