# frozen_string_literal: true

class AliasPolicy < ApplicationPolicy
  def create?
    return false unless user

    return false if user.roles?(:limited, :blocked)

    record.aliasable.created_by == user || user.roles?(:admin, :root)
  end

  def update?
    return false unless user

    return false if user.roles?(:limited, :blocked)

    record.aliasable.created_by == user || user.roles?(:admin, :root)
  end

  def destroy?
    return false unless user

    return false if user.roles?(:limited, :blocked)

    record.aliasable.created_by == user || user.roles?(:admin, :root)
  end
end
