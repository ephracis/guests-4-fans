# frozen_string_literal: true

class BookPolicy < ApplicationPolicy
  def create?
    user && !user.roles?(:limited, :blocked)
  end

  def edit?
    return false unless user

    record.created_by == user || user.roles?(:admin, :root)
  end

  def update?
    return false unless user

    record.created_by == user || user.roles?(:admin, :root)
  end

  def destroy?
    return false unless user

    user.roles?(:admin, :root)
  end

  def confirm?
    return false unless user

    user.roles?(:admin, :root)
  end

  def block?
    return false unless user

    user.roles?(:admin, :root)
  end
end
