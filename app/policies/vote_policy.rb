# frozen_string_literal: true

class VotePolicy < ApplicationPolicy
  def create?
    true
  end

  def update?
    return false unless user

    @record.user == user || user.roles?(:admin, :root)
  end

  def destroy?
    return false unless user

    @record.user == user || user.roles?(:admin, :root)
  end
end
