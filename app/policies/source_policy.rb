# frozen_string_literal: true

class SourcePolicy < ApplicationPolicy
  def create?
    return false unless user

    user.roles?(:admin, :root)
  end

  def update?
    return false unless user

    user.roles?(:admin, :root)
  end

  def destroy?
    return false unless user

    user.roles?(:admin, :root)
  end

  def fetch?
    return false unless user

    user.roles?(:admin, :root)
  end

  def generate?
    return false unless user

    user.roles?(:admin, :root)
  end
end
