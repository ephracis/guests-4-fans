# frozen_string_literal: true

class ApplicationSettingsPolicy < ApplicationPolicy
  def create?
    user&.role?(:root)
  end

  def update?
    user&.role?(:root)
  end

  def destroy?
    false
  end
end
