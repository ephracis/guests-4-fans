# frozen_string_literal: true

class UserPolicy < ApplicationPolicy
  def update?
    return false unless user

    user == record or user.role?(:root)
  end

  def destroy?
    return false unless user

    user == record or user.role?(:root)
  end
end
