# frozen_string_literal: true

class Vote < ApplicationRecord
  after_create :delete_opposite

  belongs_to :show
  belongs_to :person
  belongs_to :user, optional: true

  scope :fulfilled, -> { where(fulfilled: true) }
  scope :unfulfilled, -> { where(fulfilled: false) }

  validates :value, numericality: { only_integer: true }
  validates :user_id, presence: true, unless: :ip_address
  validates :ip_address, uniqueness: { scope: :fulfilled }, unless: :user_id

  def delete_opposite
    vote = Vote.find_by(user: user, show: show, person: person, value: value * -1)
    vote&.delete
  end
end
