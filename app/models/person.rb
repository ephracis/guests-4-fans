# frozen_string_literal: true

class Person < ApplicationRecord
  include Searchable
  include Confirmable
  include Blockable
  include Slugable
  include Topicable
  include Imageable
  include Rateable
  include Editable
  include ExternalLinkable
  include Duplicatable
  include Aliasable

  has_many :votes, dependent: :destroy
  has_many :favorites, as: :favoritable
  has_many :appearances, dependent: :destroy
  has_many :hostings, dependent: :destroy
  has_many :episodes, through: :appearances
  has_many :shows, -> { distinct }, through: :episodes
  has_many :hosted_shows, through: :hostings, source: :show
  has_many :pseudonyms, dependent: :destroy
  has_many :unfulfilled_votes, -> { unfulfilled }, class_name: 'Vote'
  has_many :fulfilled_votes, -> { fulfilled }, class_name: 'Vote'
  has_and_belongs_to_many :books

  validates :slug, presence: true,
                   uniqueness: { case_sensitive: false },
                   length: { minimum: 2 },
                   format: { with: /\A[a-z0-9_-]+\z/,
                             message: 'can only contain lowercase letters, numbers, underscore and dash' }
  validates :name, presence: true,
                   uniqueness: { case_sensitive: false },
                   length: { minimum: 2 }

  attr_accessor :vote_data

  def favorited_by?(user)
    favorites.where(user_id: user).any?
  end

  def to_s
    name
  end

  def vote_count
    attributes['vote_count'] || votes.size
  end

  def any_votes_for?(opts = {})
    if opts[:show] && opts[:user]
      any_votes_for_show_and_user?(opts[:show], opts[:user])
    elsif opts[:show]
      any_votes_for_show?(opts[:show])
    elsif opts[:user]
      any_votes_for_user?(opts[:user])
    else
      votes.size.positive?
    end
  end

  def any_votes_for_show_and_user?(show, user)
    if user.is_a? User
      votes.map { |v| v.show_id == show.id && v.user_id == user.id }.any?
    else
      votes.map { |v| v.show_id == show.id && v.ip_address == user }.any?
    end
  end

  def any_votes_for_user?(user)
    if user.is_a? User
      votes.map { |v| v.user_id == user.id }.any?
    else
      votes.map { |v| v.ip_address == user }.any?
    end
  end

  def any_votes_for_show?(show)
    votes.map { |v| v.show_id == show.id }.any?
  end

  def self.top(show = nil)
    if show
      left_joins(:votes)
        .group(:id)
        .where('votes.show_id IS NULL OR votes.show_id = ?', show.id)
        .select('people.*, COUNT(nullif(votes.fulfilled, true)) as vote_count')
        .order('vote_count DESC')
    else
      left_joins(:votes)
        .group(:id)
        .select('people.*, COUNT(nullif(votes.fulfilled, true)) as vote_count')
        .order('vote_count DESC')
    end
  end

  def self.order_by_votes(show = nil)
    count = if show
              "case when votes.show_id = #{show.id} AND NOT votes.fulfilled then 1 else null end"
            else
              'nullif(votes.fulfilled, true)'
            end
    left_joins(:votes).group(:id)
                      .uniq!(:group)
                      .select("people.*, COUNT(#{count}) as vote_count")
                      .order(Arel.sql("COUNT(#{count}) DESC"))
  end

  private

  def copy_associations_to(original)
    [votes, appearances, hostings].each do |collection|
      copy_association collection, :person, original
    end

    copy_association slugs, :slugable_id, original.id
    copy_association aliases, :aliasable_id, original.id
    copy_association favorites, :favoritable_id, original.id
    copy_association topic_topicables, :topicable_id, original.id
    copy_association ratings, :rateable_id, original.id
    copy_association edits, :editable_id, original.id

    books.each { |book| book.authors << orignal unless original.in?(book.authors) }

    original.image.attach image.blob unless original.image.attached?
  end
end
