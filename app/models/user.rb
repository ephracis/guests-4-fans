# frozen_string_literal: true

class User < ApplicationRecord
  has_merit

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :trackable, :confirmable, :omniauthable, omniauth_providers: %i[google_oauth2]

  has_many :notifications, dependent: :destroy
  has_many :votes, dependent: :destroy
  has_many :favorites, dependent: :destroy
  has_many :shows_voted_on, -> { distinct }, through: :votes, source: :show
  has_many :people_voted_on, -> { distinct }, through: :votes, source: :person
  has_many :favorite_people, through: :favorites, source: :favoritable, source_type: 'Person'
  has_many :unfulfilled_votes, -> { unfulfilled }, class_name: 'Vote'
  has_many :fulfilled_votes, -> { fulfilled }, class_name: 'Vote'
  has_and_belongs_to_many :roles

  has_many :created_shows, class_name: 'Show', foreign_key: :created_by
  has_many :created_episodes, class_name: 'Episode', foreign_key: :created_by
  has_many :created_people, class_name: 'Person', foreign_key: :created_by
  has_many :created_books, class_name: 'Book', foreign_key: :created_by
  has_many :edits, foreign_key: :created_by

  validates :time_zone, presence: true, time_zone: true
  validates :username, length: { in: 2..50 },
                       uniqueness: { case_sensitive: false },
                       format: { with: /\A[a-zA-Z0-9_.]*\z/ }
  validates :fullname, length: { maximum: 50 }

  attr_writer :login

  # Virtual attribute to allow users to sign in using either email or username.
  def login
    @login || username || email
  end

  def to_s
    fullname? ? fullname : username
  end

  # Check if the user has a given role.
  def role?(role_name)
    @roles ||= roles.pluck(:name)
    role_name.to_s.in? @roles
  end

  def roles?(*role_names)
    @roles ||= roles.pluck(:name)
    role_names.map { |r| r.to_s.in? @roles }.any?
  end

  # Get the number of votes that counts toward the show limit.
  def unfulfilled_votes_for(show)
    votes.where(show: show, fulfilled: false).size
  end

  # Override Devise's `find_for_database_authentication` so we can lookup users with the virtual login attribute.
  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup

    # if conditions contain the virtual login attribute...
    if (login = conditions.delete(:login))
      where(conditions.to_hash)
        .where(['lower(username) = :value OR lower(email) = :value', { value: login.downcase }])
        .first

    # ...or normal conditions
    elsif conditions.key?(:username) || conditions.key?(:email)
      conditions[:email]&.downcase!
      where(conditions.to_hash).first
    end
  end

  def self.from_omniauth(auth)
    where(email: auth.info.email).first_or_create do |user|
      user.password = Devise.friendly_token[0, 20]
      user.confirmed_at = DateTime.now
      user.no_password = true
      user.time_zone ||= 'UTC'
      user.fullname = auth.info.name
      user.username = user.email[/^[^@]*/]

      # if username is not unique, add random string
      user.username = "#{user.email[/^[^@]*/]}_#{rand(1000..1_000_000)}" while unscoped.find_by(username: user.username)
    end
  end

  def earned_badge?(badge_name)
    matches = badges.select { |b| b.name.to_s == badge_name.to_s }
    return false unless matches.present?
    return (matches[0].level || true) if matches.length == 1

    matches.map(&:level).max
  end

  def earned_badges
    badges.group_by { |b| b.name.itself }.map { |g| g[1].last }
  end
end
