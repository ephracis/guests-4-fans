# frozen_string_literal: true

class Favorite < ApplicationRecord
  belongs_to :user
  belongs_to :favoritable, polymorphic: true

  validates :favoritable_type, presence: true
  validates :favoritable_id, presence: true
  validates :user_id, presence: true,
                      uniqueness: { scope: %i[favoritable_type favoritable_id] }
end
