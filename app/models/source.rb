# frozen_string_literal: true

class Source < ApplicationRecord
  include SpotifyBackend
  include YoutubeBackend

  enum kinds: %i[spotify youtube]
  validates :kind, inclusion: { in: kinds.keys }
  validate :source_is_unique

  belongs_to :show

  # Validate that the source is the only one of its kind for the associated show.
  def source_is_unique
    errors.add(:kind, 'is already associated with the show') if show.sources.where(kind: kind).where.not(id: id).any?
  end

  # Fetch episodes from external service.
  def fetch
    send("fetch_from_#{kind}")
  end

  def default_foreign_id
    show[kind]
  end

  def foreign_id
    super || default_foreign_id
  end

  def find_guests_in_title(title)
    return [] unless parse_title_for_guests && title.present?

    guests = /#{guests_pattern}/i.match(title)
    return [] unless guests

    guests[:names].split(/#{split_pattern}/i).collect(&:strip).reject(&:empty?)
  end

  def clean_title(title)
    return title unless title_pattern.present?

    matches = /#{title_pattern}/i.match(title)
    return title unless matches && matches[:title].present?

    matches[:title].strip
  end

  # Make an HTTP GET request.
  def self.http_get(url, params, auth_header = nil)
    http_request(url, params, auth_header, :get)
  end

  # Make an HTTP POST request.
  def self.http_post(url, params, auth_header)
    http_request(url, params, auth_header, :post)
  end

  # Make an HTTP request.
  def self.http_request(url, params, auth_header = nil, method)
    uri = URI.parse(url)
    uri.query = URI.encode_www_form(params) if method == :get

    https = Net::HTTP.new(uri.host, uri.port)
    https.use_ssl = true

    request = if method == :post
                Net::HTTP::Post.new(uri.path)
              else
                Net::HTTP::Get.new(uri.to_s)
              end
    request['Authorization'] = auth_header if auth_header
    if method == :post
      request['Content-Type'] = 'application/x-www-form-urlencoded'
      request.set_form_data(params)
    end

    response = https.request(request)
    JSON.parse(response.body)
  end
end
