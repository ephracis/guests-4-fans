# frozen_string_literal: true

class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
  alias_attribute :display_name, :name

  def self.clean_name(name)
    name.to_s.squish.gsub('’', "'")
  end

  # TODO: We really should not need this, but link_to seems to fail otherwise.
  def empty?
    blank?
  end
end
