# frozen_string_literal: true

class Notification < ApplicationRecord
  belongs_to :user
  after_create :send_email

  def send_email
    NotificationMailer.with(notification: self).appearance.deliver_later if user.email_notifications
  end
end
