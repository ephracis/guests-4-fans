# frozen_string_literal: true

class Alias < ApplicationRecord
  belongs_to :aliasable, polymorphic: true

  validates :name, presence: true,
                   length: { minimum: 2 }
end
