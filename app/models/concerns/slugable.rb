# frozen_string_literal: true

module Slugable
  extend ActiveSupport::Concern

  class_methods do
    def slugged_field(field = nil)
      @slugged_field_name = field if field.present?
      @slugged_field_name || :name
    end
  end

  included do
    has_many :slugs, as: :slugable, dependent: :destroy

    before_update { |resource| Slug.create(name: slug_was, slugable: resource) if slug_changed? }

    def to_param
      slug.parameterize
    end

    def generate_slug
      return if valid?

      field = send(self.class.slugged_field)
      field ||= SecureRandom.urlsafe_base64(10)
      self.slug = field.parameterize

      i = 1
      while self.class.find_by(slug: slug)
        self.slug = "#{field.parameterize}#{i}"
        i += 1
      end

      Slug.where(name: slug, slugable_type: self.class.to_s).destroy_all
    end
  end
end
