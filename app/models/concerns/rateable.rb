# frozen_string_literal: true

module Rateable
  extend ActiveSupport::Concern

  included do
    has_many :ratings, as: :rateable

    scope :with_ratings, -> { left_joins(:ratings).group(:id).select('AVG(ratings.value) AS average_rating') }
    scope :order_by_ratings, -> { joins(:ratings).group(:id).order('AVG(ratings.value) DESC') }
  end
end
