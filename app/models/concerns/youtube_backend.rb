# frozen_string_literal: true

module YoutubeBackend
  extend ActiveSupport::Concern

  included do
    # Fetch episodes from YouTube service.
    def fetch_from_youtube
      channel_details = youtube_channel_details(foreign_id)
      uploads_playlist = channel_details['items'][0]['contentDetails']['relatedPlaylists']['uploads']
      playlist_items = youtube_playlist_items(uploads_playlist)
      {
        'source' => JSON.parse(to_json),
        'episodes' => playlist_items['items'].select { |e| filter_youtube_episode(e) }
                                             .map { |e| normalize_youtube_episode(e) }
      }
    end

    def normalize_youtube_episode(episode)
      title = clean_title(episode['snippet']['title'])
      {
        'description' => episode['snippet']['description'],
        'source_id' => episode['snippet']['resourceId']['videoId'],
        'image' => episode['snippet']['thumbnails']['medium']['url'],
        'title' => title,
        'aired_at' => episode['snippet']['publishedAt'],
        'guests' => find_guests_in_title(title)
      }
    end

    def filter_youtube_episode(episode)
      filter_pattern.blank? || episode['snippet']['title'].match(/#{filter_pattern}/i)
    end

    def youtube_channel_details(channel_id)
      opts = { part: 'contentDetails' }
      opts[:id] = channel_id if youtube_kind.to_sym == :channel
      opts[:forUsername] = channel_id if youtube_kind.to_sym == :user
      youtube_request 'channels', opts
    end

    def youtube_playlist_items(playlist_id)
      youtube_request 'playlistItems', { part: 'snippet', playlistId: playlist_id, maxResults: 50 }
    end

    def youtube_request(endpoint, queries)
      Source.http_get(
        "#{youtube_base_url}/#{endpoint}",
        queries.merge({ key: Source.youtube_api_key })
      )
    end

    def youtube_base_url
      'https://www.googleapis.com/youtube/v3'
    end
  end

  class_methods do
    # The API key used to authenticate API calls on YouTube.
    def youtube_api_key
      Rails.application.credentials.dig(:google, :app_key)
    end
  end
end
