# frozen_string_literal: true

module Aliasable
  extend ActiveSupport::Concern

  class_methods do
    def aliased_field(field = nil)
      @aliased_field_name = field if field.present?
      @aliased_field_name || :name
    end

    def find_by_alias(name)
      return unless name.present?

      result = where("LOWER(#{aliased_field}) = ?", name.downcase).first
      return result if result.present?

      result = Alias.where('LOWER(name) = ?', name.downcase).first
      return result.aliasable if result.present?

      nil
    end

    def find_by_alias!(name)
      result = find_by_alias(name)
      raise ActiveRecord::RecordNotFound.new(nil, self, :name) unless result

      result
    end
  end

  included do
    has_many :aliases, as: :aliasable, dependent: :destroy

    before_update do |resource|
      Alias.create(name: send("#{aliased_field}_was"), aliasable: resource) if send("#{aliased_field}_changed?")
    end

    def aliased_field
      self.class.aliased_field
    end
  end
end
