# frozen_string_literal: true

module ExternalLinkable
  extend ActiveSupport::Concern

  class_methods do
    def youtube_url_pattern(pattern = nil)
      @youtube_url_pattern = pattern if pattern.present?
      @youtube_url_pattern || 'https://youtube.com/channel/%s'
    end

    def spotify_url_pattern(pattern = nil)
      @spotify_url_pattern = pattern if pattern.present?
      @spotify_url_pattern || 'https://open.spotify.com/show/%s'
    end
  end

  included do
    def external_links?
      %i[youtube apple_podcast twitter facebook spotify wikipedia].map do |service|
        external_link_to?(service)
      end.any?
    end

    def youtube_url
      return nil unless external_link_to?(:youtube)

      self.class.youtube_url_pattern % youtube
    end

    def apple_podcast_url
      return nil unless external_link_to?(:apple_podcast)

      "https://podcasts.apple.com/#{apple_podcast}"
    end

    def spotify_url
      return nil unless external_link_to?(:spotify)

      self.class.spotify_url_pattern % spotify
    end

    def twitter_url
      return nil unless external_link_to?(:twitter)

      "https://twitter.com/#{twitter}"
    end

    def facebook_url
      return nil unless external_link_to?(:facebook)

      "https://facebook.com/#{facebook}"
    end

    def wikipedia_url
      return nil unless external_link_to?(:wikipedia)

      "https://en.wikipedia.com/wiki/#{wikipedia}"
    end

    private

    def external_link_to?(name)
      name.to_s.in?(attributes) && attribute(name.to_s).present?
    end
  end
end
