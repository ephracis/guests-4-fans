# frozen_string_literal: true

module Imageable
  extend ActiveSupport::Concern

  included do
    has_one_attached :image

    scope :without_images,
          -> { left_joins(:image_attachment).group(:id).where(active_storage_attachments: { id: nil }) }

    def attach_image(image_data)
      return true unless image_data
      return true if image_data.starts_with? '/rails/active_storage'

      if image_data.starts_with? 'data:'
        attach_image_data(image_data)
      else
        attach_image_url(image_data)
      end
      image.attached?
    end

    def attach_image_data(image_data)
      image_data.slice! 'data:'
      content_type = image_data.split(';')[0]
      encoded_data = image_data.split(';')[1]
      encoded_data.slice! 'base64,'
      decoded_data = Base64.decode64(encoded_data)
      image.attach(io: StringIO.new(decoded_data), filename: image_type_to_filename(content_type))
    end

    def attach_image_url(image_url)
      url = URI.parse(image_url)
      filename = File.basename(url.path)
      file = url.open
      image.attach(io: file, filename: filename)
    end

    def image_type_to_filename(image_content_type)
      case image_content_type
      when 'image/jpeg' then 'image.jpg'
      when 'image/png' then 'image.png'
      when 'image/webp' then 'image.webp'
      else 'image'
      end
    end
  end
end
