# frozen_string_literal: true

module Topicable
  extend ActiveSupport::Concern

  included do
    has_many :topic_topicables, as: :topicable
    has_many :topics, through: :topic_topicables
  end
end
