# frozen_string_literal: true

module Blockable
  extend ActiveSupport::Concern

  included do
    belongs_to :blocked_by, class_name: 'User', optional: true

    def self.unblocked(user = nil)
      if user
        where(created_by: user).or(unblocked)
      else
        where(blocked_by: nil)
      end
    end

    def blocked?
      !blocked_at.nil?
    end

    def block(user)
      update(blocked_by: user, blocked_at: DateTime.now)
    end

    def unblock
      update(blocked_by: nil, blocked_at: nil)
    end
  end
end
