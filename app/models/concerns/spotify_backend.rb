# frozen_string_literal: true

module SpotifyBackend
  extend ActiveSupport::Concern

  included do
    # Fetch episodes from Spotify service.
    def fetch_from_spotify
      response = Source.http_get("https://api.spotify.com/v1/shows/#{foreign_id}/episodes",
                                 { market: :US },
                                 "Bearer #{Source.spotify_access_token}")
      {
        'source' => JSON.parse(to_json),
        'episodes' => response['items'].select { |e| filter_spotify_episode(e) }
                                       .map { |e| normalize_spotify_episode(e) }
      }
    end

    def normalize_spotify_episode(episode)
      title = clean_title(episode['name'])
      {
        'description' => episode['html_description'] || episode['description'],
        'source_id' => episode['id'],
        'image' => episode['images'][0]['url'],
        'title' => title,
        'aired_at' => episode['release_date'],
        'guests' => find_guests_in_title(title)
      }
    end

    def filter_spotify_episode(episode)
      filter_pattern.blank? || episode['name'].match(/#{filter_pattern}/i)
    end
  end

  class_methods do
    # The access token used to authenticate API calls on Spotify.
    def spotify_access_token
      Rails.cache.fetch('spotify_access_token', expires_in: 50.minutes) do
        response = http_post('https://accounts.spotify.com/api/token',
                             { grant_type: :client_credentials },
                             "Basic #{spotify_auth_token}")
        response['access_token']
      end
    end

    # The authentication token used to retrieve an access token from Spotify.
    def spotify_auth_token
      Base64.strict_encode64(
        [
          Rails.application.credentials.dig(:spotify, :client_id),
          Rails.application.credentials.dig(:spotify, :client_secret)
        ].join(':')
      )
    end
  end
end
