# frozen_string_literal: true

module Editable
  extend ActiveSupport::Concern

  included do
    has_many :edits, as: :editable
    has_many :applied_edits, -> { applied }, class_name: 'Edit', as: :editable
    has_many :discarded_edits, -> { discarded }, class_name: 'Edit', as: :editable
    has_many :pending_edits, -> { pending }, class_name: 'Edit', as: :editable

    # Get the last applied edit
    #
    # This is done by iterating all applied edits, starting with the edit with the latest
    # applied_at date. The iteration is capped at the number of applied edits, and if the
    # loop is exhausted without finding an edit without a `.next` there is something wrong
    # so we throw an error.
    def last_applied_edit
      # Load all so we only make a single query.
      loaded_edits = applied_edits.order(applied_at: :desc).load

      # Start by the latest one, since it is probably the one we are looking for.
      edit = loaded_edits[0]

      # Cap the iteration at `loaded_edits.size`
      loaded_edits.size.times do
        # Found the latest
        return edit unless edit.next

        # Found a later
        edit = edit.next
      end

      raise StandardError, "Could not find last applied edit of #{self}"
    end

    # Ensure that there is an edit for `name`.
    #
    # If there is no edit for the attribute `name`, one is created with the value of
    # that attribute.
    def ensure_edit_for(name, user)
      edits.where(applied: true, name: name).first_or_create do |edit|
        edit.value = read_attribute(name)
        edit.applied_by = created_by || user
        edit.applied_at = DateTime.now
        edit.created_by = user
      end
    end
  end
end
