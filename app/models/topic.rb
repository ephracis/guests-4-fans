# frozen_string_literal: true

class Topic < ApplicationRecord
  include Slugable

  has_many :topic_topicables, dependent: :destroy
  has_many :shows, through: :topic_topicables, source: :topicable, source_type: 'Show'
  has_many :episodes, through: :topic_topicables, source: :topicable, source_type: 'Episode'
  has_many :people, through: :topic_topicables, source: :topicable, source_type: 'Person'
  has_many :books, through: :topic_topicables, source: :topicable, source_type: 'Book'

  validates :slug, presence: true,
                   uniqueness: { case_sensitive: false },
                   length: { minimum: 2 },
                   format: { with: /\A[a-z0-9_-]+\z/,
                             message: 'can only contain lowercase letters, numbers, underscore and dash' }
  validates :name, presence: true,
                   uniqueness: { case_sensitive: false },
                   length: { minimum: 2 }
end
