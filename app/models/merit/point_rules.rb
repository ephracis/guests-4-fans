# frozen_string_literal: true

# Be sure to restart your server when you modify this file.
#
# Points are a simple integer value which are given to "meritable" resources
# according to rules in +app/models/merit/point_rules.rb+. They are given on
# actions-triggered, either to the action user or to the method (or array of
# methods) defined in the +:to+ option.
#
# 'score' method may accept a block which evaluates to boolean
# (recieves the object as parameter)

module Merit
  class PointRules
    include Merit::PointRulesMethods

    def initialize
      proc = ->(badge) { points_for_level badge.level }
      score proc, on: 'badges#create'
    end

    def points_for_level(level)
      point_levels = [1, 5, 10, 25, 50, 100, 250, 500, 750, 1000]
      point_levels[level - 1]
    end
  end
end
