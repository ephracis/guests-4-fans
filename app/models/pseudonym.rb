# frozen_string_literal: true

class Pseudonym < ApplicationRecord
  belongs_to :person
  validates :name, uniqueness: { case_sensitive: false },
                   length: { minimum: 2 }
end
