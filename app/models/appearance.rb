# frozen_string_literal: true

class Appearance < ApplicationRecord
  belongs_to :person
  belongs_to :episode

  validates :episode_id, presence: true
  validates :person_id, presence: true,
                        uniqueness: { scope: :episode_id }
end
