# frozen_string_literal: true

class Show < ApplicationRecord
  include Searchable
  include Confirmable
  include Blockable
  include Slugable
  include Topicable
  include Imageable
  include Rateable
  include Editable
  include ExternalLinkable

  has_many :votes, dependent: :destroy
  has_many :episodes, dependent: :destroy
  has_many :sources, dependent: :destroy
  has_many :people, -> { distinct }, through: :episodes
  has_many :hostings, dependent: :destroy
  has_many :hosts, through: :hostings, source: :person
  has_many :unfulfilled_votes, -> { unfulfilled }, class_name: 'Vote'
  has_many :fulfilled_votes, -> { fulfilled }, class_name: 'Vote'

  validates :slug, presence: true,
                   uniqueness: { case_sensitive: false },
                   length: { minimum: 2 },
                   format: { with: /\A[a-z0-9_-]+\z/,
                             message: 'can only contain lowercase letters, numbers, underscore and dash' }
  validates :title, presence: true,
                    uniqueness: { case_sensitive: false },
                    length: { minimum: 2 }

  search_field :title
  slugged_field :title
  alias_attribute :display_name, :title

  def search_result_values
    {
      id: id,
      display: title,
      image: image
    }
  end

  def to_s
    title
  end

  def new_people
    Person.where.not(id: people.pluck(:id))
  end

  def any_votes_for?(opts = {})
    if opts[:person] && opts[:user]
      any_votes_for_person_and_user?(opts[:person], opts[:user])
    elsif opts[:person]
      any_votes_for_person?(opts[:person])
    elsif opts[:user]
      any_votes_for_user?(opts[:user])
    else
      votes.size.positive?
    end
  end

  def any_votes_for_person_and_user?(person, user)
    if user.is_a? User
      votes.map { |v| v.person_id == person.id && v.user_id == user.id }.any?
    else
      votes.map { |v| v.person_id == person.id && v.ip_address == user }.any?
    end
  end

  def any_votes_for_user?(user)
    if user.is_a? User
      votes.map { |v| v.user_id == user.id }.any?
    else
      votes.map { |v| v.ip_address == user }.any?
    end
  end

  def any_votes_for_person?(person)
    votes.map { |v| v.person_id == person.id }.any?
  end

  def self.top
    left_joins(:votes)
      .group(:id)
      .order('COUNT(votes.id) DESC')
  end
end
