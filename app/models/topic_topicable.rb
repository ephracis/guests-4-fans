# frozen_string_literal: true

class TopicTopicable < ApplicationRecord
  belongs_to :topic
  belongs_to :topicable, polymorphic: true
end
