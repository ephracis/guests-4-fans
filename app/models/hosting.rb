# frozen_string_literal: true

class Hosting < ApplicationRecord
  belongs_to :person
  belongs_to :show

  validates :show_id, presence: true
  validates :person_id, presence: true,
                        uniqueness: { scope: :show_id }
end
