# frozen_string_literal: true

class Role < ApplicationRecord
  validates :name, presence: true,
                   uniqueness: { case_sensitive: false },
                   length: { minimum: 2 },
                   format: { with: /\A[a-z0-9_-]+\z/,
                             message: 'can only contain lowercase letters, numbers, underscore and dash' }
  validates :title, presence: true,
                    uniqueness: { case_sensitive: false },
                    length: { minimum: 2 }

  has_and_belongs_to_many :users
end
