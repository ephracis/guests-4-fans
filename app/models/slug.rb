# frozen_string_literal: true

class Slug < ApplicationRecord
  belongs_to :slugable, polymorphic: true

  validates :name, presence: true,
                   uniqueness: { scope: :slugable_type }

  def hit!
    update!(last_hit_at: DateTime.now)
    self
  end
end
