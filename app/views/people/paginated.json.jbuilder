# frozen_string_literal: true

json.data do
  json.array! @records, partial: 'people/person', as: :person
end
json.pagination @pagy_metadata
