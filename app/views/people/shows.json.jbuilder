# frozen_string_literal: true

json.data do
  json.array! @records, partial: 'shows/show', as: :show
end
json.pagination @pagy_metadata
