# frozen_string_literal: true

json.data do
  json.array! @records, partial: 'episodes/episode', as: :episode
end
json.pagination @pagy_metadata
