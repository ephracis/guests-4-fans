# frozen_string_literal: true

json.extract! @results, :query

json.results do
  @results[:results].each do |section_name, section_results|
    json.set! section_name do
      json.array! section_results.each do |result|
        json.merge! result.search_result_values
        json.url url_for(result)
        json.image person_image(result) if result.is_a?(Person)
        json.image show_image(result) if result.is_a?(Show)
        json.image episode_image(result) if result.is_a?(Episode)
      end
    end
  end
end
