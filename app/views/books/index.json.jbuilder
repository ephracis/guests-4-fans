# frozen_string_literal: true

json.data do
  json.array! @records, partial: 'books/book', as: :book
end
json.pagination @pagy_metadata
