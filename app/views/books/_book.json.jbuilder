# frozen_string_literal: true

json.extract! book, :id, :slug, :name, :display_name, :description, :published_at, :created_at, :updated_at
json.image book_image(book)
json.url person_book_url(@person, book, format: :json)
json.view_url person_book_url(@person, book)
