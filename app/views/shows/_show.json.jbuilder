# frozen_string_literal: true

json.extract! show, :id, :slug, :display_name, :title, :youtube, :twitter, :facebook, :rumble, :parler, :created_at,
              :updated_at
json.voted show.any_votes_for?(person: @person, user: current_user || request.remote_ip)
json.description sanitize(show.description.to_s.gsub("\n", '<br/>'))
json.image show_image(show)

json.url show_url(show, format: :json)
json.html_url show_url(show, format: nil)
json.view_url show_url(show)
json.destroy_url show_url(show) if policy(show).destroy?
json.edit_url edit_show_url(show) if policy(show).edit?
json.report_url new_report_path_for(show) if policy(Report).create?

json.person_count show.people.count
json.episode_count show.episodes.count
json.vote_count show.votes.count
