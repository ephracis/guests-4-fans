# frozen_string_literal: true

class ReportsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_report, only: %i[show edit update destroy]

  # GET /admin/reports or /admin/reports.json
  def index
    authorize Report, :index?
    @reports = Report.all
    render layout: 'admin'
  end

  # GET /reports/new
  def new
    @report = Report.new(report_params)
    authorize @report, :create?
  end

  # GET /admin/reports/1/edit
  def edit
    authorize @report, :update?
    render layout: 'admin'
  end

  # POST /reports or /reports.json
  def create
    @report = Report.new(report_params)
    @report.user = current_user
    authorize @report

    respond_to do |format|
      if @report.save
        format.html { redirect_to url_for(@report.reportable), notice: 'Report was successfully created.' }
        format.json { render :show, status: :created, location: admin_report_path(@report) }
        format.js { render js: "window.location='#{url_for(@report.reportable)}'" }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @report.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # PATCH/PUT /admin/reports/1 or /admin/reports/1.json
  def update
    authorize @report
    respond_to do |format|
      if @report.update(report_params)
        format.html { redirect_to admin_reports_path, notice: 'Report was successfully updated.' }
        format.json { render :show, status: :ok, location: edit_admin_report_path(@report) }
        format.js { render js: "window.location='#{admin_reports_path}'" }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @report.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # DELETE /admin/reports/1 or /admin/reports/1.json
  def destroy
    authorize @report
    @report.destroy
    respond_to do |format|
      format.html { redirect_to admin_reports_url, notice: 'Report was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_report
    @report = Report.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def report_params
    params.require(:report).permit(:comment, :user_id, :category, :reportable_id, :reportable_type, :read)
  end
end
