# frozen_string_literal: true

module RateableController
  extend ActiveSupport::Concern

  included do
    # POST /RESOURCE/1/rate.json
    def rate
      authenticate_user!
      set_resource
      rating = @resource.ratings.where(user: current_user).first_or_create
      rating.value = rating_params[:value]

      respond_to do |format|
        if rating.save
          rating, my_rating = rating_values
          format.json { render json: { rating: rating, my_rating: my_rating, message: 'Rating was successful.' } }
        else
          format.json { render json: rating.errors, status: :unprocessable_entity }
        end
      end
    end

    # GET /RESOURCE/1/rating.json
    def rating
      set_resource
      rating, my_rating = rating_values
      respond_to do |format|
        format.json { render json: { rating: rating, my_rating: my_rating } }
      end
    end

    private

    def rating_params
      params.require(:rating).permit(:value)
    end

    def rating_values
      global_rating = @resource.ratings.average(:value).to_f
      personal_rating = -1
      personal_rating = @resource.ratings.where(user: current_user).pluck(:value).first || -1 if user_signed_in?
      [global_rating, personal_rating]
    end
  end
end
