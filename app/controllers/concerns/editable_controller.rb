# frozen_string_literal: true

module EditableController
  extend ActiveSupport::Concern

  included do
    def create_edits(resource, parameters)
      if policy(resource).update?
        return false unless resource.update(parameters)
      else
        parameters.each do |k, v|
          next if resource.read_attribute(k).to_s == v.to_s

          edit = resource.edits.new(name: k, value: v, created_by: current_user, editable: resource)
          return false unless edit.save

          return false if policy(resource).update? && !edit.apply!(current_user)
        end
      end

      true
    end

    def edits
      authorize @resource, :edit?
      @edits = edits_for(@resource)
      render 'edits/index'
    end

    private

    def successful_update_notice
      if policy(@resource).update?
        t("#{@resource.class.to_s.parameterize}.notice.update")
      else
        t('edit.notice.create')
      end
    end

    def edits_for(resource)
      if policy(resource).update?
        resource.pending_edits
      elsif policy(resource).edit?
        resource.pending_edits.where(created_by: current_user)
      else
        []
      end
    end
  end
end
