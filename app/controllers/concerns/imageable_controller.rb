# frozen_string_literal: true

module ImageableController
  extend ActiveSupport::Concern

  included do
    def attach_image(resource, parameters)
      if parameters[:image] == ''
        resource.image.purge
      else
        return true unless parameters[:image].present?
        return false unless resource.attach_image(parameters[:image])

      end
      parameters.extract! :image
      true
    end
  end
end
