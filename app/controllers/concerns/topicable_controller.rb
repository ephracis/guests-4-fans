# frozen_string_literal: true

module TopicableController
  extend ActiveSupport::Concern

  included do
    def update_topics(resource, parameters)
      return true unless parameters[:topics].present?

      authorize resource, :update?

      # remove old topics
      resource.topics.each do |topic|
        next if topic.name.in?(parameters[:topics])
        return false unless resource.topics.delete topic
      end

      # create new topics
      parameters[:topics].each do |topic_name|
        next if resource.topics.where('LOWER(name) = ?', topic_name.downcase).any?

        topic = Topic.where('LOWER(name) = ?', topic_name.downcase).first
        unless topic
          topic = Topic.new(name: topic_name)
          topic.generate_slug
          return false unless topic.save
        end

        resource.topics << topic
      end

      parameters.extract! :topics
      true
    end
  end
end
