# frozen_string_literal: true

class AdminController < ApplicationController
  before_action :authenticate_user!, :authorize_admin!

  def dashboard
    @users = User.order(created_at: :desc).limit(5)
    @shows = Show.order(created_at: :desc).limit(5)
    @people = Person.order(created_at: :desc).limit(5)
    @episodes = Episode.order(created_at: :desc).limit(5)
    @versions = [
      { name: 'ShocaDB', number: ShocaDB::Application.version },
      { name: 'Rails', number: Rails.version },
      { name: 'Ruby', number: RUBY_VERSION }
    ]
    @statistics = [
      { name: 'people', number: Person.count },
      { name: 'shows', number: Show.count },
      { name: 'episodes', number: Episode.count },
      { name: 'users', number: User.count },
      { name: 'votes', number: Vote.count },
      { name: 'ratings', number: Rating.count }
    ]
  end

  def unconfirmed
    limit = params[:limit] || 20

    if policy(Person).confirm?
      @people = Person.where(confirmed_by: nil).order(created_at: :desc)
      @people_count = @people.size
      @people = @people.limit(limit)
    else
      @people = []
    end

    if policy(Show).confirm?
      @shows = Show.where(confirmed_by: nil).order(created_at: :desc)
      @shows_count = @shows.size
      @shows = @shows.limit(limit)
    else
      @shows = []
    end

    if policy(Episode).confirm?
      @episodes = Episode.where(confirmed_by: nil).order(created_at: :desc)
      @episodes_count = @episodes.size
      @episodes = @episodes.limit(limit)
    else
      @episodes = []
    end

    if policy(Book).confirm?
      @books = Book.where(confirmed_by: nil).order(created_at: :desc)
      @books_count = @books.size
      @books = @books.limit(limit)
    else
      @books = []
    end
  end

  def imageless
    limit = params[:limit] || 20

    if policy(Person).confirm?
      @people = Person.without_images.order(created_at: :desc).load
      @people_count = @people.size
      @people = @people.limit(limit)
    else
      @people = []
    end

    if policy(Person).confirm?
      @shows = Show.without_images.order(created_at: :desc).load
      @shows_count = @shows.size
      @shows = @shows.limit(limit)
    else
      @shows = []
    end

    if policy(Person).confirm?
      @episodes = Episode.without_images.order(created_at: :desc).load
      @episodes_count = @episodes.size
      @episodes = @episodes.limit(limit)
    else
      @episodes = []
    end

    if policy(Book).confirm?
      @books = Book.without_images.order(created_at: :desc).load
      @books_count = @books.size
      @books = @books.limit(limit)
    else
      @books = []
    end
  end
end
