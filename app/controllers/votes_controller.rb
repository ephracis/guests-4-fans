# frozen_string_literal: true

class VotesController < ApplicationController
  before_action :set_settings, only: :create
  before_action :set_show, only: %i[for_show mine_for_show]
  before_action :set_person, only: %i[for_person mine_for_person]

  # GET /votes/show/:id.json
  def for_show
    @votes = @show.votes.order(created_at: :desc).limit(5)
    @people = @votes.map(&:person).uniq
    render :people
  end

  # GET /votes/person/:id.json
  def for_person
    @votes = @person.votes.order(created_at: :desc).limit(5)
    @shows = @votes.map(&:show).uniq
    render :shows
  end

  # GET /votes/show/:id/mine.json
  def mine_for_show
    @votes = my_votes.where(show: @show).order(created_at: :desc).limit(5)
    @people = @votes.map(&:person).uniq
    render :people
  end

  # GET /votes/person/:id/mine.json
  def mine_for_person
    @votes = my_votes.where(person: @person).order(created_at: :desc).limit(5)
    @shows = @votes.map(&:show).uniq
    render :shows
  end

  # POST /votes
  # POST /votes.json
  def create
    save_redirect_path
    @vote = find_or_create(vote_params)
    @vote.show = create_new_show(params[:show_name]) if params[:show_name]
    limited = !@vote.persisted? && reached_limit?(@vote)
    @vote.person = create_new_person(params[:person_name]) if !limited && params[:person_name]

    authorize @vote

    respond_to do |format|
      if limited
        format.html do
          flash[:alert] = limit_response[:message]
          redirect_to after_vote_path
        end
        format.json { render json: limit_response, status: :forbidden }
      elsif save_or_destroy(@vote)
        format.html do
          flash[:notice] = t('vote.notice.create')
          redirect_to after_vote_path
        end
        format.json { render :show, status: :created, location: @vote }
      else
        format.html do
          flash[:alert] = @vote.errors.full_messages
          redirect_to after_vote_path
        end
        format.json { render json: @vote.errors.full_messages, status: :unprocessable_entity }
      end
    end
  end

  private

  def set_show
    @show = Show.find_by!(slug: params[:id])
  end

  def set_person
    @person = Person.find_by!(slug: params[:id])
  end

  # Only allow a list of trusted parameters through.
  def vote_params
    params.require(:vote).permit(:value, :show_id, :person_id)
  end

  # Get votes for the current user or IP
  def my_votes
    return current_user.votes if current_user

    Vote.where(ip_address: request.remote_ip)
  end

  # Either find an opposite vote or create a new vote.
  #
  # If there is a similar vote, but with an opposite value, we return that one.
  # If no such vote is found, we create a new vote.
  #
  # This is used to allow us to remove existing votes, instead of creating two
  # votes for same person, same show, but with opposite values. This would just
  # clutter the database.
  def find_or_create(vote_params)
    @vote = find_opposite_vote(vote_params)
    return @vote if @vote

    @vote = Vote.new(vote_params)
    if current_user
      @vote.user = current_user
    else
      @vote.ip_address = request.remote_ip
    end
    @vote
  end

  # Find a vote with all the same parameters, but opposite value.
  #
  # The opposite vote must also be unfulfilled, since we want to allow users to cast a new positive vote
  # after a guest has appeared and fulfilled a vote, for example.
  def find_opposite_vote(vote_params)
    if current_user
      current_user.votes.where(
        person_id: vote_params[:person_id].to_i,
        show_id: vote_params[:show_id].to_i,
        fulfilled: false,
        value: vote_params[:value].to_i * -1
      ).first
    else
      Vote.where(
        ip_address: request.remote_ip,
        person_id: vote_params[:person_id].to_i,
        show_id: vote_params[:show_id].to_i,
        fulfilled: false,
        value: vote_params[:value].to_i * -1
      ).first
    end
  end

  # Either save or destroy a given vote.
  #
  # This is used on the result from `find_or_create` since we want to either save
  # the vote (if it is new) or destroy it (if it is an opposite vote).
  #
  # We return the success result of the save/destroy operation.
  def save_or_destroy(vote)
    if vote.persisted?
      vote.destroy
    else
      vote.save
    end
  end

  # Check if the user has reached the limit of votes.
  def reached_limit?(vote)
    if current_user
      current_user.unfulfilled_votes_for(vote.show) >= @settings.max_votes_per_show
    else
      Vote.where(ip_address: request.remote_ip).size >= @settings.max_votes_per_ip
    end
  end

  # JSON response when limit is reached.
  def limit_response
    if current_user
      {
        error: :limit_reached,
        limit: ApplicationSettings.last.max_votes_per_show,
        message: t('vote.notice.limit', count: 0)
      }
    else
      {
        error: :limit_reached,
        limit: ApplicationSettings.last.max_votes_per_ip,
        message: t('vote.notice.limit_anonymous', count: 0)
      }
    end
  end

  def after_vote_path
    return saved_redirect_path if saved_redirect_path.present?

    url_for(@vote.show)
  end
end
