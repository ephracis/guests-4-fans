# frozen_string_literal: true

class PeopleController < ApplicationController
  include Pagy::Backend
  include ConfirmableController
  include BlockableController
  include TopicableController
  include ImageableController
  include RateableController
  include EditableController
  include AliasableController

  before_action :authenticate_user!, only: %i[new create edit update destroy favorite merge_form merge]
  before_action :set_resource, except: %i[index new create previously_on_show new_to_show]
  before_action :set_edits, only: %i[show shows episodes about]
  before_action :set_show

  # GET /people
  # GET /people.json
  def index
    @people = if @show
                guests_for_show
              elsif params[:q].present?
                Person.search(params[:q])
              else
                Person.top
              end
    @people = @people.unblocked(current_user).confirmed(current_user).limit(params[:limit] || 20)
  end

  # GET /shows/1/guests/previous.json
  def previously_on_show
    @people = @show.people
                   .select('people.*, MAX(episodes.aired_at)')
                   .order('MAX(episodes.aired_at) DESC')
                   .group(:id)
                   .unblocked(current_user)
                   .confirmed(current_user)
    @pagy, @records = pagy_arel(@people, items: page_limit)
    @pagy_metadata = pagy_metadata(@pagy)
    render :paginated
  end

  # GET /shows/1/guests/other.json
  def new_to_show
    @people = @show.new_people.order_by_votes.unblocked(current_user).confirmed(current_user)
    @pagy, @records = pagy_arel(@people, items: page_limit)
    @pagy_metadata = pagy_metadata(@pagy)
    render :paginated
  end

  # GET /shows/1/people/1
  def for_show
    @returning = @person.in?(@show.people)
    @voted = current_user && current_user.unfulfilled_votes.where(person: @person, show: @show).any?
    @episodes = @person.episodes.unblocked(current_user).confirmed(current_user).order(aired_at: :desc).limit(5)
    @people = @show.people.unblocked(current_user).confirmed(current_user).top.limit(5)
    @last_episode = @person.episodes.where(show: @show).order(aired_at: :desc).first if @returning
  end

  # GET /people/1
  # GET /people/1.json
  def show
    render layout: 'person'
  end

  # GET /person/1/shows or /person/1/shows.json
  def shows
    @shows = @person.shows.select('shows.*, MAX(episodes.aired_at)').unblocked(current_user).confirmed(current_user)
                    .order('MAX(episodes.aired_at) DESC').group(:id)
    @pagy, @records = pagy(@shows, items: page_limit)
    @pagy_metadata = pagy_metadata(@pagy)
    render layout: 'person'
  end

  # GET /person/1/episodes or /person/1/episodes.json
  def episodes
    @episodes = @person.episodes.joins(:show).joins(:people).group(:id).unblocked(current_user).confirmed(current_user)
                       .order(aired_at: :desc)
    @pagy, @records = pagy(@episodes, items: page_limit)
    @pagy_metadata = pagy_metadata(@pagy)
    render layout: 'person'
  end

  # GET /person/1/about
  def about
    render layout: 'person'
  end

  # GET /people/new
  def new
    @person = Person.new
    authorize @person, :create?
  end

  # GET /people/1/edit
  def edit
    authorize @person
    save_redirect_path
    @cancel_path = saved_redirect_path || person_path(@person)
  end

  # POST /people
  # POST /people.json
  def create
    @person = Person.new
    authorize @person

    respond_to do |format|
      if create_person(@person)
        flash[:notice] = t('person.notice.create')
        format.html { redirect_to @person }
        format.json { render :show, status: :created, location: @person }
        format.js { render js: "window.location='#{url_for(@person)}'" }
      else
        format.html { render :new }
        format.json { render json: @person.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # PATCH/PUT /people/1
  # PATCH/PUT /people/1.json
  def update
    authorize @person, :edit?
    respond_to do |format|
      if update_person
        flash[:notice] = successful_update_notice
        format.html { redirect_to after_update_path }
        format.json { render :show, status: :ok, location: @person }
        format.js { render js: "window.location='#{after_update_path}'" }
      else
        format.html { render :edit }
        format.json { render json: @person.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # DELETE /people/1
  # DELETE /people/1.json
  def destroy
    authorize @person
    @person.destroy
    respond_to do |format|
      format.html { redirect_to people_url, notice: t('person.notice.destroy') }
      format.json { head :no_content }
    end
  end

  # POST /people/1/favorite
  # POST /people/1/favorite.json
  def favorite
    authorize @person
    if @person.favorited_by? current_user
      @person.favorites.where(user: current_user).destroy_all
      notice = t('person.notice.unfollow')
    else
      @person.favorites.create(user: current_user)
      notice = t('person.notice.follow')
    end
    respond_to do |format|
      format.html { redirect_to person_url(@person), notice: notice }
      format.json { render :show, status: :ok, location: @person }
    end
  end

  def merge_form
    authorize @person, :merge?
    render :merge
  end

  def merge
    authorize @person
    @original = Person.find_by(slug: params[:original_id])

    respond_to do |format|
      if !@original
        flash[:missing] = t('person.merge.empty')
        format.html { render :merge }
        format.js
      elsif @person.duplicate_of! @original
        flash[:notice] = t('person.notice.merge', person: @person, original: @original)
        format.html { redirect_to @original }
        format.js { render js: "window.location='#{url_for(@original)}'" }
      else
        format.html { render :merge }
        format.js
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_resource
    @resource = @person = Person.find_by!(slug: params[:id])
  rescue ActiveRecord::RecordNotFound
    resource = Slug.find_by!(name: params[:id], slugable_type: 'Person').hit!.slugable
    redirect_to id: resource.to_param, status: :moved_permanently
  end

  def set_show
    @show = Show.find_by!(slug: params[:show_id]) if params[:show_id]
  rescue ActiveRecord::RecordNotFound
    resource = Slug.find_by!(name: params[:show_id], slugable_type: 'Show').hit!.slugable
    redirect_to show_id: resource.to_param, status: :moved_permanently
  end

  def set_edits
    @edits = edits_for @person
  end

  # Get a list of guests for a specific show.
  #
  # Parameter `mode` can be set to either `new` or `old` to specify whether the list should contain guests that has
  # never been on the show, or who has.
  def guests_for_show
    return unless @show

    case (params[:mode] || '').to_sym
    when :mine
      if current_user
        current_user.people_voted_on.where(votes: { show: @show })
      else
        Person.joins(:votes).where(votes: { ip_address: request.remote_ip, show: @show })
      end
    when :new
      @show.new_people.top(@show)
    when :old
      @show.people.top(@show)
    else
      Person.top(@show)
    end
  end

  # Only allow a list of trusted parameters through.
  def person_params
    params.require(:person).permit(
      :name, :description, :slug, :confirmed, :blocked, :image, :twitter, :facebook, :website, :wikipedia,
      aliases: [], topics: []
    )
  end

  # Create person model.
  def create_person(person)
    parameters = person_params
    return false unless update_resource_confirmation person, parameters
    return false unless update_resource_blockade person, parameters
    return false unless attach_image person, parameters

    person.attributes = parameters.except(:aliases, :topics)
    person.created_by = current_user
    person.generate_slug unless parameters[:slug]

    return false unless person.save
    return false unless update_aliases person, parameters
    return false unless update_topics person, parameters

    true
  end

  # Update person model.
  def update_person
    parameters = person_params # save result from method so it can be modified through the update calls
    return false unless update_resource_confirmation @person, parameters
    return false unless update_resource_blockade @person, parameters
    return false unless update_aliases @person, parameters
    return false unless update_topics @person, parameters
    return false unless attach_image @person, parameters
    return false unless create_edits @person, parameters

    true
  end

  def after_update_path
    return saved_redirect_path if saved_redirect_path.present?

    if person_params.keys.length == 1 && person_params[:confirmed]
      admin_unconfirmed_path
    else
      url_for(@person)
    end
  end
end
