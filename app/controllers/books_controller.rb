# frozen_string_literal: true

class BooksController < ApplicationController
  include Pagy::Backend
  include ConfirmableController
  include BlockableController
  include TopicableController
  include ImageableController
  include RateableController
  include EditableController

  before_action :authenticate_user!, except: %i[index show rating]
  before_action :set_resource, only: %i[show edit update destroy]
  before_action :set_person

  # GET /person/1/books or /person/1/books.json
  def index
    @books = @person.books.all
    @pagy, @records = pagy(@books, items: page_limit)
    @pagy_metadata = pagy_metadata(@pagy)
    render layout: 'person'
  end

  # GET /person/1/books/new
  def new
    @book = @person.books.new
    authorize @book, :create?
  end

  def show
    if @book.amazon
      redirect_to @book.amazon_url
    else
      redirect_to person_books_path(@person)
    end
  end

  # GET /person/1/books/1/edit
  def edit
    authorize @book
  end

  # POST /person/1/books or /person/1/books.js
  def create
    @book = Book.with_name(book_params[:name]).first
    if @book
      success = true
    else
      @book = @person.books.new
      authorize @book
      success = create_book(@book)
    end

    respond_to do |format|
      if success
        @person.books << @book unless @book.in? @person.books
        flash[:notice] = t('book.notice.create')
        format.html { redirect_to edit_person_book_url(@person, @book) }
        format.js { render js: "window.location='#{edit_person_book_url(@person, @book)}'" }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # PATCH/PUT /person/1/books/1 or /person/1/books/1.js
  def update
    authorize @book, :edit?
    respond_to do |format|
      if update_book
        flash[:notice] = successful_update_notice
        format.html { redirect_to after_update_path }
        format.js { render js: "window.location='#{after_update_path}'" }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # DELETE /person/1/books/1 or /person/1/books/1.json
  def destroy
    authorize @book
    @book.destroy
    respond_to do |format|
      format.html { redirect_to person_books_url(@person), notice: t('book.notice.destroy') }
      format.json { head :no_content }
    end
  end

  private

  def set_resource
    @resource = @book = Book.find_by(slug: params[:id])
  rescue ActiveRecord::RecordNotFound
    resource = Slug.find_by!(name: params[:id] || params[:person_id], slugable_type: 'Book').hit!.slugable
    redirect_to id: resource.to_param, status: :moved_permanently
  end

  def set_person
    @person = Person.find_by!(slug: params[:person_id])
  rescue ActiveRecord::RecordNotFound
    resource = Slug.find_by!(name: params[:person_id], slugable_type: 'Person').hit!.slugable
    redirect_to person_id: resource.to_param, status: :moved_permanently
  end

  def book_params
    params.require(:book).permit(
      :name, :slug, :description, :published_at, :image, :confirmed, :blocked, :amazon, topics: []
    )
  end

  # Create book model.
  def create_book(book)
    parameters = book_params
    return false unless update_resource_confirmation book, parameters
    return false unless update_resource_blockade book, parameters
    return false unless attach_image book, parameters

    book.attributes = parameters.except(:topics)
    book.created_by = current_user
    book.generate_slug unless parameters[:slug]

    return false unless book.save
    return false unless update_topics book, parameters

    true
  end

  # Update book model.
  def update_book
    parameters = book_params # save result from method so it can be modified through the update calls
    return false unless update_resource_confirmation @book, parameters
    return false unless update_resource_blockade @book, parameters
    return false unless update_topics @book, parameters
    return false unless attach_image @book, parameters
    return false unless create_edits @book, parameters

    true
  end

  def after_update_path
    if book_params.keys.length == 1 && book_params[:confirmed]
      admin_unconfirmed_path
    else
      person_books_path(@person)
    end
  end
end
