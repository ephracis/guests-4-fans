# frozen_string_literal: true

class Admin::SettingsController < ApplicationController
  before_action :authenticate_user!, :authorize_root!
  before_action :set_settings
  layout 'admin'

  def edit
    authorize @settings, :update?
  end

  def update
    authorize @settings
    respond_to do |format|
      if @settings.update(settings_params)
        format.html { redirect_to admin_settings_path, notice: 'Settings was successfully updated.' }
        format.json { render :edit, status: :ok, location: admin_settings_path }
        format.js { render js: "window.location='#{admin_settings_path}'" }
      else
        format.html { render :edit }
        format.json { render json: @settings.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  private

  # Only allow a list of trusted parameters through.
  def settings_params
    params.require(:application_settings).permit(
      :max_votes_per_show,
      :max_votes_per_ip
    )
  end
end
