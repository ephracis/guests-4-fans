# frozen_string_literal: true

class SourcesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_show
  before_action :set_source, only: %i[edit update destroy fetch generate]

  # GET /shows/x/sources
  # GET /shows/x/sources.json
  def index
    authorize @show, :update?
    @sources = @show.sources.all
  end

  # GET /shows/x/sources/new
  def new
    @source = @show.sources.new
    authorize @source, :create?
  end

  # GET /shows/x/sources/1/edit
  def edit
    authorize @source, :update?
  end

  # POST /shows/x/sources
  # POST /shows/x/sources.json
  def create
    @source = @show.sources.new(source_params)
    authorize @source

    respond_to do |format|
      if @source.save
        format.html do
          redirect_to edit_show_source_path(@source.show, @source), notice: 'Source was successfully created.'
        end
        format.json { render :show, status: :created, location: [@show, @source] }
        format.js { render js: "window.location='#{edit_show_source_path(@source.show, @source)}'" }
      else
        format.html { render :new }
        format.json { render json: @source.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # PATCH/PUT /shows/x/sources/1
  # PATCH/PUT /shows/x/sources/1.json
  def update
    authorize @source
    respond_to do |format|
      if @source.update(source_params)
        format.html do
          redirect_to edit_show_source_path(@source.show, @source), notice: 'Source was successfully updated.'
        end
        format.json { render :show, status: :ok, location: [@show, @source] }
        format.js { render js: "window.location='#{edit_show_source_path(@source.show, @source)}'" }
      else
        format.html { render :edit }
        format.json { render json: @source.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # DELETE /shows/x/sources/1
  # DELETE /shows/x/sources/1.json
  def destroy
    authorize @source
    @source.destroy
    respond_to do |format|
      format.html { redirect_to edit_show_path(@show), notice: 'Source was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  # GET /shows/x/sources/1/fetch.json
  def fetch
    authorize @source
    @result = @source.fetch
    @result['episodes'].each do |episode|
      episode['exists'] = Episode.with_title(episode['title']).present?
    end
    respond_to do |format|
      format.json { render json: @result, status: :ok }
    end
  end

  # POST /shows/x/sources/1/generate
  # POST /shows/x/sources/1/generate.json
  def generate
    authorize @source
    respond_to do |format|
      @show.episodes.from_source params
      format.html { redirect_to show_path(@show), notice: 'Episodes was successfully generated.' }
      format.json { render :show, status: :ok, location: [@show, @source] }
    rescue RuntimeError => e
      format.html { render :edit }
      format.json { render json: { message: e.message }, status: :unprocessable_entity }
    end
  end

  private

  def set_show
    @show = Show.find_by!(slug: params[:show_id])
  end

  def set_source
    @source = @show.sources.find_by!(id: params[:id])
  end

  # Only allow a list of trusted parameters through.
  def source_params
    params.require(:source).permit(
      :foreign_id, :kind, :parse_title_for_guests, :guests_pattern, :split_pattern, :scheduled,
      :youtube_kind, :title_pattern, :filter_pattern
    )
  end
end
