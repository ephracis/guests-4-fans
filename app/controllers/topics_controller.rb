# frozen_string_literal: true

class TopicsController < ApplicationController
  def show
    @topic = Topic.find_by(slug: params[:id])
    @episodes = @topic.episodes.limit(20)
    @shows = @topic.shows.limit(20)
    @people = @topic.people.limit(20)
    @books = @topic.books.limit(20)
  end
end
