# frozen_string_literal: true

class EpisodesController < ApplicationController
  include Pagy::Backend
  include ConfirmableController
  include BlockableController
  include TopicableController
  include ImageableController
  include RateableController
  include EditableController
  include AliasableController

  before_action :authenticate_user!, except: %i[index show appearances rating]
  before_action :set_show
  before_action :set_person, only: :index
  before_action :set_resource, only: %i[show edit update destroy appearances appear disappear edits merge_form merge]

  # GET /show/X/episodes
  # GET /show/X/episodes.json
  def index
    @episodes = @show ? @show.episodes : @person.episodes
    @episodes = @episodes.search(params[:q]) if params[:q].present?
    @episodes = @episodes.unblocked(current_user).confirmed(current_user).order(aired_at: :desc)
    @pagy, @records = pagy(@episodes, items: page_limit)
    @pagy_metadata = pagy_metadata(@pagy)
  end

  # GET /show/X/episodes/1
  # GET /show/X/episodes/1.json
  def show
    @edits = edits_for @episode
    render layout: 'sidebar'
  end

  # GET /show/X/episodes/new
  def new
    @episode = Episode.new(show: @show)
    authorize @episode, :create?
  end

  # GET /show/X/episodes/1/edit
  def edit
    authorize @episode, :update?
    save_redirect_path
    @cancel_path = saved_redirect_path || episode_path(@episode)
  end

  # POST /show/X/episodes
  # POST /show/X/episodes.json
  def create
    @episode = Episode.new
    authorize @episode

    respond_to do |format|
      if create_episode(@episode)
        flash[:notice] = t('episode.notice.create')
        format.html { redirect_to [@show, @episode] }
        format.json { render :show, status: :created, location: [@show, @episode] }
        format.js { render js: "window.location='#{show_episode_url(@episode.show, @episode)}'" }
      else
        format.html { render :new }
        format.json { render json: @episode.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # PATCH/PUT /show/X/episodes/1
  # PATCH/PUT /show/X/episodes/1.json
  def update
    authorize @episode, :edit?
    respond_to do |format|
      if update_episode
        flash[:notice] = successful_update_notice
        format.html { redirect_to after_update_path }
        format.json { render :show, status: :ok, location: [@show, @episode] }
        format.js { render js: "window.location='#{after_update_path}'" }
      else
        format.html { render :edit }
        format.json { render json: @episode.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # DELETE /show/X/episodes/1
  # DELETE /show/X/episodes/1.json
  def destroy
    authorize @episode
    @episode.destroy
    respond_to do |format|
      format.html { redirect_to show_url(@show), notice: t('episode.notice.destroy') }
      format.json { head :no_content }
    end
  end

  # GET /show/X/episodes/1/appearances
  # GET /show/X/episodes/1/appearances.json
  def appearances; end

  # POST /show/X/episodes/1/appearances
  # POST /show/X/episodes/1/appearances.json
  def appear
    person = find_or_create_person(person_params[:name])
    appearance = @episode.appearances.new(person: person)
    respond_to do |format|
      if appearance.save
        notify_users(appearance)
        format.html { redirect_to [@show, @episode], notice: 'Guest was successfully added to episode.' }
        format.json { render :appearances, status: :ok, location: [@show, @episode] }
        format.js { render js: "window.location='#{show_episode_url(@episode.show, @episode)}'" }
      else
        @edits = edits_for @episode
        format.html { render :show }
        format.json { render json: appearance.errors.full_messages, status: :unprocessable_entity }
        format.js
      end
    end
  rescue ActiveRecord::RecordNotFound
    error = 'No such guest found.'
    respond_to do |format|
      format.html { render :show, alert: error, status: :not_found }
      format.json { render json: { error: error }, status: :not_found }
    end
  end

  # DELETE /show/X/episodes/1/appearances
  # DELETE /show/X/episodes/1/appearances.json
  def disappear
    person = find_or_create_person(person_params[:name])
    @episode.appearances.where(person: person).delete_all
    respond_to do |format|
      format.html { redirect_to [@show, @episode], notice: 'Guest was successfully removed from episode.' }
      format.json { render :show, status: :ok, location: [@show, @episode] }
      format.js { render js: "window.location='#{show_episode_url(@episode.show, @episode)}'" }
    end
  end

  # GET /show/X/episodes/1/merge
  def merge_form
    authorize @episode, :merge?
    render :merge
  end

  # POST /show/X/episodes/1/merge
  def merge
    authorize @episode
    @original = Episode.find_by(slug: params[:original_id])

    respond_to do |format|
      if !@original
        flash[:missing] = t('episode.merge.empty')
        format.html { render :merge }
        format.js
      elsif @episode.duplicate_of! @original
        flash[:notice] = t('episode.notice.merge', episode: @episode, original: @original)
        format.html { redirect_to @original }
        format.js { render js: "window.location='#{url_for(@original)}'" }
      else
        format.html { render :merge }
        format.js
      end
    end
  end

  private

  def set_resource
    @resource = @episode = @show.episodes.find_by!(slug: params[:id] || params[:episode_id])
  rescue ActiveRecord::RecordNotFound
    resource = Slug.find_by!(name: params[:id], slugable_type: 'Episode').hit!.slugable
    redirect_to id: resource.to_param, status: :moved_permanently
  end

  def set_show
    @show = Show.find_by!(slug: params[:show_id]) if params[:show_id].present?
  rescue ActiveRecord::RecordNotFound
    resource = Slug.find_by!(name: params[:show_id], slugable_type: 'Show').hit!.slugable
    redirect_to show_id: resource.to_param, status: :moved_permanently
  end

  def set_person
    @person = Person.find_by!(slug: params[:person_id]) if params[:person_id].present?
  rescue ActiveRecord::RecordNotFound
    resource = Slug.find_by!(name: params[:person_id], slugable_type: 'Person').hit!.slugable
    redirect_to person_id: resource.to_param, status: :moved_permanently
  end

  # Only allow a list of trusted parameters through.
  def episode_params
    params.require(:episode).permit(
      :slug, :title, :description, :aired_at, :youtube, :apple_podcast, :spotify, :website, :image, :confirmed,
      :blocked, aliases: [], topics: []
    )
  end

  def person_params
    params.require(:person).permit(:id, :name)
  end

  def notify_users(appearance)
    show_url = url_for([appearance.episode.show, appearance.episode])
    title = format('%<person>s appeared on a show', person: appearance.person.name)
    content = format('%<person>s appeared on the show %<show>s',
                     person: helpers.link_to(appearance.person.name, appearance.person),
                     show: helpers.link_to(appearance.episode.show.title, show_url))

    votes = appearance.episode.show.votes.where(person: appearance.person, fulfilled: false)
    votes.map(&:user).uniq.each do |user|
      user.notifications.create(
        title: title,
        content: content,
        image: appearance.person.image,
        read: false
      )
    end
    votes.update_all(fulfilled: true)
  end

  # Create episode model.
  def create_episode(episode)
    parameters = episode_params
    return false unless update_resource_confirmation episode, parameters
    return false unless update_resource_blockade episode, parameters
    return false unless attach_image episode, parameters

    episode.attributes = parameters.except(:aliases, :topics)
    episode.created_by = current_user
    episode.generate_slug unless parameters[:slug]
    episode.show = @show

    return false unless episode.save
    return false unless update_aliases episode, parameters
    return false unless update_topics episode, parameters

    true
  end

  # Update episode model.
  def update_episode
    parameters = episode_params # save result from method so it can be modified through the update calls
    return false unless update_resource_confirmation @episode, parameters
    return false unless update_resource_blockade @episode, parameters
    return false unless update_aliases @episode, parameters
    return false unless update_topics @episode, parameters
    return false unless attach_image @episode, parameters
    return false unless create_edits @episode, parameters

    true
  end

  def after_update_path
    return saved_redirect_path if saved_redirect_path.present?

    if episode_params.keys.length == 1 && episode_params[:confirmed]
      admin_unconfirmed_path
    else
      url_for(@episode)
    end
  end
end
