# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include Pundit::Authorization

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  around_action :use_time_zone, if: :current_user
  around_action :switch_locale
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :store_user_location!, if: :storable_location?
  before_action :set_unconfirmed_count

  def search
    @results = {
      query: params[:q],
      results: {
        people: Person.search(params[:q]).limit(20),
        shows: Show.search(params[:q]).limit(20),
        episodes: Episode.search(params[:q]).limit(20)
      }
    }
  end

  # Create shortcut for URLs to nested resources
  def episode_url(episode, opts = {})
    show_episode_url(episode.show || Show.first, episode, opts)
  end
  helper_method :episode_url

  def episode_path(episode, opts = {})
    show_episode_path(episode.show || Show.first, episode, opts)
  end
  helper_method :episode_path

  def edit_episode_path(episode, opts = {})
    edit_show_episode_path(episode.show || Show.first, episode, opts)
  end
  helper_method :edit_episode_path

  def edits_episode_path(episode, opts = {})
    edits_show_episode_path(episode.show || Show.first, episode, opts)
  end
  helper_method :edits_episode_path

  def rate_episode_url(episode, opts = {})
    rate_show_episode_url(episode.show || Show.first, episode, opts)
  end
  helper_method :rate_episode_url

  def rating_episode_url(episode, opts = {})
    rating_show_episode_url(episode.show || Show.first, episode, opts)
  end
  helper_method :rating_episode_url

  def switch_locale(&action)
    locale = extract_locale_from_settings ||
             extract_locale_from_accept_language_header ||
             I18n.default_locale
    I18n.with_locale(locale, &action)
  end

  protected

  # Check if the current user is root.
  #
  # Will return true if there are no roots.
  def root?
    return false unless user_signed_in?
    return true if current_user.role? :root
    return true unless any_with_role? :root

    false
  end
  helper_method :root?

  # Check if the current user is admin.
  def admin?
    return false unless user_signed_in?
    return true if current_user.role?(:root) || current_user.role?(:admin)

    false
  end
  helper_method :admin?

  # Ensure that the user is authorized as root.
  def authorize_root!
    return if root?

    error_msg = t('auth.not_root')
    if (params[:format] || 'html').to_sym == :html
      redirect_to root_path, flash: { error: error_msg }
    else
      json = { error: error_msg }
      render json: json, status: :forbidden
    end
  end

  # Ensure that the user is authorized as admin.
  def authorize_admin!
    return if admin?

    error_msg = t('auth.not_admin')
    if (params[:format] || 'html').to_sym == :html
      redirect_to root_path, flash: { error: error_msg }
    else
      json = { error: error_msg }
      render json: json, status: :forbidden
    end
  end

  def set_unconfirmed_count
    return unless user_signed_in?
    return unless current_user.roles?(:admin, :root)

    @unconfirmed_count = [
      Episode.where(confirmed_by: nil).size,
      Person.where(confirmed_by: nil).size,
      Show.where(confirmed_by: nil).size,
      Book.where(confirmed_by: nil).size
    ].sum
  end

  def configure_permitted_parameters
    added_attrs = %i[
      username email password password_confirmation remember_me country time_zone locale fullname email_notifications
      twitter facebook
    ]
    devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
    devise_parameter_sanitizer.permit :sign_in, keys: %i[login password]
    devise_parameter_sanitizer.permit :account_update, keys: added_attrs
  end

  def set_settings
    @settings = ApplicationSettings.last || ApplicationSettings.create!
  end

  def after_sign_in_path_for(resource_or_scope)
    stored_location_for(resource_or_scope) || root_path
  end

  def log_event(name, parameters)
    session[:events] ||= []
    session[:events] << { name: name, parameters: parameters }
  end

  # Create a new person.
  def create_new_person(name)
    person = Person.find_by_alias(name)
    unless person
      authorize Person, :create?
      person = Person.new(name: name, created_by: current_user)
      person.generate_slug
      return false unless person.save
    end
    person
  end

  def find_or_create_person(name)
    if policy(Person).create?
      create_new_person(name)
    else
      Person.find_by_alias!(name)
    end
  end

  # Create a new show.
  def create_new_show(title)
    show = Show.find_by(title: title)
    unless show
      authorize Show, :create?
      show = Show.new(title: title, created_by: current_user)
      show.generate_slug
      return false unless show.save
    end
    show
  end

  def save_redirect_path
    session[:redirect_path] = request.referer if request.referer.present? && request.referer != request.original_url
  end

  def saved_redirect_path
    session[:redirect_path]
  end

  def page_limit(default = 5)
    if params[:limit].present? && params[:limit].to_i.positive?
      params[:limit].to_i
    else
      default
    end
  end

  private

  # Check if there are any users with the 'root' role.
  def any_with_role?(name)
    roles = Role.find_by(name: name)
    return false unless roles

    roles.users.any?
  end

  # Use the time zone of the current user
  def use_time_zone(&block)
    Time.use_zone(current_user.time_zone || 'UTC', &block)
  end

  # It's important that the location is NOT stored if:
  # - The request method is not GET (not idempotent)
  # - The request is handled by a Devise controller such as Devise::SessionsController as that could cause an
  #   infinite redirect loop.
  # - The request is an Ajax request as this can lead to very unexpected behaviour.
  def storable_location?
    request.get? && is_navigational_format? && !devise_controller? && !request.xhr?
  end

  def store_user_location!
    store_location_for(:user, request.fullpath)
  end

  def extract_locale_from_accept_language_header
    http_accept_language.compatible_language_from(I18n.available_locales)
  end

  def extract_locale_from_settings
    return unless current_user && current_user.locale.present? && current_user.locale.to_sym.in?(I18n.available_locales)

    current_user.locale.to_sym
  end

  def user_not_authorized(exception)
    policy_name = exception.policy.class.to_s.underscore

    error_message = t "#{policy_name}.#{exception.query}", scope: 'pundit', default: :default

    respond_to do |format|
      format.html do
        flash[:error] = error_message
        redirect_to(request.referrer || root_path)
      end
      format.json { render json: { error: 403, message: error_message }, status: :forbidden }
    end
  end
end
