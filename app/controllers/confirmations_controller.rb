# frozen_string_literal: true

class ConfirmationsController < Devise::ConfirmationsController
  prepend_before_action :check_captcha, only: [:create]

  def show
    super
    @confirmation = resource # Needed for Merit / Must be AFTER the super
  end

  def check_captcha
    return if verify_recaptcha(action: 'confirmation/resend')

    self.resource = resource_class.new

    respond_with_navigational(resource) do
      flash[:recaptcha_error] = 'reCAPTCHA verification failed, please try again.' unless flash[:recaptcha_error]
      flash.discard(:recaptcha_error) # We need to discard flash to avoid showing it on the next page reload
      render :new
    end
  end
end
